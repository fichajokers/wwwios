angular.module('starter.controllers')


.controller('homeCtrl', ['$scope','$http','$state','$ionicNativeTransitions','$ionicPopup','$rootScope','$ionicModal','$cordovaGeolocation','$cordovaToast','$httpParamSerializer','$ionicPopover','$ionicPlatform','$ionicLoading','$cordovaFile','$interval','$cordovaAppVersion',function($scope,$http,$state,$ionicNativeTransitions,$ionicPopup,$rootScope,$ionicModal,$cordovaGeolocation,$cordovaToast,$httpParamSerializer,$ionicPopover,$ionicPlatform,$ionicLoading,$cordovaFile,$interval,$cordovaAppVersion) {
  $ionicPlatform.ready(function() {
  /*
  cordova.getAppVersion(function(version) {
    appVersion = version;
    console.log(version);
    
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getVersion.php",
      data: $httpParamSerializer({
        "so"    : "android",
        version : version
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        if(response.data.resultado == "ok"){
          //alert("OK");
        }else{
          $rootScope.map.setClickable(false);
          var alertPopup = $ionicPopup.alert({
            title: 'Aviso',
            template: 'Actualice la versión de Atacama Go para seguir utilizando la aplicación',
            okText : 'Actualizar',
            okType : 'btn-morado'
          });

          alertPopup.then(function(res) {
            window.open('https://play.google.com/store/apps/details?id=com.uv.atacamago&hl=es','_system');
            //$rootScope.map.setClickable(true);
          });

        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

  });

  function callAtInterval() {
    $cordovaFile.readAsText("file:/proc/", "meminfo")
      .then(function (success) {
        //console.log("[ASD]",success);

        var linea = success.split("\n"); 
        //console.log(linea);
        var numberPattern = /\d+/g;


        var MemoriaTotal = linea[0].match( numberPattern );
        var MemFree      = linea[1].match( numberPattern );
        var buffers      = linea[2].match( numberPattern );
        var cached       = linea[3].match( numberPattern );

        var MemoriaFree = parseInt(MemFree) + parseInt(buffers) + parseInt(cached);
        //console.log("MemFree: " + MemFree);
        //console.log("buffers: " + buffers);
        //console.log("cached: "  + cached);

        MemoriaTotal = parseInt(MemoriaTotal)/1024;
        MemoriaFree  = parseInt(MemoriaFree)/1024;
        porcentaje   = 100 - ( (MemoriaFree*100)/MemoriaTotal );

        //console.log( "total: " + MemoriaTotal + " MB | free: " + MemoriaFree + "MB Porcentaje: " + porcentaje + "%");

        if(porcentaje >= 95){
          $rootScope.toast('Dispositivo con poca memoria libre', 'short');
        }


      }, function (error) {
        // error
      });
  } */

  $scope.esAndroid = false;
  if( ionic.Platform.isAndroid() ){
    $scope.esAndroid = true;
  }else{
    $scope.esAndroid = false;
  }

  $rootScope.inicioSesionModal = function(valor){
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/tieneCuestionario.php",
      data: $httpParamSerializer({
        "id_usuario": localStorage.getItem("id_usuario")
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        console.log("tiene cuestionario: ",response);
        
        if(valor == 1){
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
          });
          $scope.seleccionadoMostrarTodos = false;

          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = false;
          });
          $scope.seleccionadoMostrarTodosServicio = false;
        }

        if(response.data.tieneCuestionario == 0){
          $scope.openModalPreferencias2();
        }

    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });


  }
  //Variable para que no se haga zoom a un marcador al mostrar por primera vez el mapa
  $scope.auxMarcador1                     = 0;
  //cambiar menu si se selecciona crear marcador,(cambiara a true la variable)
  //que es la funcion openModalFiguraGps().
  $scope.btnCrearMarcador                 = false;
  
  //contador cantidad de veces que entra a watch position
  $scope.watchPositionCont                = 0;
  $scope.miRuta                           = [];
  $rootScope.id_item_turistico            = 0;
  $scope.marcadores                       = [];
  $scope.markerArray                      = [];
 
  $scope.markerArrayItemRutaDinamica       = [];
  $scope.markerArrayServiciosRutaDinamica  = [];
  $scope.markerArrayPatrimonioRutaDinamica = [];
  
  $scope.patrimonios                      = [];
  $scope.markerArrayPatrimonio            = [];
  
  $scope.serviciosTuristicos              = [];
  $scope.markerArrayServiciosTuristicos   = [];
  
  $scope.latLng                           = [];
  $rootScope.map                          = [];
  $rootScope.id_item_turistico_modificar  = null;
  $rootScope.modalMenuAccionesModificar   = null;
  $scope.overlaytype                      = 0;
  
  $rootScope.tipo_seleccionado            = "";
  
  $rootScope.patrimonio_seleccionado      = "";
  $rootScope.servicio_seleccionado        = "";
  
  $scope.showHideItem                     = false; //false oculto true visible
  $scope.showHidePatrimonio               = false; //0 oculto 1 visible
  $scope.showHideServicio                 = false; //0 oculto 1 visible
  $scope.rutaDinamica                     = false; //0 oculto 1 visible
  
  $scope.sub_tipo_atractivo               = [];
  $scope.tipo_servicio                    = [];
  $scope.patrimonios_seleccionados        = [];
  $scope.servicios_seleccionados          = [];
  $scope.seleccionadoMostrarTodos         = false;
  $scope.seleccionadoMostrarTodosServicio = false;
  $scope.auxiliarPantalla                 = 0;
  $scope.auxLatPantalla                   = 0;
  $scope.auxLngPantalla                   = 0;
  $scope.agregarPatrimonios               = 0;
  $scope.agregarServicios                 = 0;
  
  $scope.serviciosDinamica                = [];
  $scope.patrimoniosDinamica              = [];

  $scope.agregarMarkerDinamico            = 0;
  $scope.auxiliarMostrarDinamica          = 0;
  $rootScope.btnRecorriendoRuta           = false;
  $scope.entrarUnaVezRecomendado          = 0;
  $scope.entrarUnaVezDinamico             = 0;
  $scope.pausa                            = false;
  $scope.textoPausarContinuar             = "pausar";
  $scope.btnTerminarAventura              = false;
  $scope.btnAgregarItemRutaTipica         = false;
  $scope.btnOpcionesMarcador              = false;
  $scope.btnVolverVerItem                 = false;
  $scope.bloquearMostrarPatrimonios       = false;
  $scope.bloquearMostrarServicios         = false;
  $scope.btnMostrarReservaEnMapa          = false;
  $scope.menuVerItemsTrurista             = false;
  $scope.menuVerItemsTruristaTotal        = false;

  $scope.markerArrayEventosDinamicosRuta  = [];
  $scope.eventosDinamicosRuta             = [];

  $rootScope.id_ruta_recorrida_turista    = null;

  /*$http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/tieneCuestionario.php",
    data: $httpParamSerializer({
      "id_usuario": localStorage.getItem("id_usuario")
    }),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      //console.log("tiene cuestionario: ",response);
      if(response.data.tieneCuestionario == 0){
        $scope.openModalPreferencias2();
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });*/

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuPatrimonio.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      //console.log(response);
      if(response.data.resultado == "ok"){
        $scope.sub_tipo_atractivo = response.data.sub_tipo_atractivo;
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      if(response.data.resultado == "ok"){
        $scope.tipo_servicio = response.data.tipo_servicio;
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  document.addEventListener("deviceready", function() {
    var div = document.getElementById("map_canvas");
    var COPIAPO                = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);

    // Initialize the map view
    $rootScope.map = plugin.google.maps.Map.getMap(div,{
      'backgroundColor' : 'white',
      'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
      'controls' : {
        'compass'          : true,
        'myLocationButton' : true,
        'indoorPicker'     : true,
        'zoom'             : true
      },
      'gestures': {
        'scroll' : true,
        'tilt'   : true,
        'rotate' : true,
        'zoom'   : true
      },
      'camera': {
        'latLng' : COPIAPO,
        'tilt'   : 0,
        'zoom'   : 10,
        'bearing': 0
      }
    });

    // Wait until the map is ready status.
    $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);
  }, false);

    function onMapReady() {
      $rootScope.map.setPadding( 0, 0 , 200 , 0);
      //$interval(callAtInterval, 10000);
      if($rootScope.flagMostrarAtractivosTuristicos == true){
        $rootScope.flagMostrarAtractivosTuristicos = false;
        var id_sub_tipo_atractivo = $rootScope.mostrarAtractivosTuristicos.id_sub_tipo_atractivo;
        var index                 = $rootScope.mostrarAtractivosTuristicos.index;
        console.log(id_sub_tipo_atractivo,index); 
        $scope.mostrarPatrimonios(id_sub_tipo_atractivo,index);
      }

      if($rootScope.flagMostrarServiciosTuristicos == true){
        $rootScope.flagMostrarServiciosTuristicos = false;
        var id_tipo_servicio = $rootScope.mostrarServiciosTuristicos.id_tipo_servicio;
        var index            = $rootScope.mostrarServiciosTuristicos.index;
        console.log(id_tipo_servicio,index); 
        $scope.mostrarServiciosTuristicos(id_tipo_servicio,index);
      }

      document.addEventListener('backbutton', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $rootScope.map.remove();
      }, false);
    }

    function onBtnClicked() {
      map.showDialog();
    }
    /*
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getPatrimonioRutaDinamica.php",
      data: $httpParamSerializer({}),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
        if(response.data.resultado == "ok"){
          //console.log(response.data.item_turistico);
          $scope.patrimoniosDinamica = response.data.patrimonio;
          
        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosRutaDinamicaNuevo.php",
      data: $httpParamSerializer({}),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
        if(response.data.resultado == "ok"){
          //console.log(response.data.item_turistico);
          $scope.serviciosDinamica = response.data.servicios;

        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });
    */
    $scope.cerrarHome = function(){
      event.preventDefault();
      event.stopPropagation();
      $rootScope.map.remove();
    }
    $scope.mostrarMarcadores = function(){

      $ionicLoading.show({
        template: 'Iniciando aventura...'
      }).then(function(){
        //console.log("cargando al apretar comenzar recorrido :c");
      });

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addRutaRecorridaTurista.php",
        data: $httpParamSerializer({
          'id_usuario' : localStorage.getItem("id_usuario")
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si recato bien los datos
          //esconder loading
          $ionicLoading.hide().then(function(){});
          
          if(response.data.resultado == "ok"){
            $rootScope.id_ruta_recorrida_turista = response.data.id_ruta_recorrida_turista;
            //var contador = 0  //contador que indica si es la primera repeticion del watPosition..
            //document.getElementById("containerLoading").className = "fadeIn";
            if($scope.rutaDinamica == false){ //está oculto, por lo tanto mostrarlo....
              $scope.btnTerminarAventura = true; //ocultar icono y mostrar el "menu aventura"...
              //$scope.rutaDinamica = true;
              $scope.entrarUnaVezDinamico = 0;
              $scope.fechaAnteriorDinamica = new Date();
              $scope.watchMarcadores();
            }else{ //esta visible, por lo tanto ocultarlo
              $scope.rutaDinamica = false;
              $scope.entrarUnaVezDinamico = 0;
              navigator.geolocation.clearWatch($scope.watchRutaDinamica);
              $scope.watchRutaDinamica = null;
              $rootScope.map.clear();
              $rootScope.map.off();
            }
            //document.getElementById("containerLoading").className = "fadeOut";
          }
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
      
    }

    //mostrar la figura del gps, para crear un item turístico
    $scope.openModalFiguraGps = function(){
      $scope.popoverCrearItem.hide();
      document.getElementById("contenedorMarcador").className = "fadeIn";
      $scope.btnCrearMarcador = true;
      $scope.btnOpcionesMarcador = false;
      //si es true se oculta el icono del marcador....
      $scope.btnAgregarItemRutaTipica = true;
    }

    $scope.openOpcionMarcador = function(){
      $scope.btnOpcionesMarcador = true;
    }

    $scope.openVerItemsTurista = function(valor){
      $scope.popoverCrearItem.hide();
      $scope.btnOpcionesMarcador = false;
      $scope.btnVolverVerItem    = true;
      
      if(valor == 0 || valor == '0'){
        $scope.menuVerItemsTruristaTotal = false;
        if($scope.menuVerItemsTrurista == false){
          $scope.menuVerItemsTrurista = true;
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getItemTuristicoUsuario.php",
            data: $httpParamSerializer({
              "id_usuario": localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              console.log(response.data.item_turistico);
              if(response.data.resultado == "ok"){
                
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayItemUsuario = [];
                $scope.itemTuristicosVer = response.data.item_turistico;

                angular.forEach(response.data.item_turistico, function(value, key) {  
                                 
                var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemUsuario[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
                });

                $scope.markerArrayServiciosTuristicos = [];

                  angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
                        var ubicacion = new plugin.google.maps.LatLng(
                          parseFloat(value.direccion_georeferenciada_latitud),
                          parseFloat(value.direccion_georeferenciada_longitud) );

                        //var url = "www/img/servicios/"+value.icono+".png";
                        var url = "www/img/servicios/"+value.icono+"Oculto.png";

                        $rootScope.map.addMarker({
                          'position': ubicacion,
                          'title': value.nombre_item_turistico,
                          'draggable': true,
                          'anchor':  [30, 45],
                          icon: {
                            url: url,
                            size: { width: 30, height: 45 },
                          },
                          zIndex: 1
                        },function(marker) {
                            //accion al hacer clic en el marcador
                            $scope.markerArrayServiciosTuristicos[key] = marker;
                            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                              $rootScope.tipo_seleccionado = "servicio";
                              $rootScope.item_seleccionado = value;
                              $scope.openModalMenuAccionesModificar();
                            });
                        });
                        
                });

                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/patrimonios/"+value.icono+".png";
                      var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': true,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayPatrimonio[key] = marker;
                        $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                        $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "patrimonio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                      });
                });

              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }else{
          $scope.menuVerItemsTrurista = false;
        
          $scope.btnVolverVerItem = false;
          $scope.markerArrayItemUsuario = [];
          $scope.itemTuristicosVer      = [];
          $rootScope.map.clear();
          $rootScope.map.off();
          $scope.markerArrayServiciosTuristicos = [];

          angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': true,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
          });

          $scope.markerArrayPatrimonio = [];

          angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': true,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
          });

        }
      }

      if(valor == 1){
        $scope.menuVerItemsTrurista = false;
        if($scope.menuVerItemsTruristaTotal == false){
          $scope.menuVerItemsTruristaTotal = true;
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getItemTuristicoTotal.php",
            data: $httpParamSerializer({
              id_usuario: localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              console.log(response.data.item_turistico);
              if(response.data.resultado == "ok"){
                
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayItemTotal = [];
                $scope.itemTuristicosVerTotal = response.data.item_turistico;

                angular.forEach(response.data.item_turistico, function(value, key) {  
                                 
                var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemTotal[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
                });

                $scope.markerArrayServiciosTuristicos = [];

                  angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
                        var ubicacion = new plugin.google.maps.LatLng(
                          parseFloat(value.direccion_georeferenciada_latitud),
                          parseFloat(value.direccion_georeferenciada_longitud) );

                        //var url = "www/img/servicios/"+value.icono+".png";
                        var url = "www/img/servicios/"+value.icono+"Oculto.png";

                        $rootScope.map.addMarker({
                          'position': ubicacion,
                          'title': value.nombre_item_turistico,
                          'draggable': true,
                          'anchor':  [30, 45],
                          icon: {
                            url: url,
                            size: { width: 30, height: 45 },
                          },
                          zIndex: 1
                        },function(marker) {
                            //accion al hacer clic en el marcador
                            $scope.markerArrayServiciosTuristicos[key] = marker;
                            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                              $rootScope.tipo_seleccionado = "servicio";
                              $rootScope.item_seleccionado = value;
                              $scope.openModalMenuAccionesModificar();
                            });
                        });
                        
                });

                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/patrimonios/"+value.icono+".png";
                      var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': true,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayPatrimonio[key] = marker;
                        $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                        $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "patrimonio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                      });
                });

              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }else{
          $scope.menuVerItemsTruristaTotal = false;
        
          $scope.btnVolverVerItem       = false;
          $scope.markerArrayItemTotal   = [];
          $scope.itemTuristicosVerTotal = [];
          $rootScope.map.clear();
          $rootScope.map.off();
          
          $scope.markerArrayServiciosTuristicos = [];

          angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': true,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
          });

          $scope.markerArrayPatrimonio = [];

          angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': true,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
          });

        }
      }
        
        
    }
    
    $rootScope.mostrarEventoSeleccionado = function(latitud,longitud,titulo,evento){
      $rootScope.map.clear();
      $rootScope.map.off();
               
      var ubicacion = new plugin.google.maps.LatLng(
        parseFloat(latitud),
        parseFloat(longitud) );

      $rootScope.map.moveCamera({
        'target': ubicacion,
        'zoom': 18,
      }, function() {
        console.log("Camera position changed.");
      });


      var url = "www/img/gps.png";

      $rootScope.map.addMarker({
        'position': ubicacion,
        'title': titulo,
        'draggable': true,
        'anchor':  [30, 45],
        icon: {
          url: url,
          size: { width: 30, height: 45 },
        },
        zIndex: 1
      },function(marker) {
          //accion al hacer clic en el marcador
          $scope.markerArrayServiciosTuristicos = marker;
          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
            $rootScope.tipo_seleccionado = "evento";
            $rootScope.evento_seleccionado = evento;
            $scope.openModalEvento();
          });
      });
                      
      $scope.markerArrayServiciosTuristicos = [];

        angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
              var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

              //var url = "www/img/servicios/"+value.icono+".png";
              var url = "www/img/servicios/"+value.icono+"Oculto.png";

              $rootScope.map.addMarker({
                'position': ubicacion,
                'title': value.nombre_item_turistico,
                'draggable': true,
                'anchor':  [30, 45],
                icon: {
                  url: url,
                  size: { width: 30, height: 45 },
                },
                zIndex: 1
              },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayServiciosTuristicos[key] = marker;
                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                    $rootScope.tipo_seleccionado = "servicio";
                    $rootScope.item_seleccionado = value;
                    $scope.openModalMenuAccionesModificar();
                  });
              });
                        
      });

      $scope.markerArrayPatrimonio = [];

      angular.forEach($scope.patrimonios, function(value, key) {

            var ubicacion = new plugin.google.maps.LatLng(
              parseFloat(value.direccion_georeferenciada_latitud),
              parseFloat(value.direccion_georeferenciada_longitud) );

            //var url = "www/img/patrimonios/"+value.icono+".png";
            var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

            $rootScope.map.addMarker({
              'position': ubicacion,
              'title': value.nombre_item_turistico,
              'draggable': true,
              'anchor':  [30, 45],
              icon: {
                url: url,
                size: { width: 30, height: 45 },
              },
              zIndex: 1
            },function(marker) {
              //accion al hacer clic en el marcador
              $scope.markerArrayPatrimonio[key] = marker;
              $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
              $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

              marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                $rootScope.tipo_seleccionado = "patrimonio";
                $rootScope.item_seleccionado = value;
                $scope.openModalMenuAccionesModificar();
              });
            });
      });
    }


    $scope.volverMenuPrincipal = function(){
      $scope.btnVolverVerItem = false;
      $scope.markerArrayItemUsuario = [];
      $rootScope.map.clear();
      $rootScope.map.off();
              $scope.markerArrayServiciosTuristicos = [];

              angular.forEach($scope.serviciosTuristicos, function(value, key) {  
               
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/servicios/"+value.icono+".png";
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': true,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
              });

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
              });
    }

    $scope.openModalEvento = function(){
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView()){
        $rootScope.map.setClickable(false);
      }  
      
      //Modal rutas Tipicas...
      $ionicModal.fromTemplateUrl('templates/eventoSeleccionado.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalEventoSeleccionado = modal;
        $rootScope.modalEventoSeleccionado.show();
      });

    }

    //se oculta la figura del gps, cuando ya se creó o se cancelo 
    //el crear item turístico
    $scope.cerrarModalFiguraGps = function(){
      document.getElementById("contenedorMarcador").className = "fadeOut";
      $scope.btnCrearMarcador = false;

      //si es true se MUESTRA el icono del marcador....
      $scope.btnAgregarItemRutaTipica = false;
    }

    //Modal Crear nuevo Marcador (itemTurístico)...
    $ionicModal.fromTemplateUrl('templates/crearMarcador.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalCrearMarcador = modal;
    });

    //Popover con las rutas
    $ionicPopover.fromTemplateUrl('templates/menuRutas.html', {
        scope: $scope,
        animation: 'fadeInLeft',
    }).then(function(popover) {
        $rootScope.popover = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopover = function($event) {
      //hacer que el mapa no sea clickeable.
      //if( ionic.Platform.isAndroid() )

      $rootScope.map.setClickable(false);

      $rootScope.popover.show($event);

    }; 

    // Execute action on hide popover 
    // (cuando cierro el popover hacer clickeable el mapa)
    $scope.$on('popover.hidden', function() {
      // hacer clickeable el mapa
      $rootScope.map.setClickable(true);
    });
    
    //Popover menu patrimonios
    $ionicPopover.fromTemplateUrl('templates/menuPatrimonios.html', {
        scope: $scope,
        animation: 'fadeInLeft',
    }).then(function(popover) {
        $scope.popoverPatrimonios = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopoverPatrimonios = function($event) {
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);

      $scope.popoverPatrimonios.show($event);

    };


    // Execute action on hide popover
    $scope.$on('popover.hidden', function() {
      // hacer clickeable el mapa
      $rootScope.map.setClickable(true);
    });
    
    //Popover menuServicios
    $ionicPopover.fromTemplateUrl('templates/menuServicios.html', {
        scope: $scope,
        animation: 'fadeInLeft',
    }).then(function(popover) {
        $scope.popoverServicios = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopoverServicios = function($event) {
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);

      $scope.popoverServicios.show($event);

    };

    //Popover menu crear item
    $ionicPopover.fromTemplateUrl('templates/menuCrearItem.html', {
        scope: $scope,
        animation: 'fadeInLeft',
    }).then(function(popover) {
        $scope.popoverCrearItem = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopoverCrearItem = function($event) {
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);

      $scope.popoverCrearItem.show($event);

    };

    //abrir el modal cuando apretamos un item, patrimonio, servicio
    //en el mapa
    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
    }

    //Modal preferencias2...
  $scope.openModalPreferencias2 = function(){
    $ionicModal.fromTemplateUrl('templates/preferencias2.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalPreferencias2 = modal;
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);
      $rootScope.modalPreferencias2.show();
      
    });
  };

    $scope.a = function(){
      alert("ASdads");
    }

    //Modal menu Acciones... (igual al modal menuAccionesModificar),
    //pero este es para crear un nuevo item turístico, por lo tanto no es
    //modificar, sino agregar.
    
    $scope.openModalMenuAcciones22 = function(){
      $ionicModal.fromTemplateUrl('templates/menuAcciones.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAcciones = modal;
        $rootScope.map.setClickable(false);
        $rootScope.modalMenuAcciones.show();
      });
    };

    $scope.volverantAntesCrearMarcador = function(){
      $scope.btnOpcionesMarcador = false;
    }
    
    $rootScope.MostrarReservaEnMapaHomeCtrl = function(){
      $scope.btnMostrarReservaEnMapa = true;
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(true);  
    }

    $rootScope.MostrarModalReservaEnMapaHomeCtrl = function(){
      $scope.btnMostrarReservaEnMapa = false;
      $rootScope.modalReservas.show();
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);  
    }

    $scope.openModalMenuAcciones = function(){
      $ionicLoading.show({
        template: 'Cargando...'
      });

      //si es false se MUESTRA el icono del marcador....
      $scope.btnAgregarItemRutaTipica = false;

      document.getElementById("contenedorMarcador").className = "fadeOut";
      //ahora hay que crear el marcador con la latitud y longitud seleccionada 
      //conseguimos la latitud y longitud seleccionada
      if( true ){
        $rootScope.map.getCameraPosition(function(camera) {
          var lat = camera.target.lat;
          var lng = camera.target.lng;
          
          $rootScope.POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);
          
          //obtenemos la direccion del lugar....
          var request = {
            'position': $rootScope.POS_MARCADOR
          };

          plugin.google.maps.Geocoder.geocode(request, function(results) {
            if (results.length) {
              var result = results[0];
              var position = result.position; 
              var address = {
                numero: result.subThoroughfare || "",
                calle: result.thoroughfare || "",
                ciudad: result.locality || "",
                region: result.adminArea || "",
                postal: result.postalCode || "",
                pais:   result.country || ""
              };
              
              //GUARDAR LA DIRECCION ...
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addItemTuristico.php",
                data: $httpParamSerializer({
                  "id_usuario": localStorage.getItem("id_usuario"),
                  "direccion_georeferenciada_longitud" : lng,
                  "direccion_georeferenciada_latitud" : lat,
                  "direccion_item" : address.callle,
                  "numero_direccion_item" : address.numero,
                  "ciudad" : address.ciudad,
                  "id_ruta_recorrida_turista" : $rootScope.id_ruta_recorrida_turista
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                console.log(response);
                if(response.data.resultado == "ok"){
    
                  //guardar el id_item_turistico para guardar la descripcion titulos videos etc.
                  $rootScope.id_item_turistico = response.data.id_item_turistico;
                  var id_item_turistico_creado_usuario = response.data.id_item_turistico;

                  $scope.openModalMenuAcciones22();

                  /*creamos el marcador...*/
                  var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);
                   
                  $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: 'darkcyan'
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        var marcadorNuevo = {};
                        $http({
                          method: "POST",
                          url: "http://200.14.68.107/atacamaGo/getItemTuristicoNuevo.php",
                          data: $httpParamSerializer({
                            "id_item_turistico": id_item_turistico_creado_usuario,
                            "id_usuario": localStorage.getItem("id_usuario"),
                        }),
                          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                        }).then(function(response){ //ok si guardó correctamente.
                          //console.log(response);
                          if(response.data.resultado == "ok"){
                            
                            marcadorNuevo = {
                              "descripcion"                        : response.data.item_turistico.descripcion,
                              "nombre"                             : response.data.item_turistico.nombre,
                              "direccion_georeferenciada_latitud"  : "",
                              "direccion_georeferenciada_longitud" : "",
                              "foto"                               : response.data.item_turistico.foto,
                              "id"                                 : id_item_turistico_creado_usuario,
                              "id_usuario"                         : response.data.item_turistico.id_usuario
                            };

                            $rootScope.tipo_seleccionado = "item";
                            $rootScope.item_seleccionado = marcadorNuevo;
                            $ionicLoading.hide();
                            $scope.openModalMenuAccionesModificar();
                          }else if ( response.data.resultado == "no data" ){
                            //$rootScope.toast('lugar aún no tiene audios', 'short');
                            $ionicLoading.hide();
                          }else{
                            $rootScope.toast('error, intenta nuevamente', 'short');
                            $ionicLoading.hide();
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                          $ionicLoading.hide();
                        });

                      });   
                  });
                  /*fin crear marcador...*/
                  $scope.cerrarModalFiguraGps();
                  $rootScope.toast('Item agregado', 'short');
                  $ionicLoading.hide();
                }
                else{
                  $scope.cerrarModalFiguraGps();
                  $rootScope.toast('Sólo se pueden crear ítems en Atacama', 'short');
                  $ionicLoading.hide();
                }

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short');
                  $ionicLoading.hide();
              });
              //GUARDAR LA DIRECCION ...       
            } else {
              $rootScope.toast('Verifica tu conexión a internet', 'short');
              $ionicLoading.hide();
            }
          });

        });
      }

    }

    //Mostrar patrimonios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de patrimionios....
    $scope.mostrarPatrimonios = function(id_sub_tipo_atractivo,index){

      document.getElementById("containerLoading").className = "fadeIn";
      if( $scope.popoverPatrimonios.isShown() ){
        $scope.popoverPatrimonios.hide();  
      }
      
      //Cambiar clase al elemento seleccionado
      if( index != -1 ){
        if($scope.sub_tipo_atractivo[index].seleccionado == false)
          $scope.sub_tipo_atractivo[index].seleccionado = true;
        else
          $scope.sub_tipo_atractivo[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_sub_tipo_atractivo == -1){
        //seleccionar todos los sub tipos atractivos....
        if( $scope.seleccionadoMostrarTodos == false){
          $scope.seleccionadoMostrarTodos = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodos = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.patrimonios = [];
        }
        
        //Cambiar todas los botones a activos...
        var menuPatrimonio   = document.getElementsByClassName("menuPatrimonio");
        menuPatrimonio.class = "menuPatrimonio";
      } 

      $scope.showHidePatrimonio = false;
      if($scope.showHidePatrimonio == false){ //está oculto, por lo tanto mostrarlo....
        //console.log("Llamar a php");
        //conseguir patrimonios

        if( id_sub_tipo_atractivo == -1){
          //console.log($scope.patrimonios_seleccionados.length, $scope.sub_tipo_atractivo.length + 1);
          if($scope.patrimonios_seleccionados.length == $scope.sub_tipo_atractivo.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.patrimonios_seleccionados = [];
          }else{

            $scope.patrimonios_seleccionados = [];
            angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
              $scope.patrimonios_seleccionados.push(value.id_sub_tipo_atractivo)     
            });
          }
        }

        if( $scope.patrimonios_seleccionados.length == 0 ){
          $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
        }else{
          if($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo) == -1) {
            $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
          }else
            $scope.patrimonios_seleccionados.splice($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo), 1);
        }

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
          data: $httpParamSerializer({
            id_sub_tipo_atractivo     : id_sub_tipo_atractivo,
            patrimonios_seleccionados : JSON.stringify($scope.patrimonios_seleccionados)
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente
            if(response.data.resultado == "ok"){
              //console.log("Lista patrimoinios: ");
              //console.log(response.data);

              var actualizarMarcadores = true;
              //evaluar si se rescatan patrimoinos o no....
              
              if( $scope.seleccionadoMostrarTodos == false ){
                var contatorMarcadores = 0;
                angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
                  
                  if( value.seleccionado == false ){
                    contatorMarcadores++;
                  }
                });
                
              }

              if( contatorMarcadores == $scope.sub_tipo_atractivo.length ){
                $scope.patrimonios_seleccionados = [];
                $scope.showHidePatrimonio = false;
                
                $scope.patrimonios = [];
              }else{
                 $scope.patrimonios = response.data.patrimonio;
                 $scope.showHidePatrimonio = true; //mantener visto...
              }
              
              console.log( $scope.patrimonios );

              $scope.agregarPatrimonios = 1;

              $rootScope.map.clear();
              $rootScope.map.off();

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
                });

                $scope.markerArrayServiciosTuristicos = [];

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': true,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
                });

                $scope.markerArrayItemUsuario = [];
            
                angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemUsuario[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
                });

                $scope.markerArrayItemTotal = [];
            
                angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemTotal[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
                });

                $scope.agregarPatrimonios = 0;

                //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
                if( $scope.btnTerminarAventura == true ){

                  $scope.markerArrayServiciosTuristicosDinamicos = [];
                            
                  angular.forEach($scope.serviciosDinamica, function(value, key) {  
                             
                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/servicios/"+value.icono+".png";
                                  var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': true,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "servicio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                  });
                                  
                            });

                  $scope.markerArrayPatrimonioDinamicos = [];

                  angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                var ubicacion = new plugin.google.maps.LatLng(
                                  parseFloat(value.direccion_georeferenciada_latitud),
                                  parseFloat(value.direccion_georeferenciada_longitud) );

                                //var url = "www/img/patrimonios/"+value.icono+".png";
                                var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                $rootScope.map.addMarker({
                                  'position': ubicacion,
                                  'title': value.nombre_item_turistico,
                                  'draggable': true,
                                  'anchor':  [30, 45],
                                  icon: {
                                    url: url,
                                    size: { width: 30, height: 45 },
                                  },
                                  zIndex: 1
                                },function(marker) {
                                  //accion al hacer clic en el marcador
                                  $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                    $rootScope.tipo_seleccionado = "patrimonio";
                                    $rootScope.item_seleccionado = value;
                                    $scope.openModalMenuAccionesModificar();
                                  });
                                });
                  });

                  $scope.markerItemDinamicos = [];
                  angular.forEach($scope.itemDinamico, function(value, key) {

                                var ubicacion = new plugin.google.maps.LatLng(
                                  parseFloat(value.direccion_georeferenciada_latitud),
                                  parseFloat(value.direccion_georeferenciada_longitud) );

                                //var url = "www/img/patrimonios/"+value.icono+".png";
                                var url = "www/img/posicionGpsTuristaOculto.png";

                                $rootScope.map.addMarker({
                                  'position': ubicacion,
                                  'title': value.nombre_item_turistico,
                                  'draggable': true,
                                  'anchor':  [30, 45],
                                  icon: {
                                    url: url,
                                    size: { width: 30, height: 45 },
                                  },
                                  zIndex: 1
                                },function(marker) {
                                  //accion al hacer clic en el marcador
                                  $scope.markerItemDinamicos[key] = marker;
                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                    $rootScope.tipo_seleccionado = "item";
                                    $rootScope.item_seleccionado = value;
                                    $scope.openModalMenuAccionesModificar();
                                  });
                                });
                  });
                }
                document.getElementById("containerLoading").className = "fadeOut";
                $scope.bloquearMostrarPatrimonios = false;
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
        
      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    //Mostrar Servicios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de servicios....
    $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
      document.getElementById("containerLoading").className = "fadeIn";
      $scope.popoverServicios.hide();
      //Cambiar clase al elemento seleccionado
      if( index != -1 ){
        if($scope.tipo_servicio[index].seleccionado == false)
          $scope.tipo_servicio[index].seleccionado = true;
        else
          $scope.tipo_servicio[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_tipo_servicio == -1){
        //seleccionar todos los sub tipos servicios....
        if( $scope.seleccionadoMostrarTodosServicio == false){
          $scope.seleccionadoMostrarTodosServicio = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodosServicio = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.serviciosTuristicos = [];
        }
        
        //Cambiar todas los botones a activos...
        var menuServicios   = document.getElementsByClassName("menuServicios");
        menuServicios.class = "menuServicios";
      } 

      $scope.showHideServicio = false;
      if($scope.showHideServicio == false){ //está oculto, por lo tanto mostrarlo....

        if( id_tipo_servicio == -1){
          if($scope.servicios_seleccionados.length == $scope.tipo_servicio.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.servicios_seleccionados = [];
          }else{

            $scope.servicios_seleccionados = [];
            angular.forEach($scope.tipo_servicio, function(value, key) {
              $scope.servicios_seleccionados.push(value.id_tipo_servicio)     
            });
          }
        }
        /*---------------------------------*/
        if( $scope.servicios_seleccionados.length == 0 ){
          $scope.servicios_seleccionados.push(id_tipo_servicio);
        }else{
          if($scope.servicios_seleccionados.indexOf(id_tipo_servicio) == -1) {
            $scope.servicios_seleccionados.push(id_tipo_servicio);
          }else
            $scope.servicios_seleccionados.splice($scope.servicios_seleccionados.indexOf(id_tipo_servicio), 1);
        }

        //conseguir Servivios tuiristicos
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
          data: $httpParamSerializer({
            id_tipo_servicio        : id_tipo_servicio,
            servicios_seleccionados : JSON.stringify($scope.servicios_seleccionados),
            id_usuario              : localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente
            if(response.data.resultado == "ok"){
              console.log(response.data);

              var actualizarMarcadoresServicios = true;
              //evaluar si se rescatan servicios o no....
              
              if( $scope.seleccionadoMostrarTodosServicio == false ){
                var contatorMarcadoresServicio = 0;
                angular.forEach($scope.tipo_servicio, function(value, key) {
                  
                  if( value.seleccionado == false ){
                    contatorMarcadoresServicio++;
                  }
                });     
              }

              if( contatorMarcadoresServicio == $scope.tipo_servicio.length ){
                $scope.servicios_seleccionados = [];
                $scope.showHideServicio        = false;
                $scope.serviciosTuristicos     = [];
              }else{
                 $scope.serviciosTuristicos    = response.data.servicios;
                 $scope.showHideServicio = true; //mantener visto...
              }
              
              $scope.agregarServicios = 1;
              
              $rootScope.map.clear();
              $rootScope.map.off();
              $scope.markerArrayServiciosTuristicos = [];

              angular.forEach($scope.serviciosTuristicos, function(value, key) {  
               
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/servicios/"+value.icono+".png";
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': true,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
              });

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
              });

              $scope.markerArrayItemUsuario = [];
            
              angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                  
                var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

                var url = "www/img/posicionGpsTuristaOculto.png";

                $rootScope.map.addMarker({
                  'position': ubicacion,
                  'title': value.nombre_item_turistico,
                  'draggable': true,
                  'anchor':  [30, 45],
                  icon: {
                    url: url,
                    size: { width: 30, height: 45 },
                  },
                  zIndex: 1
                },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayItemUsuario[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "item";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                  });
                });
                                      
              });

              $scope.markerArrayItemTotal = [];
            
              angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemTotal[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
              });

              //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
              if( $scope.btnTerminarAventura == true ){

                  $scope.markerArrayServiciosTuristicosDinamicos = [];
                            
                  angular.forEach($scope.serviciosDinamica, function(value, key) {  
                             
                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/servicios/"+value.icono+".png";
                                  var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': true,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "servicio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                  });
                                  
                            });

                  $scope.markerArrayPatrimonioDinamicos = [];

                  angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                var ubicacion = new plugin.google.maps.LatLng(
                                  parseFloat(value.direccion_georeferenciada_latitud),
                                  parseFloat(value.direccion_georeferenciada_longitud) );

                                //var url = "www/img/patrimonios/"+value.icono+".png";
                                var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                $rootScope.map.addMarker({
                                  'position': ubicacion,
                                  'title': value.nombre_item_turistico,
                                  'draggable': true,
                                  'anchor':  [30, 45],
                                  icon: {
                                    url: url,
                                    size: { width: 30, height: 45 },
                                  },
                                  zIndex: 1
                                },function(marker) {
                                  //accion al hacer clic en el marcador
                                  $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                    $rootScope.tipo_seleccionado = "patrimonio";
                                    $rootScope.item_seleccionado = value;
                                    $scope.openModalMenuAccionesModificar();
                                  });
                                });
                  });

                  $scope.markerItemDinamicos = [];
                  angular.forEach($scope.itemDinamico, function(value, key) {

                                var ubicacion = new plugin.google.maps.LatLng(
                                  parseFloat(value.direccion_georeferenciada_latitud),
                                  parseFloat(value.direccion_georeferenciada_longitud) );

                                //var url = "www/img/patrimonios/"+value.icono+".png";
                                var url = "www/img/posicionGpsTuristaOculto.png";

                                $rootScope.map.addMarker({
                                  'position': ubicacion,
                                  'title': value.nombre_item_turistico,
                                  'draggable': true,
                                  'anchor':  [30, 45],
                                  icon: {
                                    url: url,
                                    size: { width: 30, height: 45 },
                                  },
                                  zIndex: 1
                                },function(marker) {
                                  //accion al hacer clic en el marcador
                                  $scope.markerItemDinamicos[key] = marker;
                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                    $rootScope.tipo_seleccionado = "item";
                                    $rootScope.item_seleccionado = value;
                                    $scope.openModalMenuAccionesModificar();
                                  });
                                });
                  });
              }

              document.getElementById("containerLoading").className = "fadeOut";
              $scope.bloquearMostrarServicios = false;
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    $scope.recorrerRuta = function(valor){

      if(valor == "volver"){
        console.log("volver");
      
        navigator.geolocation.clearWatch($scope.watchRutaTipica);
        $scope.watchRutaTipica = null;

        $rootScope.btnRecorrerRuta = false;
        $rootScope.map.clear();
        $rootScope.map.off();
        $scope.entrarUnaVezRecomendado = 0;
      }

      if(valor == "comenzar"){ //apreto "comenzar recorrido"

        /*$ionicLoading.show({
        template: 'Cargando...'
        }).then(function(){
           console.log("cargando al apretar comenzar recorrido :c");
        });*/

        console.log("comenzar");
        $rootScope.btnRecorrerRuta    = false;
        $rootScope.btnRecorriendoRuta = true;

        //actualizar el estado de la ruta
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addEstadoRutaTuristica.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario"),
            "id_ruta"   : $rootScope.id_ruta_tipica_seleccionada,
            "estado"    : 1
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            $rootScope.toast('Comenzando ruta...', 'short');
            $scope.entrarUnaVezRecomendado = 0;
            $scope.fechaAnterior = new Date();
            $scope.rutaTipica();
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
      }

    }

    $rootScope.iniciarRutaEncurso = function(){
      //marcador posicion
      $scope.watchRutaTipica = navigator.geolocation.watchPosition(function(position){
      var lat  = position.coords.latitude;
      var long = position.coords.longitude;
      
      var posision = new plugin.google.maps.LatLng(lat, long);
      $scope.miRuta.push( posision );

      if($scope.watchPositionCont >0){
          //dibujar la ruta
          $rootScope.map.addPolyline({
            points: [
              $scope.miRuta[$scope.watchPositionCont],
              $scope.miRuta[$scope.watchPositionCont-1]
            ],
            'color' : '#FD9013',
            'width': 3,
            'geodesic': true
          }, function(polyline) {
              $rootScope.posicionUsuario = polyline;
          });
      }

      $scope.watchPositionCont+=1;
    },function(error){
      console.log(error);
    },{
      timeout : 30000
    });

      //quitar filtros de marcadores....
      angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodos = false;
      $scope.patrimonios = [];
      $scope.markerArrayPatrimonio = [];

      angular.forEach($scope.tipo_servicio, function(value, key) {
        value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodosServicio = false;
      $scope.serviciosTuristicos = [];
      $scope.markerArrayServiciosTuristicos = [];
      //actualizar el estado de la ruta
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addEstadoRutaTuristica.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario"),
            "id_ruta"   : $rootScope.id_ruta_tipica_seleccionada,
            "estado"    : 1
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            //$rootScope.toast('Comenzando ruta...', 'short');
            $scope.entrarUnaVezRecomendado = 0;
            $scope.fechaAnterior = new Date();
            //$scope.rutaTipica();
            $scope.textoPausarContinuar = "Continuar";
            $scope.pausa = true;
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
    }

    $scope.recorriendoRuta = function(valor){
        if(valor == "volver"){
          console.log("volver");

          navigator.geolocation.clearWatch($scope.watchRutaTipica);
          $scope.watchRutaTipica = null;

          $rootScope.btnRecorrerRuta    = false;
          $rootScope.btnRecorriendoRuta = false;
          $rootScope.map.clear();
          $rootScope.map.off();
          $scope.entrarUnaVezRecomendado = 0;

        }

        if(valor == "pausar"){
          if( $scope.pausa == false ){ //si aprieta pausar.....
            $scope.textoPausarContinuar = "Continuar";
            $rootScope.toast('Ruta en pausa', 'short');
            $scope.pausa = true;
          }else{
            $scope.textoPausarContinuar = "Pausar";
            $scope.pausa = false;
            $rootScope.toast('Continuando ruta', 'short');
          }
        }

        if(valor == "terminar"){
          //actualizar el estado de la ruta
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/addEstadoRutaTuristica.php",
            data: $httpParamSerializer({
              "id_usuario": localStorage.getItem("id_usuario"),
              "id_ruta"   : $rootScope.id_ruta_tipica_seleccionada,
              "estado"    : 2
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              if(response.data.resultado == "ok"){
                
                console.log($scope.watchRutaTipica);
                navigator.geolocation.clearWatch($scope.watchRutaTipica);
                $scope.watchRutaTipica = null;

                $rootScope.toast('Ruta típica finalizada', 'short');
                $rootScope.btnRecorrerRuta    = false;
                $rootScope.btnRecorriendoRuta = false;
                $rootScope.map.clear();
                $rootScope.map.off();
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }
    }

    $scope.distanciaEntreCoordenadas = function(lat1,long1,lat2,long2) {
      
      var degtorad = 0.01745329;
      var radtodeg = 57.29577951;
      
      var lat1h    = lat1;
      var lat2h    = lat2;
      var long1h   = long1;
      var long2h   = long2;
      var lat1     = parseFloat(lat1h);
      var lat2     = parseFloat(lat2h);
      var long1    = parseFloat(long1h);
      var long2    = parseFloat(long2h);

      
      var dlong  = (long1 - long2);
      var dvalue = (Math.sin(lat1 * degtorad) * Math.sin(lat2 * degtorad))
            + (Math.cos(lat1 * degtorad) * Math.cos(lat2 * degtorad)
            * Math.cos(dlong * degtorad));
      var dd     = Math.acos(dvalue) * radtodeg;
      var miles  = (dd * 69.16);
      miles      = (miles * 100)/100;
      var km     = (dd * 111.302);
      km         = (km * 100)/100;

      return km*1000;
    }

    $scope.rutaTipica = function(){
      $scope.watchRutaTipica = navigator.geolocation.watchPosition(function(position){
        //console.log(position);
          
        if($scope.pausa == false){
          if($scope.entrarUnaVezRecomendado == 0){
            //actualizar el estado de la ruta
            //console.log("AYUDA2");
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
              data: $httpParamSerializer({
                "id_usuario"     : localStorage.getItem("id_usuario"),
                "id_ruta"        : $rootScope.id_ruta_tipica_seleccionada,
                "punto_longitud" : long,
                "punto_latitud"  : lat
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si recato bien los datos
                console.log(response.data);
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
            });

            $scope.mostrarRecomendados(position.coords.latitude,position.coords.longitude); 
            $scope.entrarUnaVezRecomendado = 1;
          }
            $scope.now = new Date();
            if ($scope.now - $scope.fechaAnterior > 30000) {
              //console.log("AYUDA2");
              var lat  = position.coords.latitude;
              var long = position.coords.longitude;
              //console.log(lat, long, $scope.now-$scope.fechaAnterior,$scope.now, $scope.fechaAnterior);

              //actualizar el estado de la ruta
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
                data: $httpParamSerializer({
                  "id_usuario"     : localStorage.getItem("id_usuario"),
                  "id_ruta"        : $rootScope.id_ruta_tipica_seleccionada,
                  "punto_longitud" : long,
                  "punto_latitud"  : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  console.log(response.data);
              }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
              });

              $scope.mostrarRecomendados(lat,long);

              $scope.fechaAnterior = $scope.now;
            }
        } 

      },function(error){
        console.log(error);
      },{
        timeout : 30000
      });
    }

    $scope.watchMarcadores = function(){
      $rootScope.iniciarRutaEncurso();
      $scope.watchRutaDinamica = navigator.geolocation.watchPosition(function(position){
        //console.log(position);
        //console.log($scope.watchRutaTipica);
        if($scope.entrarUnaVezDinamico == 0){

          var lat  = position.coords.latitude;
          var long = position.coords.longitude;
            
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
            data: $httpParamSerializer({
              "id_usuario"     : localStorage.getItem("id_usuario"),
              "id_ruta"        : "",
              "punto_longitud" : long,
              "punto_latitud"  : lat
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              console.log(response.data);
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });

          $scope.mostrarDinamicos(position.coords.latitude,position.coords.longitude); 
          $scope.entrarUnaVezDinamico = 1;
        }
            $scope.now = new Date();
            if ($scope.now - $scope.fechaAnteriorDinamica > 30000) {
              var lat  = position.coords.latitude;
              var long = position.coords.longitude;
            
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
                data: $httpParamSerializer({
                  "id_usuario"     : localStorage.getItem("id_usuario"),
                  "id_ruta"        : "",
                  "punto_longitud" : long,
                  "punto_latitud"  : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  console.log(response.data);
              }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
              });

              $scope.mostrarDinamicos(lat,long);
              $scope.fechaAnteriorDinamica = $scope.now;
            }

      },function(error){
        console.log(error);
      },{
        timeout : 90000
      });
    };


    /*$scope.watchMarcadores = function(){
      var contador = 0  //contador que indica si es la primera repeticion del watPosition..
       
        //preguntar por la ubicacion del usuario
        var watch = $cordovaGeolocation.watchPosition({
          timeout : 30000,
          enableHighAccuracy: false // may cause errors if true
        }).then(
          null,
          function(err) {
            console.log(err);
              $cordovaToast.show('Por favor activa el GPS', 'short', 'bottom').then(function(success) {
            }, function (error) {
              console.log("The toast was not shown due to " + error);
            });
          },
          function(position) {
            
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            
            if( $scope.rutaDinamica == true ){ //si está visible....  
              console.log("dentro rua dinamica...");
              if( $scope.auxiliarMostrarDinamica == 0 ){
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.auxiliarMostrarDinamica = 1;
              }

              //Calcular distancia entre items( desde bd) y mi posicion  

              angular.forEach($scope.marcadores, function(value, key) {

                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud); 

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  
                  if(distancia <= $rootScope.radio){
                    var url = "www/img/posicionGpsMarcador.png";
                    if($scope.markerArrayItemRutaDinamica[key]){
                      /*$scope.markerArrayItemRutaDinamica[key].seticon({
                        'url': url,
                        'size': { width: 30, height: 45 },
                        'anchor':  [10, 10]
                      });  
                    }
                    
                  }else{
                    var url = "www/img/posicionGpsTuristaOculto.png";
                    if($scope.markerArrayItemRutaDinamica[key]){
                      /*$scope.markerArrayItemRutaDinamica[key].seticon({
                        'url': url,
                        'size': { width: 30, height: 45 },
                        'anchor':  [10, 10]
                      });
                    }
                  }
                  if( $scope.agregarMarkerDinamico == 0){
                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                        anchor:  [10, 10],
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemRutaDinamica[key]     = marker;
                      $scope.markerArrayItemRutaDinamica[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayItemRutaDinamica[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                  }
                  
              }); //FIN ANGULAR FOREACH

              angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud); 

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  if(distancia <= $rootScope.radio){
                    var url = "www/img/patrimonios/"+value.icono+".png";
                    if($scope.markerArrayPatrimonioRutaDinamica[key]){
                      /*$scope.markerArrayPatrimonioRutaDinamica[key].seticon({
                        'url': url,
                        'size': { width: 30, height: 45 },
                        'anchor':  [10, 10]
                      });
                    }
                      
                  }else{
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";
                    if($scope.markerArrayPatrimonioRutaDinamica[key]){
                      /*$scope.markerArrayPatrimonioRutaDinamica[key].seticon({
                        'url': url,
                        'size': { width: 30, height: 45 },
                        'anchor':  [10, 10]
                      });
                    }   
                  }
                    

                  if( $scope.agregarMarkerDinamico == 0){
                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                        anchor:  [10, 10],
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonioRutaDinamica[key] = marker;
                      $scope.markerArrayPatrimonioRutaDinamica[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonioRutaDinamica[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                  }
                  
                }); //FIN ANGULAR FOREACH

              angular.forEach($scope.serviciosDinamica, function(value, key) {  
                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);  
          
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  if(distancia <= $rootScope.radio){
                    var url = "www/img/servicios/"+value.icono+".png";
                    if($scope.markerArrayServiciosRutaDinamica[key]){
                      /*$scope.markerArrayServiciosRutaDinamica[key].seticon({
                        'url': url,
                        'size': { width: 30, height: 45 },
                        'anchor':  [10, 10]
                      });
                    }
                  }else{
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";
                    if($scope.markerArrayServiciosRutaDinamica[key]){
                      /*$scope.markerArrayServiciosRutaDinamica[key].seticon({
                        'url': url,
                        'size': { width: 30, height: 45 },
                        'anchor':  [10, 10]
                      });
                    }
                  }

                  if( $scope.agregarMarkerDinamico == 0){
                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                        anchor:  [10, 10],
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayServiciosRutaDinamica[key]     = marker;
                      $scope.markerArrayServiciosRutaDinamica[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayServiciosRutaDinamica[key].lng = parseFloat(value.direccion_georeferenciada_longitud);
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "servicio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                  }
                
                }); //FIN ANGULAR FOREACH
              $scope.agregarMarkerDinamico = 1;
              document.getElementById("containerLoading").className = "fadeOut";
            }else{
              if( $scope.auxiliarMostrarDinamica == 1 ){
                console.log("dentro");
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayItemRutaDinamica       = [];
                $scope.markerArrayServiciosRutaDinamica  = [];
                $scope.markerArrayPatrimonioRutaDinamica = [];
                document.getElementById("containerLoading").className = "fadeOut";  
                $scope.auxiliarMostrarDinamica = 0;
                $scope.agregarMarkerDinamico   = 0;
              }
              
            }
/*---------------------------------------------------------------------------------------------*/
//console.log($scope.showHidePatrimonio, $scope.showHideItem);
            /*if( $scope.showHidePatrimonio == true || $scope.showHideItem == true){ //si está visible....

              if( $scope.agregarPatrimonios == 1 ){
                console.log("patrimoinio Activos....");  
              
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud); 

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  if(distancia <= $rootScope.radio)
                    var url = "www/img/patrimonios/"+value.icono+".png";
                  else
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    //$scope.markerArrayPatrimonio[key].setVisible(false);
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
                });

                $scope.agregarPatrimonios = 0;
                document.getElementById("containerLoading").className = "fadeOut";
              }

              //cambiar  a icono oculto o normaldependiendo de la distancia....
              angular.forEach($scope.markerArrayPatrimonio, function(value, key) {
                var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);

                if( $scope.markerArrayPatrimonio[key] ){
                  if(distancia <= $rootScope.radio){
                    $scope.markerArrayPatrimonio[key].setIcon({
                      'url': "www/img/patrimonios/"+value.icono+".png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                  }else{
                    $scope.markerArrayPatrimonio[key].setIcon({
                      'url': "www/img/patrimonios/"+value.icono+"_oculto.png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                      
                  }
                }    
              });

              //console.log($scope.patrimonios);
              //document.getElementById("containerLoading").className = "fadeOut";
              //Calcular distancia entre items( desde bd) y mi posicion  
              /*angular.forEach($scope.patrimonios, function(value, key) {
                var distancia = $scope.distanciaEntreCoordenadas(
                  lat,
                  long,
                  value.direccion_georeferenciada_latitud,
                  value.direccion_georeferenciada_longitud);  

                if(distancia <= $rootScope.radio){ //dentro del rango...
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  var url = "www/img/patrimonios/"+value.icono+".png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].setVisible(false);
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                  });
                  
                }else{
                 
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio_oculto;
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";   

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].setVisible(false);
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    $scope.markerArrayPatrimonio[key].setIcon({
                      'url': "http://www.sadepi.cl/webequipo/assets/img/equipo/foto_fabian_cuad.jpg",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });   

                }//FIN ELSE
              });
              //FIN ANGULAR FOREACH
            }else{
              if($scope.showHideServicio == false){
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayPatrimonio = [];
                document.getElementById("containerLoading").className = "fadeOut";
              }
              
              //console.log("esta visible, se ocultará");
              /*angular.forEach($scope.patrimonios, function(value, key) {
                if( $scope.markerArrayPatrimonio[key] )
                $scope.markerArrayPatrimonio[key].remove();
              });*/
              
              /*console.log("borrar todos los patrimonios..");
              angular.forEach($scope.markerArrayPatrimonio, function(value, key) {
                if( $scope.markerArrayPatrimonio[key] ){
                  $scope.markerArrayPatrimonio[key].remove();  
                  $scope.markerArrayPatrimonio.splice($scope.markerArrayPatrimonio.indexOf(key), 1);
                }    
              });
              console.log("markerArray: ", $scope.markerArrayPatrimonio);
              
            }*/
/*---------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------*/
            /*if( $scope.showHideServicio == true || $scope.showHideItem == true ){ //si está visible....
              
              if( $scope.agregarServicios == 1 ){
                console.log("servicios Activos....");

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);  
 

                 
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );


                    //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;

                    if(distancia <= $rootScope.radio)
                      var url = "www/img/servicios/"+value.icono+".png";
                    else
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";
                    

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                        anchor:  [10, 10],
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
                });

                $scope.agregarServicios = 0;
                document.getElementById("containerLoading").className = "fadeOut";
              }
              
              //cambiar  a icono oculto o normaldependiendo de la distancia....
              angular.forEach($scope.markerArrayServiciosTuristicos, function(value, key) {
                var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);

                if( $scope.markerArrayServiciosTuristicos[key] ){
                  if(distancia <= $rootScope.radio){
                    $scope.markerArrayServiciosTuristicos[key].setIcon({
                      'url': "www/img/servicios/"+value.icono+".png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                  }else{
                    $scope.markerArrayServiciosTuristicos[key].setIcon({
                      'url': "www/img/servicios/"+value.icono+"Oculto.png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                      
                  }
                }    
              });

              
              //Calcular distancia entre items( desde bd) y mi posicion
              /*angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                var distancia = $scope.distanciaEntreCoordenadas(
                  lat,
                  long,
                  value.direccion_georeferenciada_latitud,
                  value.direccion_georeferenciada_longitud);  

                if(distancia <= $rootScope.radio){ //dentro del rango...
                  //console.log("dentro del rango, distancia: "+distancia+" , nombre marker: "+value.nombre_item_turistico);
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );


                  var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayServiciosTuristicos[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "servicio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                  });
                  
                }else{
                 
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio_oculto;

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayServiciosTuristicos[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "servicio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });   

                }//FIN ELSE
              });
              //FIN ANGULAR FOREACH
            }else{
              if($scope.showHidePatrimonio == false){
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayServiciosTuristicos = [];
                document.getElementById("containerLoading").className = "fadeOut";
              }
              
              /*angular.forEach($scope.serviciosTuristicos, function(value, key) {
                if( $scope.markerArrayServiciosTuristicos[key] )
                $scope.markerArrayServiciosTuristicos[key].remove();
              });*/

              /*angular.forEach($scope.markerArrayServiciosTuristicos, function(value, key) {
                if( $scope.markerArrayServiciosTuristicos[key] )
                $scope.markerArrayServiciosTuristicos[key].remove();
              });
            }
/*---------------------------------------------------------------------------------------------


              //dibujar la ruta
              var posision = new plugin.google.maps.LatLng(lat, long);
              $scope.miRuta.push( posision );

              if( $scope.watchPositionCont > 0 ){
                
                $rootScope.map.addPolyline({
                points: [
                  $scope.miRuta[$scope.watchPositionCont],
                  $scope.miRuta[$scope.watchPositionCont-1]
                ],
                'color' : '#AA00FF',
                'width': 3,
                'geodesic': true
                }, function(polyline) {
                  /*setTimeout(function() {
                    polyline.remove();
                  }, 3000);
                });
            
          }
          /*  
          $scope.watchPositionCont+=1;
          contador++;
          angular.forEach($scope.marcadores, function(value, key) {
            if( $scope.markerArray[key] )
            $scope.markerArray[key].remove();
          });*/
          
          /*angular.forEach($scope.patrimonios, function(value, key) {
            if( $scope.markerArrayPatrimonio[key] )
            $scope.markerArrayPatrimonio[key].remove();
          });*/
          
          /*angular.forEach($scope.serviciosTuristicos, function(value, key) {
            if( $scope.markerArrayServiciosTuristicos[key] )
            $scope.markerArrayServiciosTuristicos[key].remove();
          });*/
          

          /*$rootScope.map.getVisibleRegion(function(latLngBounds) {
            //console.log(latLngBounds.northeast.toUrlValue() + ", " + latLngBounds.southwest.toUrlValue());
            //console.log(latLngBounds.northeast.lat);
            //console.log(latLngBounds.northeast.lng);

            if ($scope.auxiliarPantalla == 0){
              $scope.auxLatPantalla = latLngBounds.northeast.toUrlValue();
              $scope.auxLngPantalla = latLngBounds.southwest.toUrlValue();

              $scope.auxiliarPantalla = 1;
            }else{
                if($scope.auxLatPantalla != latLngBounds.northeast.toUrlValue() &&
                   $scope.auxLngPantalla != latLngBounds.southwest.toUrlValue() ){

                  //actualizo las variables de lat y lng...
                  $scope.auxLatPantalla = latLngBounds.northeast.toUrlValue();
                  $scope.auxLngPantalla = latLngBounds.southwest.toUrlValue();

                  var asdq = 0;
                  //mostrar solo los patrimionios en pantalla
                  angular.forEach($scope.markerArrayPatrimonio, function(value, key) {
                    if( $scope.markerArrayPatrimonio[key] ){
                      //console.log($scope.markerArrayPatrimonio[key].lat, $scope.markerArrayPatrimonio[key].lng, latLngBounds.northeast.toUrlValue(), latLngBounds.southwest.toUrlValue());
                      if( $scope.markerArrayPatrimonio[key].lat <= latLngBounds.northeast.lat &&
                          $scope.markerArrayPatrimonio[key].lat >= latLngBounds.southwest.lat &&
                          $scope.markerArrayPatrimonio[key].lng <= latLngBounds.northeast.lng &&
                          $scope.markerArrayPatrimonio[key].lng >= latLngBounds.southwest.lng
                        ){
                          //Mostrar Patrimonio....
                          $scope.markerArrayPatrimonio[key].setVisible(true);
                          asdq++;
                      }else{
                        $scope.markerArrayPatrimonio[key].setVisible(false);
                      }
                    }
                  });

                  console.log(asdq);
                  asdq = 0;
                                  
                }
            }

          });

        });
        /* FIN WATCH POSITION
    }*/

    $scope.mostrarRecomendados = function(lat,long){
            //Obtener patrimonios Recomendacion
              $http({
                method: "POST",
                //url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
                url: "http://200.14.68.107/atacamaGo/getPatrimonioRecomendacion.php",
                data: $httpParamSerializer({
                  "id_usuario" : localStorage.getItem("id_usuario"),
                  "id_ruta"    : $rootScope.id_ruta_tipica_seleccionada,
                  "longitud"   : long,
                  "latitud"    : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  console.log(response.data);
                  $scope.patrimoniosRecomendados = response.data.patrimonio;
                  console.log("patrimoinos: ",response.data.patrimonio);

                  //Obtener Servicios Recomendacion
                  $http({
                    method: "POST",
                    //url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
                    url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosRutaDinamicaNuevo.php",
                    data: $httpParamSerializer({
                      "id_usuario" : localStorage.getItem("id_usuario"),
                      "id_ruta"    : $rootScope.id_ruta_tipica_seleccionada,
                      "longitud"   : long,
                      "latitud"    : lat
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.serviciosRecomendados = response.data.servicios;
                      console.log("Servicios: ",response.data.servicios);

                      $http({
                        method: "POST",
                        //url: "http://200.14.68.107/atacamaGo/getItemsTuristicosRecomendacion2.php",
                        url: "http://200.14.68.107/atacamaGo/getItemsTuristicosRecomendacion.php",
                        data: $httpParamSerializer({
                          "id_usuario" : localStorage.getItem("id_usuario"),
                          "longitud"   : long,
                          "latitud"    : lat
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                      }).then(function(response){ //ok si recato bien los datos
                          $scope.itemsRecomendados = response.data.item_recomendacion;
                          console.log("itemsRecomendados: ",response.data.item_recomendacion);

                          $http({
                            method: "POST",
                            url: "http://200.14.68.107/atacamaGo/getEventosDinamicos.php",
                            data: $httpParamSerializer({
                              "id_usuario" : localStorage.getItem("id_usuario"),
                              "longitud"   : long,
                              "latitud"    : lat
                            }),
                            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                          }).then(function(response){ 
                              if(response.data.resultado == "ok"){
                                $scope.eventosDinamicosRuta = response.data.eventos;
                                console.log("eventosDinamicos:",response.data.eventos);

                                //llenar mapa con patrimoinos y servicios recmendados...
                                //$rootScope.map.clear();
                                //$rootScope.map.off();

                                //dibujar ruta en el mapa....
                                $scope.ruta = [];
                                angular.forEach($rootScope.coorRutaTipica, function(value, key) {
                                    var inicio = new plugin.google.maps.LatLng(value.inicio_lat,value.inicio_lng);
                                    var fin    = new plugin.google.maps.LatLng(value.fin_lat,value.fin_lng);
                                    $scope.ruta.push(inicio);
                                    $scope.ruta.push(fin);            
                                });
                                  
                                var idx = 0;
                                $rootScope.map.addPolyline({
                                   'points': $scope.ruta,
                                   'color' : "red",
                                   'width': 3,
                                   'geodesic': true
                                }, function(polyline) {
                                   polyline.on(plugin.google.maps.event.OVERLAY_CLICK, function() {
                                   });
                                });

                                //ELiminar marcadores de Servicios markerArrayServiciosTuristicosRecomendados
                                angular.forEach($scope.markerArrayServiciosTuristicosRecomendados, function(value, key) { 
                                  $scope.markerArrayServiciosTuristicosRecomendados[key].remove();
                                });
                                $scope.markerArrayServiciosTuristicosRecomendados = [];
                                
                                angular.forEach($scope.serviciosRecomendados, function(value, key) {  
                                 
                                      var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.direccion_georeferenciada_latitud),
                                        parseFloat(value.direccion_georeferenciada_longitud) );

                                      //var url = "www/img/servicios/"+value.icono+".png";
                                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                      $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': value.nombre_item_turistico,
                                        'draggable': true,
                                        'anchor':  [30, 45],
                                        icon: {
                                          url: url,
                                          size: { width: 30, height: 45 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                          //accion al hacer clic en el marcador
                                          $scope.markerArrayServiciosTuristicosRecomendados[key] = marker;
                                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                            $rootScope.tipo_seleccionado = "servicio";
                                            $rootScope.item_seleccionado = value;
                                            $scope.openModalMenuAccionesModificar();
                                          });
                                      });
                                      
                                });

                                //ELiminar marcadores de Servicios markerArrayPatrimonioRecomendados
                                angular.forEach($scope.markerArrayPatrimonioRecomendados, function(value, key) { 
                                  $scope.markerArrayPatrimonioRecomendados[key].remove();
                                });
                                $scope.markerArrayPatrimonioRecomendados = [];

                                angular.forEach($scope.patrimoniosRecomendados, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': true,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayPatrimonioRecomendados[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "patrimonio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                //ELiminar marcadores de Servicios markerArrayItemRecomendados
                                angular.forEach($scope.markerArrayItemRecomendados, function(value, key) { 
                                  $scope.markerArrayItemRecomendados[key].remove();
                                });
                                $scope.markerArrayItemRecomendados = [];

                                angular.forEach($scope.itemsRecomendados, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/posicionGpsTuristaOculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': true,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayItemRecomendados[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "item";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                //llenar mapa con eventos dinamicos...
                                //ELiminar marcadores de Servicios markerArrayEventosDinamicosRuta
                                angular.forEach($scope.markerArrayEventosDinamicosRuta, function(value, key) { 
                                  $scope.markerArrayEventosDinamicosRuta[key].remove();
                                });
                                $scope.markerArrayEventosDinamicosRuta = [];
                                          
                                angular.forEach($scope.eventosDinamicosRuta, function(value, key) {
                                   
                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.georeferenciacion_evento_latitud),
                                    parseFloat(value.georeferenciacion_evento_longitud) );

                                    var url = "www/img/gps.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.titulo_evento,
                                      'draggable': true,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                        //accion al hacer clic en el marcador
                                        $scope.markerArrayEventosDinamicosRuta[key] = marker;
                                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                          $rootScope.tipo_seleccionado = "evento";
                                          $rootScope.evento_seleccionado = value;
                                          $scope.openModalEvento();
                                        });
                                    });

                                  });

                            $ionicLoading.hide().then(function(){});
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                        });
                      }, function(){ //Error de conexión
                        $rootScope.toast('Verifica tu conexión a internet', 'short');
                      });

                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });

                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });           
    };

    $scope.mostrarDinamicos = function(lat,long){
            //Obtener patrimonios Recomendacion
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/getPatrimonioRutaDinamica.php",
                data: $httpParamSerializer({
                  "id_usuario" : localStorage.getItem("id_usuario"),
                  "longitud"   : long,
                  "latitud"    : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  console.log(response.data);
                  $scope.patrimoniosDinamica = response.data.patrimonio;
                  //console.log("patrimoinos: ",response.data.patrimonio);

                  //Obtener Servicios Recomendacion
                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosRutaDinamicaNuevo.php",
                    data: $httpParamSerializer({
                      "id_usuario" : localStorage.getItem("id_usuario"),
                      "longitud"   : long,
                      "latitud"    : lat
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.serviciosDinamica = response.data.servicios;
                      //console.log("Servicios: ",response.data.servicios);

                      $http({
                        method: "POST",
                        url: "http://200.14.68.107/atacamaGo/getItemTuristicoDinamico.php",
                        data: $httpParamSerializer({
                          "id_usuario" : localStorage.getItem("id_usuario"),
                          "longitud"   : long,
                          "latitud"    : lat
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                      }).then(function(response){
                          $scope.itemDinamico = response.data.item_dinamico;

                          $http({
                            method: "POST",
                            url: "http://200.14.68.107/atacamaGo/getEventosDinamicos.php",
                            data: $httpParamSerializer({
                              "id_usuario" : localStorage.getItem("id_usuario"),
                              "longitud"   : long,
                              "latitud"    : lat
                            }),
                            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                          }).then(function(response){ 
                              if(response.data.resultado == "ok"){
                                $scope.eventosDinamicos = response.data.eventos;
                                console.log($scope.eventosDinamicos);
                                
                                //llenar mapa con patrimoinos y servicios recmendados...
                                /*$rootScope.map.clear();
                                $rootScope.map.off();*/
                                
                                //ELiminar marcadores de Servicios markerArrayServiciosTuristicosDinamicos
                                angular.forEach($scope.markerArrayServiciosTuristicosDinamicos, function(value, key) { 
                                  $scope.markerArrayServiciosTuristicosDinamicos[key].remove();
                                });
                                $scope.markerArrayServiciosTuristicosDinamicos = [];

                                angular.forEach($scope.serviciosDinamica, function(value, key) {  
                                 
                                      var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.direccion_georeferenciada_latitud),
                                        parseFloat(value.direccion_georeferenciada_longitud) );

                                      //var url = "www/img/servicios/"+value.icono+".png";
                                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                      $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': value.nombre_item_turistico,
                                        'draggable': true,
                                        'anchor':  [30, 45],
                                        icon: {
                                          url: url,
                                          size: { width: 30, height: 45 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                          //accion al hacer clic en el marcador
                                          $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                            $rootScope.tipo_seleccionado = "servicio";
                                            $rootScope.item_seleccionado = value;
                                            $scope.openModalMenuAccionesModificar();
                                          });
                                      });
                                      
                                });

                                //ELiminar marcadores de markerArrayPatrimonioDinamicos
                                angular.forEach($scope.markerArrayPatrimonioDinamicos, function(value, key) { 
                                  $scope.markerArrayPatrimonioDinamicos[key].remove();
                                });
                                $scope.markerArrayPatrimonioDinamicos = [];

                                angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': true,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "patrimonio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                //ELiminar marcadores de markerItemDinamicos
                                angular.forEach($scope.markerItemDinamicos, function(value, key) { 
                                  $scope.markerItemDinamicos[key].remove();
                                });
                                $scope.markerItemDinamicos = [];

                                angular.forEach($scope.itemDinamico, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/posicionGpsTuristaOculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': true,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerItemDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "item";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });


                                /**************MOSTRAR SERVICIOS Y PATRIMONIOS FILTRADOS X EL USUARIO**************/
                                
                                //ELiminar marcadores de markerArrayServiciosTuristicos
                                angular.forEach($scope.markerArrayServiciosTuristicos, function(value, key) { 
                                  $scope.markerArrayServiciosTuristicos[key].remove();
                                });

                                $scope.markerArrayServiciosTuristicos = [];

                                angular.forEach($scope.serviciosTuristicos, function(value, key) {    
                                 
                                      var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.direccion_georeferenciada_latitud),
                                        parseFloat(value.direccion_georeferenciada_longitud) );

                                      //var url = "www/img/servicios/"+value.icono+".png";
                                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                      $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': value.nombre_item_turistico,
                                        'draggable': true,
                                        'anchor':  [30, 45],
                                        icon: {
                                          url: url,
                                          size: { width: 30, height: 45 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                          //accion al hacer clic en el marcador
                                          $scope.markerArrayServiciosTuristicos[key] = marker;
                                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                            $rootScope.tipo_seleccionado = "servicio";
                                            $rootScope.item_seleccionado = value;
                                            $scope.openModalMenuAccionesModificar();
                                          });
                                      });
                                      
                                });

                                //ELiminar marcadores de markerArrayPatrimonio
                                angular.forEach($scope.markerArrayPatrimonio, function(value, key) { 
                                  $scope.markerArrayPatrimonio[key].remove();
                                });

                                $scope.markerArrayPatrimonio = [];

                                angular.forEach($scope.patrimonios, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': true,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayPatrimonio[key] = marker;
                                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "patrimonio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                
                               
                                //llenar mapa con eventos dinamicos...
                                //ELiminar marcadores de markerArrayEventosDinamicos
                                angular.forEach($scope.markerArrayEventosDinamicos, function(value, key) { 
                                  $scope.markerArrayEventosDinamicos[key].remove();
                                });
                                $scope.markerArrayEventosDinamicos = [];
                                    
                                angular.forEach($scope.eventosDinamicos, function(value, key) {  
                                     
                                          var ubicacion = new plugin.google.maps.LatLng(
                                            parseFloat(value.georeferenciacion_evento_latitud),
                                            parseFloat(value.georeferenciacion_evento_longitud) );

                                          var url = "www/img/gps.png";

                                          $rootScope.map.addMarker({
                                            'position': ubicacion,
                                            'title': value.titulo_evento,
                                            'draggable': true,
                                            'anchor':  [30, 45],
                                            icon: {
                                              url: url,
                                              size: { width: 30, height: 45 },
                                            },
                                            zIndex: 1
                                          },function(marker) {
                                              //accion al hacer clic en el marcador
                                              $scope.markerArrayEventosDinamicos[key] = marker;
                                              marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                                $rootScope.tipo_seleccionado = "evento";
                                                $rootScope.evento_seleccionado = value;
                                                $scope.openModalEvento();
                                              });
                                          });
                                          
                                });
                          }
                          /***************************************************************************************/
                            document.getElementById("containerLoading").className = "fadeOut";
                          }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                        });

                      }, function(){ //Error de conexión
                        $rootScope.toast('Verifica tu conexión a internet', 'short');
                      });                       

                    }, function(){ //Error de conexión
                      $rootScope.toast('Verifica tu conexión a internet', 'short');
                    });

                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });           
    };


    $scope.terminarAventura = function(){
      if($scope.btnTerminarAventura == false){ //oculto, mostrar
        $scope.btnTerminarAventura = true;
      }else{ //si esta visible (true), ocultar (false)
        $scope.btnTerminarAventura = false;
      }

      console.log("terminar vaventura....");
      $scope.entrarUnaVezDinamico = 0;
      $scope.rutaDinamica = false;
        
      navigator.geolocation.clearWatch($scope.watchRutaDinamica);
      $scope.watchRutaDinamica = null;

      //quitar filtros de marcadores....
      angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodos = false;
      $scope.patrimonios = [];
      $scope.markerArrayPatrimonio = [];

      angular.forEach($scope.tipo_servicio, function(value, key) {
        value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodosServicio = false;
      $scope.serviciosTuristicos = [];
      $scope.markerArrayServiciosTuristicos = [];


      $rootScope.map.clear();
      $rootScope.map.off();
      $rootScope.id_ruta_recorrida_turista = null;

      navigator.geolocation.clearWatch($scope.watchRutaTipica);
      $scope.watchRutaTipica = null;
    }

    $rootScope.restaurarMarcadoresMapa = function(){
      $rootScope.map.clear();
      $rootScope.map.off();
      $scope.markerArrayServiciosTuristicos = [];

              angular.forEach($scope.serviciosTuristicos, function(value, key) {  
               
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/servicios/"+value.icono+".png";
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': true,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
              });

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
              });

              $scope.markerArrayItemUsuario = [];
            
              angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                  
                var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

                var url = "www/img/posicionGpsTuristaOculto.png";

                $rootScope.map.addMarker({
                  'position': ubicacion,
                  'title': value.nombre_item_turistico,
                  'draggable': true,
                  'anchor':  [30, 45],
                  icon: {
                    url: url,
                    size: { width: 30, height: 45 },
                  },
                  zIndex: 1
                },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayItemUsuario[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "item";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                  });
                });
                                      
              });

              $scope.markerArrayItemTotal = [];
            
              angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': true,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemTotal[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
              });
    }

    $rootScope.existeServicioMapa = function (id_servicio) {
      //console.log(id_servicio);
      var resultado = false;
      angular.forEach($scope.serviciosTuristicos, function(value, key) {
        //console.log(value.id);
        if(value.id == id_servicio){
          resultado = true;
        }
      });

      angular.forEach($scope.serviciosDinamica, function(value, key) {
        //console.log(value.id);
        resultado = true;
      });

      return resultado;
    }

    /*$ionicPlatform.onHardwareBackButton(function() {
      //ionic.Platform.exitApp();
    });*/

    /*$ionicPlatform.registerBackButtonAction(function (event) {}, 100);
    $rootScope.flagDesdeNuevoHomeServicios  = true;
      $rootScope.flagDesdeNuevoHomeAtractivos = true;
      //$state.go("nuevoHome");*/

    
  });
}]);