// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ionic-modal-select','ionic-native-transitions','ionic-datepicker','ngCordova','ionic.rating','ngImageCompress','ionicLazyLoad','ngCordovaOauth'])

.run(function($ionicPlatform,$cordovaToast,$rootScope,$state) {
  $ionicPlatform.ready(function() {

    $rootScope.backToMenuEventos = false;

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    /**
    * @param  {[string] mensaje                       [contiene el mensaje a mostrar]}
    * @param  {[string] duracion  {long|short}        [duración del mensaje]}
    * @param  {[string] ubicacion {top|center|bottom} [ubicaiocn del mensaje]}
    * @return {[void]}      
    **/

    
    $rootScope.toast = function(texto,duracion) {
      console.log(texto);
      $cordovaToast.show(texto, duracion, 'bottom').then(function(success) {
        //console.log("The toast was shown");
      }, function (error) {
        //console.log("The toast was not shown due to " + error);
      });
    };

    if( localStorage.getItem("id_usuario") && localStorage.getItem("id_agenda") ){
          $state.go('app.nuevoHome');
    }
    
  });
})


.config(function($stateProvider, $ionicConfigProvider,$urlRouterProvider,ionicDatePickerProvider,$sceDelegateProvider,$compileProvider,$ionicConfigProvider) {
  
  /*--------------- Seccion optimizacion app --------------*/
  
  //habilitar deshabiliar scrolling mediante js (si es false es scrolling nativo)
  $ionicConfigProvider.scrolling.jsScrolling(false);
  
  //en produccion para que no pase variables $scope al DOM
  //$compileProvider.debugInfoEnabled(false);
  
  /*--------------- Seccion optimizacion app --------------*/

  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http://200.14.68.107/**'
  ]);

  if( ionic.Platform.platform() == 'android' ){
    var datePickerObj = {
      inputDate: new Date(),
      setLabel: 'Guardar',
      todayLabel: 'Hoy',
      closeLabel: 'x',
      mondayFirst: false,
      weeksList: ["D", "L", "M", "M", "J", "V", "S"],
      monthsList: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      templateType: 'popup',
      from: new Date(1900,1,1),
      to: new Date(2018, 8, 1),
      showTodayButton: true,
      dateFormat: 'dd / MM / yyyy',
      closeOnSelect: false
    };
  }else{
    var datePickerObj = {
      inputDate: new Date(),
      setLabel: 'Seleccionar',
      todayLabel: 'Hoy',
      closeLabel: 'x',
      mondayFirst: false,
      weeksList: ["D", "L", "M", "M", "J", "V", "S"],
      monthsList: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      templateType: 'popup',
      from: new Date(1900,1,1),
      to: new Date(2018, 8, 1),
      showTodayButton: true,
      dateFormat: 'dd-MM-yyyy',
      closeOnSelect: false
    };
  }

  ionicDatePickerProvider.configDatePicker(datePickerObj);

  if (ionic.Platform.isAndroid()) {
    $ionicConfigProvider.scrolling.jsScrolling(false);
  }
  
  $stateProvider
    .state('login', {
      url: '/login',
      cache:false,
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })
    .state('registro', {
      url: '/registro',
      cache:false,
      templateUrl: 'templates/registro.html',
      controller: 'registroCtrl'

    })
    .state('misDatos', {
      url: '/misDatos',
      cache:false,
      templateUrl: 'templates/misDatos.html',
      //controller: 'misDatosCtrl'
    })
    .state('configuracion', {
      url: '/configuracion',
      cache:false,
      templateUrl: 'templates/configuracion.html'
    })
    .state('preferencias', {
      url: '/preferencias',
      cache:false,
      templateUrl: 'templates/preferencias.html'
    })
    .state('misRutas', {
      url: '/misRutas',
      cache:false,
      templateUrl: 'templates/misRutas.html'
    })
    .state('rutasTipicas', {
      url: '/rutasTipicas',
      cache:false,
      templateUrl: 'templates/rutasTipicas.html'
    })
    .state('rutasCompartidas', {
      url: '/rutasCompartidas',
      cache:false,
      templateUrl: 'templates/rutasCompartidas.html'
    })
    .state('menuAcciones', {
      url: '/menuAcciones',
      cache:false,
      templateUrl: 'templates/menuAcciones.html'
    })
    .state('subirImagen', {
      url: '/subirImagen',
      cache:false,
      templateUrl: 'templates/subirImagen.html'
    })
    .state('agregarVideo', {
      url: '/agregarVideo',
      cache:false,
      templateUrl: 'templates/agregarVideo.html'
    })
    .state('agregarAudio', {
      url: '/agregarAudio',
      cache:false,
      templateUrl: 'templates/agregarAudio.html'
    })
    .state('agregarDescripcion', {
      url: '/agregarDescripcion',
      cache:false,
      templateUrl: 'templates/agregarDescripcion.html'
    })
    .state('agregarComentario', {
      url: '/agregarComentario',
      templateUrl: 'templates/agregarComentario.html'
    })
    .state('agregarCalificar', {
      url: '/agregarCalificar',
      cache:false,
      templateUrl: 'templates/agregarCalificar.html'
    })

    .state('menuAccionesModificar', {
      url: '/menuAccionesModificar',
      cache: false,
      templateUrl: 'templates/menuAccionesModificar.html'
    })
    .state('modificarImagen', {
      url: '/modificarImagen',
      cache:false,
      templateUrl: 'templates/modificarImagen.html'
    })
    .state('modificarVideo', {
      url: '/modificarVideo',
      cache:false,
      templateUrl: 'templates/modificarVideo.html'
    })
    .state('modificarAudio', {
      url: '/modificarAudio',
      cache:false,
      templateUrl: 'templates/modificarAudio.html'
    })
    .state('modificarDescripcion', {
      url: '/modificarDescripcion',
      cache:false,
      templateUrl: 'templates/modificarDescripcion.html'
    })
    .state('modificarComentario', {
      url: '/modificarComentario',
      cache:false,
      templateUrl: 'templates/modificarComentario.html'
    })
    .state('modificarCalificar', {
      url: '/modificarCalificar',
      cache:false,
      templateUrl: 'templates/modificarCalificar.html'
    })

    .state('crearMarcador', {
      url: '/crearMarcador',
      cache:false,
      templateUrl: 'templates/crearMarcador.html'
    })

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('app.search', {
      url: '/search',
      views: {
        'menuContent': {
          templateUrl: 'templates/search.html'
        }
      }
    })

    .state('app.home', {
        url: '/home',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
            controller: 'homeCtrl'
          }
        }
      })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.nuevoHome', {
      url: '/nuevoHome',
      cache:true,
      views: {
        'menuContent': {
          templateUrl: 'templates/nuevoHome.html',
          controller: 'nuevoHomeCtrl'
        }
      }
    })

    .state('dejaTuHuella', {
      url: '/dejaTuHuella',
      // abstract: true,
      templateUrl: 'templates/dejaTuHuella.html',
      controller: 'dejaTuHuellaCtrl'
    })

    .state('misRecorridos', {
      url: '/misRecorridos',
      // abstract: true,
      templateUrl: 'templates/misRecorridos.html',
      controller: 'misRecorridosCtrl'
    })

    .state('iniciarMiRecorrido', {
      url: '/iniciarMiRecorrido',
      // abstract: true,
      templateUrl: 'templates/iniciarMiRecorrido.html',
      controller: 'iniciarMiRecorridoCtrl'
    })

    .state('reservaVer', {
      url: '/reservaVer',
      // abstract: true,
      templateUrl: 'templates/reservaVer.html',
      controller: 'reservaVerCtrl'
    })

    .state('app.reservas', {
      url: '/reservas',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/reservas.html',
          controller: 'reservasCtrl'
        }
      }
    })

    .state('eventoVer', {
      url: '/eventoVer',
      // abstract: true,
      templateUrl: 'templates/eventoVer.html',
      controller: 'eventoVerCtrl'
    })

    .state('serviciosAtractivos', {
      url: '/serviciosAtractivos',
      // abstract: true,
      templateUrl: 'templates/serviciosAtractivos.html',
      controller: 'serviciosAtractivosCtrl'
    })

    .state('app.tutorial', {
      url: '/tutorial',
      views: {
        'menuContent': {
          templateUrl: 'templates/tutorial.html',
          controller: 'tutorialCtrl'
        }
      }
    })

    .state('recorrerRutaTipica', {
      url: '/recorrerRutaTipica',
      // abstract: true,
      templateUrl: 'templates/recorrerRutaTipica.html',
      controller: 'recorrerRutaTipicaCtrl'
    })

    .state('recorrerRutaTipicaOffline', {
      url: '/recorrerRutaTipicaOffline',
      // abstract: true,
      templateUrl: 'templates/recorrerRutaTipicaOffline.html',
      controller: 'recorrerRutaTipicaOfflineCtrl'
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });



  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
})

.directive('keyboardCssRedraw', function($interval){
    return {
        restrict: 'A',
        link: link
    };

    function link(scope, element, attrs) {
        var interval = setInterval(function(){
            redraw(element[0]);
        }, 30);
        scope.$on('$destroy', function(){
            clearInterval(interval);
        });
    }

    // force redraw
    function redraw(element) {
        element.style.zIndex = 1;
        element.offsetHeight;
        element.style.zIndex = '';
    }
});