angular.module('starter.controllers')


.controller('experienciaAtacamaComentarCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce) {
    console.log("detalle experiencia controller: "+ $rootScope.id_producto_servicio_turistico);
    $rootScope.experienciaDetalleMostrar = null;
    $scope.itemSeleccionado = $rootScope.experienciaSeleccionadaNuevoHome;

    $scope.comentarios         = [];
    $scope.item                = [];
    $scope.foto_perfil_usuario = "";
    $scope.cantComentarios     = 2; //inicializa en 2 para cuando se llamela fx se rescaten 20...
    $scope.comentarios         = [];
    $scope.comentariosTotal    = [];
    $scope.id_usuario          = localStorage.getItem("id_usuario");
    $scope.item                = [];
    $scope.item.comentario     = "";

    $scope.esAndroid = false;
    if( ionic.Platform.isAndroid() ){
      $scope.esAndroid = true;
    }else{
      $scope.esAndroid = false;
    }


    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getComentarioPaqueteTuristico.php",
      data: $httpParamSerializer({
        "id_producto_servicio_turistico" : $scope.itemSeleccionado.id_producto_servicio_turistico,
        "id_usuario"                     : localStorage.getItem("id_usuario")
    }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si guardó correctamente.
      //console.log(response);
      if(response.data.resultado == "ok"){
        //$scope.comentarios         = response.data.comentario;

        $scope.comentariosTotal    = response.data.comentario;
        //obtengo los 10 primeros comentarios
        angular.forEach($scope.comentariosTotal, function(value, key) {
          if( key < 4){
            $scope.comentarios.push($scope.comentariosTotal[key]);    
          }
        });
        
        //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
        if( response.data.foto_perfil_usuario.includes("https") ){
          $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
        }else{
          $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
        }

      }else if ( response.data.resultado == "no data" ){
        //$rootScope.toast('lugar aún no tiene comentarios', 'short');
        $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/" + response.data.foto_perfil_usuario;
      }else{
        $rootScope.toast('error, intenta nuevamente', 'short');
      }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short')
    });


    $scope.cerrarModalExperienciaAtacamaComentar = function(){
    	 $rootScope.modalExperienciaAtacamaComentar.hide();
  	}

  	$scope.goToNuevoHome = function(){
      $rootScope.modalDetalleExperiencia.remove();
      $rootScope.modalExperienciaAtacamaComentar.hide();
    }


    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.addComentario = function(item){
      $ionicLoading.show({
          template: 'Cargando...'
      });

      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      navigator.geolocation.getCurrentPosition(
        function(position){
          var lat  = position.coords.latitude;
          var long = position.coords.longitude;
          $scope.addComentarioGeo(lat,long,item);
        },function(err) {
          $scope.addComentarioGeo(0,0,item);
        },posOptions
      );
    }

    $scope.addComentarioGeo = function(lat,long,item){   

      if(item.comentario || item.comentario != "" ){
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addComentarioPaqueteTuristico.php",
              data: $httpParamSerializer({
                "id_producto_servicio_turistico" : $scope.itemSeleccionado.id_producto_servicio_turistico,
                "id_usuario"                     : localStorage.getItem("id_usuario"),
                "comentario"                     : item.comentario,
                "lat"                            : lat,
                "long"                           : long
            }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
              console.log(response);
              if(response.data.resultado == "ok"){
                $rootScope.toast('comentario agregado correctamente', 'short');
                  /*$scope.comentarios.push({
                    "texto_comentario" : item.comentario,
                    "fecha_comentario" : "Ahora",
                    "nombre"           : "",
                    "apellido_paterno" : "",
                    "foto_perfil"      : $scope.foto_perfil_usuario
                  });
                  $scope.item.comentario = "";*/

                  $scope.comentarios.unshift({
                    "foto_perfil"      : $scope.foto_perfil_usuario,
                    "texto_comentario" : item.comentario,
                    "id_usuario"       : $scope.id_usuario,
                    "id"               : response.data.id
                  });

                  $scope.comentariosTotal.unshift({
                    "foto_perfil"      : $scope.foto_perfil_usuario,
                    "texto_comentario" : item.comentario,
                    "id_usuario"       : $scope.id_usuario,
                    "id"               : response.data.id
                  });
                  $scope.item.comentario = "";

              }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
              }
              $ionicLoading.hide();
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
              $ionicLoading.hide();
            });


      }else{
        $ionicLoading.hide();
        $rootScope.toast('Debes escribir un comentario', 'short');
      }
    }

    $scope.masComentarios = function(cantComentarios){
      $scope.comentarios = [];
      //obtengo los comentarios 20, 30, 40, etc...
      console.log(cantComentarios);
      console.log($scope.comentariosTotal);
      angular.forEach($scope.comentariosTotal, function(value, key) {
        if( key < (cantComentarios*4) ){
          $scope.comentarios.push($scope.comentariosTotal[key]);    
        }
      });
      $scope.cantComentarios ++;
    }

    $scope.deleteComentario = function(comentario){
      console.log(comentario);

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/eliminarComentarioExperiencia.php",
        data: $httpParamSerializer({
          "id_comentario_experiencia" : comentario.id,
      }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guard√≥ correctamente.
        console.log(response);
        if(response.data.resultado == "ok"){

          $scope.comentariosTotal = [];
          $scope.comentarios      = [];
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getComentarioPaqueteTuristico.php",
            data: $httpParamSerializer({
              "id_producto_servicio_turistico" : $scope.itemSeleccionado.id_producto_servicio_turistico,
              "id_usuario"                     : localStorage.getItem("id_usuario")
          }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si guard√≥ correctamente.
            //console.log(response);
            if(response.data.resultado == "ok"){
              //$scope.comentarios         = response.data.comentario;

              $scope.comentariosTotal    = response.data.comentario;
              //obtengo los 10 primeros comentarios
              angular.forEach($scope.comentariosTotal, function(value, key) {
                if( key < 4){
                  $scope.comentarios.push($scope.comentariosTotal[key]);    
                }
              });
              
              //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
              if (response.data.foto_perfil_usuario == null){
                $scope.foto_perfil_usuario = "img/perfil.jpg";
              }else{
                if( response.data.foto_perfil_usuario.includes("https") ){
                  $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
                }else if( response.data.foto_perfil_usuario != null ){
                  $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
                }
              }
              

              console.log($scope.foto_perfil_usuario);

            }else if ( response.data.resultado == "no data" ){
              //$rootScope.toast('lugar a√∫n no tiene comentarios', 'short');
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/" + response.data.foto_perfil_usuario;
            }else{
              $rootScope.toast('error, intenta nuevamente', 'short');
            }
          }, function(){ //Error de conexi√≥n
            $rootScope.toast('Verifica tu conexi√≥n a internet', 'short')
          });


        }else{
          $rootScope.toast('error, intenta nuevamente', 'short');
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexi√≥n
        $rootScope.toast('Verifica tu conexi√≥n a internet', 'short');
        $ionicLoading.hide();
      });
    }
    

}]);