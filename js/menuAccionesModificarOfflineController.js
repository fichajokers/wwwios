angular.module('starter.controllers')


.controller('menuAccionesModificarOfflineCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$httpParamSerializer','$timeout','$sce','$cordovaCamera','$cordovaFileTransfer','$cordovaSocialSharing','$ionicPopup','$ionicLoading','$ionicNativeTransitions', function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$httpParamSerializer,$timeout,$sce,$cordovaCamera,$cordovaFileTransfer,$cordovaSocialSharing,$ionicPopup,$ionicLoading,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called

  width = window.screen.width;
  console.log("width: ",width);


  $scope.tieneImagen = false;


  /*384-767 HUAWEI*/
    if( width > 321 && width <= 360 ){
      console.log("esto es un huawei");
      $scope.contenedorServicio = {
        "width"             : "100%",  
        "height"            : "60px",
        "background-color"  : "#7162AB",  
        "margin-top"        : "-5px", 
        "margin-top"        : "-2%",
      }

      $scope.contenedorItem = {
        "width"             : "100%",  
        "height"            : "60px",  
        "background-color"  : "#3A2B86",
        "margin-top"        : "-2%",
      }

      $scope.contenedorAtractivo = {
        "width"             : "100%",  
        "height"            : "60px",  
        "background-color"  : "#745A3A",   
        "margin-top"        : "-2%",
      };

      $scope.icono = {
        "width"         : "45px",
        "margin-top"    : "0%",
        "float"         : "left",
        "margin-left"   : "2%",
        "margin-right"  : "2%",
        "z-index"       : "10",
        "margin-bottom" : "3%",
        "height"        : "43px",
      };

      $scope.iconoItem = {
        'width': '50px',
        'margin': '0% 2% 3%',
        'float': 'left',
        'z-index': '10',
        'height': '50px',
      };

      $scope.noVisitado = {
        'margin-left': '-8%',
        'margin-top': '0.5%',
      };

      $scope.visitado = {
        'margin-left': '-7%',
        'margin-top': '0.5%',
      };

      $scope.visitadoItem = {
        'margin-left': '-5%',
        'margin-top': '2.5%',
      };

      $scope.noVisitadoItem = {
        'margin-left': '-7%',
        'margin-top': '2.5%',
      };

      $scope.parrafoVisitanos ={
        'margin-left': '14%',
      }
    }

    /*lenovo tablet*/
    if( width > 361 && width <= 480 ){
      console.log("esto es un talet");

      $scope.contenedorServicio = {
        "width"             : "100%",  
        "height"            : "60px",
        "background-color"  : "#7162AB",  
        "margin-top"        : "-5px", 
        "margin-top"        : "-1%",
      };

      $scope.contenedorItem = {
        "width"             : "100%",  
        "height"            : "65px",  
        "background-color"  : "#3A2B86",
        "margin-top"        : "-1%",
      };

      $scope.contenedorAtractivo = {
        "width"             : "100%",  
        "height"            : "65px",  
        "background-color"  : "#745A3A",   
        "margin-top"        : "-1%",
      };

      $scope.icono = {
        "width"         : "47px",
        "margin-top"    : "0%",
        "float"         : "left",
        "margin-left"   : "2%",
        "margin-right"  : "4%",
        "z-index"       : "10",
        "margin-bottom" : "3%",
        "height"        : "47px",
      };

      $scope.iconoItem = {
        'width': '55px',
        'margin': '0% 4% 3%',
        'float': 'left',
        'z-index': '10',
        'height': '55px',
      };

      $scope.noVisitadoItem = {
        'margin-left': '-6%',
        'margin-top': '2.5%',
      };

      $scope.noVisitado = {
        'margin-left': '-8%',
        'margin-top': '1.5%',
      };

      $scope.visitado = {
        'margin-left': '-7%',
        'margin-top': '1.5%',
      };

      $scope.visitadoItem = {
        'margin-left': '-4%',
        'margin-top': '2.5%',
      };

      $scope.parrafoVisitanos ={
        'margin-left': '12%',
      }
    }
    
    /*lenovo tablet*/
    if( width > 481 && width <= 600 ){
      console.log("esto es un talet");

      $scope.contenedorServicio = {
        "width"             : "100%",  
        "height"            : "60px",
        "background-color"  : "#7162AB",  
        "margin-top"        : "-5px", 
        "margin-top"        : "-1%",
      };

      $scope.contenedorItem = {
        "width"             : "100%",  
        "height"            : "70px",  
        "background-color"  : "#3A2B86",
        "margin-top"        : "-1%",
      };

      $scope.contenedorAtractivo = {
        "width"             : "100%",  
        "height"            : "70px",  
        "background-color"  : "#745A3A",
        "margin-top"        : "-5px",   
        "margin-top"        : "-1%",
      };

      $scope.icono = {
        "width"         : "47px",
        "margin-top"    : "0%",
        "float"         : "left",
        "margin-left"   : "4%",
        "margin-right"  : "4%",
        "z-index"       : "10",
        "margin-bottom" : "3%",
        "height"        : "47px",
      };

      $scope.iconoItem = {
        'width': '60px',
        'margin': '0% 5% 6%',
        'float': 'left',
        'z-index': '10',
        'height': '60px',
      };

      $scope.visitadoItem = {
        'margin-left': '-3%',
        'margin-top': '3.5%',
      };

      $scope.noVisitadoItem = {
        'margin-left': '-4.5%',
        'margin-top': '3.5%',
      };

      $scope.noVisitado = {
        'margin-left': '-6%',
        'margin-top': '1.5%',
      };

      $scope.visitado = {
        'margin-left': '-5%',
        'margin-top': '1.5%',
      };

      $scope.parrafoVisitanos ={
        'margin-left': '9%',
      }
    }

  console.log("OFFLINE ACCIONES");
  console.log($rootScope.item);
  console.log($rootScope.tipo_seleccionado);
  console.log($rootScope.item.descripcion);


  $scope.cerrarMenuAccionesModificarOffline = function(){

        //$rootScope.volverExploraYDescubre = true;
        $rootScope.modalMenuAccionesModificar.remove();
        
        $rootScope.volverExploraYDescubreRutas = true;
        
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
    }

  $scope.atrasMenuAccionesModificarOffline = function(){
      
      $rootScope.modalMenuAccionesModificar.remove();

  }
}]);