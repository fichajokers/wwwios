angular.module('starter.controllers')


.controller('rutasTipicasOfflineCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.agregarMarkerDinamico       = 0;
  $scope.markerArrayItemRutaDinamica = [];
  $scope.calificacion                = {};
  $scope.filtro                      = "todas";
  

    //conseguir coordenadas de item_turistico
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getItemTuristico.php",
      data: $httpParamSerializer({
        "id_usuario": localStorage.getItem("id_usuario"),
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
      if(response.data.resultado == "ok"){
        console.log(response.data.item_turistico);
        $scope.marcadores = response.data.item_turistico;
      }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

  	$http({
    	method: "POST",
    	url: "http://200.14.68.107/atacamaGo/getRutasTipicas.php",
    	data: $httpParamSerializer({
      	"id_usuario": localStorage.getItem("id_usuario")
    	}),
    	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  	}).then(function(response){ //ok si recato bien los datos
      	$scope.rutas_tipicas     = response.data.rutas_tipicas;
        $scope.rutas_tipicas_aux = response.data.rutas_tipicas;
      	console.log($scope.rutas_tipicas);
  	}, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet', 'short');
  	});

    $scope.filtrar = function(filtro){
      //console.log($scope.filtro);
      console.log(filtro);
      $scope.rutas_tipicas = {};
      if(filtro == 'todas'){
       
        $scope.rutas_tipicas = $scope.rutas_tipicas_aux;
      }

      if(filtro == 'enRecorrido'){
        var contador = 0;
        angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
          if(value.estado_ruta == 'en curso'){
            $scope.rutas_tipicas[contador] = value;
            contador +=1;
          }      
        });

      }

      if(filtro == 'recorridas'){
        var contador = 0;
        angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
          if(value.estado_ruta == 'completa'){

            $scope.rutas_tipicas[contador] = value;
            contador +=1;
          }      
        });

        
      }

      if(filtro == 'sinRecorrer'){
        var contador = 0;
        angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
          if(value.estado_ruta == null){
            $scope.rutas_tipicas[contador] = value;
            contador +=1;
          }      
        });
      }
      console.log("Rutas_tipicas:",$scope.rutas_tipicas);
    }

    $scope.descargarRuta = function(id_ruta){
      
      $ionicLoading.show({
        template: 'Cargando...'
      }).then(function(){
         console.log("The loading indicator is now displayed");
      });

      console.log(id_ruta);
      $rootScope.id_ruta_tipica_seleccionada = id_ruta;
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/getCoordenadasRutaTipica.php",
        data: $httpParamSerializer({
          "id_ruta": id_ruta
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si recato bien los datos
          
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
      });

    }
}]);