angular.module('starter.controllers')


.controller('contactoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.contacto = "";

  $scope.cerrarModalContacto = function(){   
    $rootScope.modalContacto.hide();
    if(ionic.Platform.isWebView()){
      $rootScope.map.setClickable(true);  
    }
  }

  $scope.sendContacto = function(contacto){
    console.log(contacto);
    console.log($scope.contacto);
    if( $scope.contacto == undefined || $scope.contacto == "" ){
      $rootScope.toast('Debes llenar el campo de contacto', 'short');
    }else{
      $ionicLoading.show({
        template: 'Enviando mensaje...'
      });
      
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacama/public/usuario/contactoMovil",
          data: $httpParamSerializer({
            "id_usuario" : localStorage.getItem("id_usuario"),
            "contacto"   : $scope.contacto
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.contacto = "";
            $ionicLoading.hide();
            $rootScope.toast('Listo', 'short');
          }else{
            $rootScope.toast('Intente nuevamente', 'short');
            $scope.contacto = "";
            $ionicLoading.hide();
          }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
        });  

    }

  }
}]);