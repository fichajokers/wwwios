angular.module('starter.controllers')


.controller('loginCtrl', ['$scope','$http','$state','$ionicNativeTransitions','$ionicPopup','$httpParamSerializer','$cordovaToast','$rootScope','$cordovaPushV5','$ionicPlatform','$cordovaOauth','$ionicLoading','$cordovaPush','$ionicSlideBoxDelegate','$timeout',function($scope,$http,$state,$ionicNativeTransitions,$ionicPopup,$httpParamSerializer,$cordovaToast,$rootScope,$cordovaPushV5,$ionicPlatform,$cordovaOauth,$ionicLoading,$cordovaPush,$ionicSlideBoxDelegate,$timeout) {

  
  // With the new view caching in Ionic, Controllers are only called  
  $scope.usuario            = [];
  $scope.usuario.username   = "";
  //$scope.usuario.username = "soporte.raveno@uv.cl";
  $scope.usuario.password   = "";
  $scope.gcmMovil           = "";
  $scope.unaVez             = 0;
  $scope.primerUso          = false;
  $scope.ocultarSlider      = false;
  $scope.modalEmailEnviado  = false;

  altura = window.screen.height;
  alturaReal = window.screen.height;
  
  $scope.myStyle={
    "height" : altura+"px",
    "width"  : "100%"
  }

  $scope.estiloIonSlide={
    "height" : alturaReal+"px"
  }

  width = window.screen.width;
  console.log("width: ",width);
  
  //Determinar el primer uso de la aplicación
  if( localStorage.getItem("primerUso") != '1' ){
    localStorage.setItem("primerUso",'1');
    $scope.primerUso        = true;
  }
  $scope.$on('$ionicView.afterEnter', function(){
    if($rootScope.volverRegistro){
      $ionicSlideBoxDelegate.slide(3, 300);
    }
    
    /*if( localStorage.getItem("id_usuario") && localStorage.getItem("id_agenda")){
        
        setTimeout(function(){
          //Verificar si el usuario tiene sesion iniciada...
          $state.go('app.home');
        },1000);

        setTimeout(function(){
          //Verificar si el usuario tiene sesion iniciada...
          document.getElementById("containerLoading2").style.display = "none";
        },1000);
        
    }*/
    //document.getElementById("containerLoading2").className = "fadeOut";
    /*
    setTimeout(function(){
      //Verificar si el usuario tiene sesion iniciada...
      
      //document.getElementById("containerLoading").style.display = "none";
      document.getElementById("containerLoading").className = "fadeOut";
    },1000);*/
  });


  $scope.registro = function(){
  	$ionicNativeTransitions.stateGo('registro', {inherit:false}, {
    	"type": "slide",
    	"direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
    	"duration": 400, // in milliseconds (ms), default 400
	  });
    /*
     $scope.data = {};

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<input type="text" ng-model="data.wifi" placeholder="Email">\
      <br>\
      <textarea ng-model="data.texto" rows="4" cols="15" style="width: 100%;" placeholder="Información adicional">\
      </textarea>',
      title: 'Solocitar usuario',
      subTitle: '',
      cssClass: 'my-custom-popup',
      scope: $scope,
      buttons: [
        { text: 'Cancelar' },
        {
          text: '<b>Aceptar</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.data.wifi) {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {
              return $scope.data;
            }
          }
        }
      ]
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
      if(res){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/solicitarUsuario.php",
          data: $httpParamSerializer({
            "email" : res.wifi,
            "texto" : res.texto
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
            var alertPopup = $ionicPopup.alert({
              title: 'Aviso',
              template: 'Pronto nos contactaremos contigo'
            });

            alertPopup.then(function(res) {
              console.log('Thank you for not eating my delicious ice cream cone');
            });
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      }
    });
    */
  }

  $scope.iniciarSesion = function(usuario){

    if(usuario.username && usuario.password){
      //pasar username a lowercase... 
      //
      usuario.username = angular.lowercase(usuario.username);
      
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacama/public/sesion/doLogin",
        data: $httpParamSerializer({
          "email"    : usuario.username,
          "password" : usuario.password
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        if(response.data.href){
          console.log(response.data);
          //guardar datos del usuario en local Storage
          usuario.username = angular.lowercase(usuario.username);
          localStorage.setItem("id_usuario", response.data.id_usuario);
          localStorage.setItem("id_agenda",  response.data.id_email.id_agenda);
          localStorage.setItem("correo",  usuario.username);

          $ionicLoading.hide();
          //Conseguir y alamacenar el CGM
          
          //$scope.getGcm(response.data.id_usuario);
          //ir al home
          //alert("antes de ir al home");
          $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
            "type": "slide",
            "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
            "duration": 400, // in milliseconds (ms), default 400
          });
        }
        else{
          $rootScope.toast('Usuario y/o contraseña incorrecta', 'short');
          $ionicLoading.hide();
        }

      }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
          $ionicLoading.hide();
      });

      
    }else{
        var alertPopup = $ionicPopup.alert({
          title: 'Aviso!',
          template: 'Debes llenar todos los campos',
          okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
          okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
        });
        alertPopup.then(function(res) {
           //console.log('Thank you for not eating my delicious ice cream cone');
        });
    }
    
  }
  
  $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
  };

  $scope.recuperarContrasena = function(){
    $scope.nombre = {};
    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<p CLASS="fuenteRoboto fuenteModal">INGRESA TU EMAIL</p><div class="circuloMoradoEmail"></div><img class="imgPlaceholderLoginCandado" src="img/login/correo.png" alt=""><input class="inputFormularioModal" type="text" ng-model="nombre.nombre"><img class="logoTitulo" src="img/login/pass.png">',
      title: 'Recuperar contraseña',
      scope: $scope,
      buttons: [
        { 
            text: '<i class="icon ion-close-round"></i>',
            type:'popclose',
              onTap: function(e) {
                
              }
        },
        {
          text: '<b>Aceptar</b>',
          type: 'button-positive asd',
          onTap: function(e) {
            if (!$scope.nombre.nombre) {
              //don't allow the user to close unless he enters nombre password
              e.preventDefault();
            } else {
              console.log($scope.nombre.nombre);
              
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacama/public/usuario/recuperarPasswordUsuarioMovil",
                data: $httpParamSerializer({
                  "email"    : $scope.nombre.nombre
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                console.log(response);
                if(response.data.resultado == "ok"){
                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacamaGo/recuperarContrasena.php",
                    data: $httpParamSerializer({
                      "email"    : $scope.nombre.nombre,
                      "pass"     : response.data.pass
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si guardó correctamente.
                      console.log(response);
                      //$rootScope.toast('Email enviado correctamente', 'short');
                      $scope.modalEmailEnviado = true;
                      $timeout(function() {
                        $scope.modalEmailEnviado = false;
                      }, 800);
                      
                  }, function(){ //Error de conexión
                      $rootScope.toast('Verifica tu conexión a internet', 'short')
                  });

                }
                else{
                  $rootScope.toast('Email no encontrado', 'short');
                }

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short')
              });

            }
          }
        }
      ]
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  }
    
  $scope.pdf = function() {
    if( ionic.Platform.isIOS() )
          cordova.InAppBrowser.open('http://200.14.68.107/atacamaGo/manual/AtacamaGo6.pdf', '_blank', 'location=yes');
      else
        window.open("http://200.14.68.107/atacamaGo/manual/AtacamaGo6.pdf","_system",'location=yes');
  }
  
  $scope.getGcm = function(id_usuario){
    $ionicPlatform.ready(function () {
      var options = {
        android: {
          senderID: "928988279941"
        },
        ios: {
          alert: "true",
          badge: "true",
          sound: "true"
        },
        windows: {}
      };
      
      var androidConfig = {
          "senderID": "928988279941",
      };

      $cordovaPush.register(androidConfig).then(function(result) {
        //alert(result);
        //console.log("GCM: " + result);
      }, function(err) {
        // Error
      });

      $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
        switch(notification.event) {
          case 'registered':
            if (notification.regid.length > 0 ) {
              //alert('registration ID = ' + notification.regid);
            }
            break;

          case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            //alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
            break;

          case 'error':
            //alert('GCM error = ' + notification.msg);
            break;

          default:
            //alert('An unknown GCM event has occurred');
            break;
        }
        if($scope.unaVez == 0){
          $scope.unaVez = 1;
          $scope.gcmMovil = notification.regid;
          //alert($scope.gcmMovil);
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/addDispositivo.php",
            data: $httpParamSerializer({
              gcm        : $scope.gcmMovil,
              ssoo       : 'android',
              id_usuario : localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              console.log(response);
              
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        } 
          
      });
      /*
      //console.log($cordovaPushV5);
      // initialize
        console.log($cordovaPushV5);
        $cordovaPushV5.initialize(options).then(function() {
          alert("ENTRO A LA FUNCION $cordovaPushV5.initialize");

          // start listening for new notifications
          $cordovaPushV5.onNotification();
          // start listening for errors
          $cordovaPushV5.onError();

          // register to get registrationId
          $cordovaPushV5.register().then(function(registrationId) {
            alert("$cordovaPushV5.register()");
            console.log(registrationId);
            console.log("sdfadsfd");
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addDispositivo.php",
              data: $httpParamSerializer({
                "id_usuario" : id_usuario,
                "gcm"        : registrationId,
                "ssoo"       : ionic.Platform.platform()
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response);
                 response.resultado.id_dispositivo;
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short')
            });

          })
        });
        */
    });

  }

  /**
   * [accion cuando se registra el usuario]
   * @param  {[Array]} registro [array con toda la info del formulario]
   */
  $scope.registroFacebook = function(registro){
    console.log("function registrarse");

    //validar que esten los campos llenos...
    console.log("Datos...: ",registro);
    flagCamposLlenos = 0;
    var count = 0;
    for (var p in registro) {
      registro.hasOwnProperty(p) && count++;
    }

    if( registro.correo==null    || registro.correo== ""  || 
        registro.nombre==null    || registro.nombre== ""  || 
        registro.paterno==null   || registro.paterno== "" || 
        //registro.materno==null   || registro.materno== "" ||
        registro.password2==null || registro.password2== ""){
      $rootScope.toast("debes llenar todos los campos","short");
      flagCamposLlenos = 1;
    }
    //validar password
    if( registro.password == registro.password2 && flagCamposLlenos == 0){

      $ionicLoading.show({
        template: 'Cargando...'
      });

      /*
      var options = {
        fileKey: "perfil",
        fileName: $scope.nombreImagen,
        chunkedMode: false
      };

      $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagen.php", $scope.rutaImagen, options).then(function(result) {
          console.log("SUCCESS:")
          console.log(result.response);
      }, function(err) {
        console.log("ERROR: ");
        console.log(err);
      }, function (progress) {
          // constant progress updates
      });*/
      

      registro.correo = angular.lowercase(registro.correo);
     
      //alert(registro.materno);
      if( registro.materno == undefined){
        registro.materno = "";
      }else{
        console.log("no entro al undefined: " + registro.materno);
      }

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacama/public/usuario/guardarUsuarioMovil",
        data: $httpParamSerializer({
          "correo"     : registro.correo,
          "nombre"     : registro.nombre,
          "apellido_p" : registro.paterno,
          "apellido_m" : registro.materno,
          "password"   : registro.password,
          "pais"       : registro.pais,
          "genero"     : registro.sexo,
          "nacimiento" : registro.fechaNac,
          "foto"       : registro.foto
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        console.log(response.data.resultado);
        if(response.data.resultado == "Registrado"){
          

          $http({
            method: "POST",
            url: "http://200.14.68.107/atacama/public/usuario/mensajeBienvenidaMovil",
            data: $httpParamSerializer({
              "nombre"   : registro.nombre + " " + registro.paterno + " " + registro.materno,
              'correo'   : registro.correo,
              'pass'     : registro.password
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
            console.log(response);
            //$rootScope.toast("Contraseña enviada al correo","short");

            var usuario = [];

            usuario = {
              "username" : registro.correo,
              "password" : registro.password
            }
            
            $scope.iniciarSesion(usuario);
            $ionicLoading.hide();
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }
        else{

          var usuario = [];

          usuario = {
            "username" : registro.correo,
            "password" : registro.password
          }
          
          $scope.iniciarSesion(usuario);
          //$rootScope.toast("Usuario ya registrado, debe recuperar contraseña","short");
          $ionicLoading.hide();
        }
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet',"short");
        $ionicLoading.hide();
      });
    }else{
      $rootScope.toast("Contraseñas no coinciden","short");
      $ionicLoading.hide();
    }   
  }


  $scope.loginFacebook = function(){
    $cordovaOauth.facebook("714480612066766", ["email", "user_website", "user_location", "user_relationships"]).then(function(token) {
          //$localStorage.accessToken = result.access_token;
          //location.path("/profile");
          //console.log(result);

           $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: token.access_token, fields: "id,first_name,last_name,age_range,gender,location,picture,email", format: "json" }}).then(function(result) {
                $scope.profileData = result.data;

                //normalizar datos...
                if(result.data.email){
                  var registro = [];
                  var sexo = "";
                  var apellido = result.data.last_name;
                  var apellidos = apellido.split(/(\s+)/);

                  if(result.data.gender == "male")
                    sexo = "M";
                  else
                    sexo = "F";

                  //guardar la info.
                  registro={
                    "correo"    : result.data.email,
                    "nombre"    : result.data.first_name,
                    "paterno"   : apellidos[0],
                    "materno"   : apellidos[2],
                    "password"  : result.data.id,
                    "password2" : result.data.id,
                    "sexo"      : sexo,
                    "foto"      : result.data.picture.data.url,
                  };
                  

                  console.log("y el registro es: ");
                  console.log(registro);
                  
                  $scope.registroFacebook(registro);
                }else{
                  //decir al usuario que debe verificar el correo electronico de su cuentade facebook
                  $rootScope.toast("Debes verificar tu email con facebook","short");
                }
            }, function(error) {
                //alert("There was a problem getting your profile.  Check the logs for details.");
                $rootScope.toast("Ha ocurrido un problema, intenta nuevamente","short");
                //alert("Ha ocurrido un problema, intenta nuevamente");
                console.log(error);
            });

        }, function(error) {
            //alert("There was a problem signing in!  See the console for logs");
            $rootScope.toast("Ha ocurrido un problema, intenta nuevamente","short");
            //alert("Ha ocurrido un problema, intenta nuevamente");
            console.log(error);
        });
  }

  $scope.omitirPrimerUso = function(){
    $scope.primerUso = false;
  }

  $scope.saltarSlider = function(){
    //$scope.ocultarSlider = true;
    $ionicSlideBoxDelegate.slide(3, 500);
  }
  
}]);