angular.module('starter.controllers')


.controller('preferenciasCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$ionicLoading','$ionicModal','$ionicPlatform',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$ionicLoading,$ionicModal,$ionicPlatform) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.mostrarAcompanado        = false;
  $rootScope.cuestionario             = {};
  $scope.comida                   = "";
  $scope.tipoTours                = "";
      
  $scope.noRespondeActividades    = false;
  $scope.noRespondeComida         = false;
  $scope.noRespondeDuracion       = false;
  $scope.noRespondeGasto          = false;
  $scope.noRespondeNumeroPersonas = false;
  $scope.noRespondeSoloAcom       = false;
  $scope.noRespondeTipoServicio   = false;
  $scope.noRespondeTipoTours      = false;
  $scope.noRespondeTransporte     = false;
  $scope.noRespondeTipoViaje      = false;
  $scope.noRespondeMoneda         = false;
      
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getPreferencia.php",
    data: $httpParamSerializer({
      "id_usuario"    : localStorage.getItem("id_usuario")
    }),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    console.log(response.data)
    if(response.data.cuestionario){


        $rootScope.cuestionario.moneda         = response.data.cuestionario.moneda;
        $rootScope.cuestionario.numeroPersonas = response.data.cuestionario.numeroPersonas;
        $rootScope.cuestionario.gasto          = response.data.cuestionario.gasto;
        $rootScope.cuestionario.duracion       = response.data.cuestionario.duracion;
        
        $rootScope.cuestionario.transporte     = response.data.cuestionario.medio_transporte;
        $rootScope.cuestionario.comida         = response.data.cuestionario.tipo_alimentacion;
        $rootScope.cuestionario.tipoTours      = response.data.cuestionario.tipo_tour;

        $rootScope.cuestionario.tipoServicio   = response.data.cuestionario.id_alojamiento_perfil;
        $rootScope.cuestionario.soloAcom       = response.data.cuestionario.viaja_solo;

        // if(response.data.cuestionario.viaja_solo == true){
        //   $rootScope.cuestionario.soloAcom = "TRUE";
        // }
        // else if(response.data.cuestionario.viaja_solo == false) {
        //   $rootScope.cuestionario.soloAcom = "FALSE";
        // }
        // else if(response.data.cuestionario.viaja_solo == "nodata") {
        //   $rootScope.cuestionario.soloAcom = "nodata";
        // }


        if(response.data.cuestionario.viaja_solo_original == true){
          $rootScope.cuestionario.soloAcom = "TRUE";
        }
        else if(response.data.cuestionario.viaja_solo_original == false){
          $rootScope.cuestionario.soloAcom = "FALSE";
        }
        else if(response.data.cuestionario.viaja_solo_original == null){
          $rootScope.cuestionario.soloAcom = "nodata";
        }


        
        /*if( response.data.cuestionario.numeroPersonas != 0 )
          $rootScope.cuestionario.soloAcom = "FALSE";
        else
          $rootScope.cuestionario.soloAcom = "TRUE";
*/

        
        $rootScope.cuestionario.actividades    = {};
        $rootScope.cuestionario.tipoViaje      = {};
        
        //selecionar si viaja solo o acompañado
        /*if( $rootScope.cuestionario.numeroPersonas == 0 ){
          //viaja solo
          $scope.solo();
        }else{
          $scope.acompanado();
        }*/

        //Seleccionar tipo de viaje
        angular.forEach(response.data.cuestionario.tipo_viaje , function(value, key) {

            if( value.tipo_viaje == "Vacaciones/Descanso/Ocio"){
              $rootScope.cuestionario.tipoViaje.vacaciones = true;
            }

            if( value.tipo_viaje == "Visita a Familiares/Amigos"){
              $rootScope.cuestionario.tipoViaje.familiares = true;
            }

            if( value.tipo_viaje == "Educación/Formación"){
              $rootScope.cuestionario.tipoViaje.educacion = true;
            }

            if( value.tipo_viaje == "Salud"){
              $rootScope.cuestionario.tipoViaje.salud = true;
            }

            if( value.tipo_viaje == "Religiosos/Peregrinaciones"){
              $rootScope.cuestionario.tipoViaje.religioso = true;
            }

            if( value.tipo_viaje == "Compras"){
              $rootScope.cuestionario.tipoViaje.compras = true;
            }

            if( value.tipo_viaje == "Negocios/Profesionales"){
              $rootScope.cuestionario.tipoViaje.negocios = true;
            }
        });

        //seleccionar perfil actividad
        angular.forEach(response.data.cuestionario.perfil_actividad , function(value, key) {
          if( value.nombre_actividad == "Deportes de playa y acuáticos"){
              $rootScope.cuestionario.actividades.deportes = true;
          }

          if( value.nombre_actividad == "Visitar Casinos / Salas de juegos"){
              $rootScope.cuestionario.actividades.casinos = true;
          }

          if( value.nombre_actividad == "Actividades rurales"){
              $rootScope.cuestionario.actividades.rurales = true;
          }

          if( value.nombre_actividad == "Actividades relacionadas a pueblos originarios"){
              $rootScope.cuestionario.actividades.originarios = true;
          }

          if( value.nombre_actividad == "Excursiones náuticas, cruceros"){
              $rootScope.cuestionario.actividades.exursiones = true;
          }

          if( value.nombre_actividad == "Visita a viñas y zonas de producción vinícolas"){
              $rootScope.cuestionario.actividades.enoturismo = true;
          }

          if( value.nombre_actividad == "Recreativas y deportivas de montaña y nieve"){
              $rootScope.cuestionario.actividades.aventura = true;
          }

          if( value.nombre_actividad == "Rutas de interés temático"){
              $rootScope.cuestionario.actividades.especiales = true;
          }

          if( value.nombre_actividad == "Actividades de Playa"){
              $rootScope.cuestionario.actividades.playa = true;
          }

          if( value.nombre_actividad == "Visitar Áreas Protegidas, ecoturismo, observación de paisaje, flora y fauna"){
              $rootScope.cuestionario.actividades.naturaleza = true;
          }

          if( value.nombre_actividad == "Actividades Profesionales"){
              $rootScope.cuestionario.actividades.reuniones = true;
          }

          if( value.nombre_actividad == "Gastronomía"){
              $rootScope.cuestionario.actividades.gastronomia = true;
          }

          if( value.nombre_actividad == "Visitas a museos, centros históricos, monumentos"){
              $rootScope.cuestionario.actividades.museos = true;
          }

          if( value.nombre_actividad == "Visitas a observatorios astronómicos"){
              $rootScope.cuestionario.actividades.astronomicos = true;
          }

          if( value.nombre_actividad == "Ocio-Descanso"){
              $rootScope.cuestionario.actividades.ocio = true;
          }

          if( value.nombre_actividad == "City tour"){
              $rootScope.cuestionario.actividades.cityTour = true;
          }

          if( value.nombre_actividad == "Otro"){
              $rootScope.cuestionario.actividades.otro = true;
          }
        });


        console.log($rootScope.cuestionario);

    }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short')
  });

  $scope.cerrarModalPreferencias = function(){
    console.log("cerrar modal");
    //if(ionic.Platform.isWebView())
      //$rootScope.map.setClickable(true);
      //$scope.addCuestionario($rootScope.cuestionario);
      $rootScope.modalPreferencias.remove();
  }

  $scope.addCuestionario = function(cuestionario){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addCuestionarioGeo(lat,long,cuestionario);
      },function(err) {
        $scope.addCuestionarioGeo(0,0,cuestionario);
       },posOptions
    );
  }

  $scope.addCuestionarioGeo = function(lat,long,cuestionario){
      console.log(cuestionario);

      /*if( cuestionario == [] ){
        console.log("asdfsdfasdfgasd");
        $scope.noRespondeActividades    = true;
        $scope.noRespondeComida         = true;
        $scope.noRespondeDuracion       = true;
        $scope.noRespondeGasto          = true;
        $scope.noRespondeNumeroPersonas = true;
        $scope.noRespondeSoloAcom       = true;
        $scope.noRespondeTipoServicio   = true;
        $scope.noRespondeTipoTours      = true;
        $scope.noRespondeTransporte     = true;
        $scope.noRespondeTipoViaje      = true;
        $scope.noRespondeMoneda         = true;
      }else{
        $scope.noRespondeActividades    = false;
        $scope.noRespondeComida         = false;
        $scope.noRespondeDuracion       = false;
        $scope.noRespondeGasto          = false;
        $scope.noRespondeNumeroPersonas = false;
        $scope.noRespondeSoloAcom       = false;
        $scope.noRespondeTipoServicio   = false;
        $scope.noRespondeTipoTours      = false;
        $scope.noRespondeTransporte     = false;
        $scope.noRespondeTipoViaje      = false;
        $scope.noRespondeMoneda         = false;
      }*/

      /*if( cuestionario.actividades == null    || cuestionario.actividades=="" ){
        $scope.noRespondeActividades = true;
      }
      if( cuestionario.comida == null         || cuestionario.comida == "" ){
        $scope.noRespondeComida = true;
      }
      if( cuestionario.duracion == null       || cuestionario.duracion == "" ){
        $scope.noRespondeDuracion = true;
      }
      if( cuestionario.gasto == null          || cuestionario.gasto == "" ){
        $scope.noRespondeGasto = true;
      }
      if( cuestionario.numeroPersonas == null || cuestionario.numeroPersonas == "" ){
        $scope.noRespondeNumeroPersonas = true;
      }
      if( cuestionario.soloAcom == null       || cuestionario.soloAcom == "" ){
        $scope.noRespondeSoloAcom = true;
      }
      if( cuestionario.tipoServicio == null   || cuestionario.tipoServicio == "" ){
        $scope.noRespondeTipoServicio = true;
      }
      if( cuestionario.tipoTours == null      || cuestionario.tipoTours == "" ){
        $scope.noRespondeTipoTours = true;
      }
    if( cuestionario.transporte == null     || cuestionario.transporte == ""){
        $scope.noRespondeTransporte = true;
      }
      if( cuestionario.tipoViaje == null     || cuestionario.tipoViaje == ""){
        $scope.noRespondeTipoViaje = true;
      }
      if( cuestionario.moneda == null     || cuestionario.moneda == ""){
        $scope.noRespondeMoneda = true;
      }*/

    
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addPreferencia.php",
        data: $httpParamSerializer({
          "cuestionario" : JSON.stringify(cuestionario),
          "id_usuario"   : localStorage.getItem("id_usuario"),
          "lat"          : lat,
            "long"         : long
      }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        console.log(response.data)
        if(response.data.resultado == "ok"){
          $ionicLoading.hide();
          //$rootScope.toast('preferencias actualizadas', 'short');
          //$scope.cerrarModalPreferencias();
          $rootScope.modalPreferencias.remove();
        }else{
          $rootScope.toast('Debes responder todas las preguntas', 'short');
          $ionicLoading.hide();
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
      });
     
  }

      $scope.acompanado = function(){
        $scope.mostrarAcompanado = true;
      }

      $scope.solo = function(){   
        $scope.mostrarAcompanado = false;
        $rootScope.cuestionario.numeroPersonas = 0;
      }
	//$scope.preferencia = [];
  /*$scope.mostrarAcompanado        = false;
  $rootScope.cuestionario             = {};
  $rootScope.cuestionario.tipoViaje   = [];
  $rootScope.cuestionario.actividades = {};
  $scope.comida                   = "";
  $scope.tipoTours                = "";

  $scope.noRespondeActividades    = false;
  $scope.noRespondeComida         = false;
  $scope.noRespondeDuracion       = false;
  $scope.noRespondeGasto          = false;
  $scope.noRespondeNumeroPersonas = false;
  $scope.noRespondeSoloAcom       = false;
  $scope.noRespondeTipoServicio   = false;
  $scope.noRespondeTipoTours      = false;
  $scope.noRespondeTransporte     = false;
  $scope.noRespondeTipoViaje      = false;
  $scope.noRespondeMoneda         = false;

  $rootScope.cuestionario.tipoViaje.push({
    'educacion' : true
  });

  $http({
	    method: "POST",
	    url: "http://200.14.68.107/atacamaGo/getPreferencia.php",
	    data: $httpParamSerializer({
	  		"id_usuario"    : localStorage.getItem("id_usuario")
	  }),
	    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	  }).then(function(response){ //ok si guardó correctamente.
	  	console.log(localStorage.getItem("id_usuario") );
      console.log(response.data)
	    if(response.data.resultado == "ok"){
  			//$rootScope.cuestionario = response.data.cuestionario;
        $rootScope.cuestionario.moneda         = response.data.cuestionario.moneda;
        $rootScope.cuestionario.numeroPersonas = response.data.cuestionario.numeroPersonas;
        $rootScope.cuestionario.gasto          = response.data.cuestionario.gasto;
        $rootScope.cuestionario.duracion       = response.data.cuestionario.duracion;
        
        $rootScope.cuestionario.transporte     = response.data.cuestionario.medio_transporte;
        $rootScope.cuestionario.comida         = response.data.cuestionario.tipo_alimentacion;
        $rootScope.cuestionario.tipoTours      = response.data.cuestionario.tipo_tour;

        $rootScope.cuestionario.tipoServicio   = response.data.cuestionario.id_alojamiento_perfil;

        if( response.data.cuestionario.numeroPersonas != 0 )
          $rootScope.cuestionario.soloAcom = "FALSE";
        else
          $rootScope.cuestionario.soloAcom = "TRUE";
        
        $rootScope.cuestionario.actividades    = {};
        $rootScope.cuestionario.tipoViaje      = {};
        
        //selecionar si viaja solo o acompañado
        if( $rootScope.cuestionario.numeroPersonas == 0 ){
          //viaja solo
          $scope.solo();
        }else{
          $scope.acompanado();
        }

        //Seleccionar tipo de viaje
        angular.forEach(response.data.cuestionario.tipo_viaje , function(value, key) {

            if( value.tipo_viaje == "Vacaciones/Descanso/Ocio"){
              $rootScope.cuestionario.tipoViaje.vacaciones = true;
            }

            if( value.tipo_viaje == "Visita a Familiares/Amigos"){
              $rootScope.cuestionario.tipoViaje.familiares = true;
            }

            if( value.tipo_viaje == "Educación/Formación"){
              $rootScope.cuestionario.tipoViaje.educacion = true;
            }

            if( value.tipo_viaje == "Salud"){
              $rootScope.cuestionario.tipoViaje.salud = true;
            }

            if( value.tipo_viaje == "Religiosos/Peregrinaciones"){
              $rootScope.cuestionario.tipoViaje.religioso = true;
            }

            if( value.tipo_viaje == "Compras"){
              $rootScope.cuestionario.tipoViaje.compras = true;
            }

            if( value.tipo_viaje == "Negocios/Profesionales"){
              $rootScope.cuestionario.tipoViaje.negocios = true;
            }
        });

        //seleccionar perfil actividad
        angular.forEach(response.data.cuestionario.perfil_actividad , function(value, key) {
          if( value.nombre_actividad == "Deportes de playa y acuáticos"){
              $rootScope.cuestionario.actividades.deportes = true;
          }

          if( value.nombre_actividad == "Visitar Casinos / Salas de juegos"){
              $rootScope.cuestionario.actividades.casinos = true;
          }

          if( value.nombre_actividad == "Actividades rurales"){
              $rootScope.cuestionario.actividades.rurales = true;
          }

          if( value.nombre_actividad == "Actividades relacionadas a pueblos originarios"){
              $rootScope.cuestionario.actividades.originarios = true;
          }

          if( value.nombre_actividad == "Excursiones náuticas, cruceros"){
              $rootScope.cuestionario.actividades.exursiones = true;
          }

          if( value.nombre_actividad == "Visita a viñas y zonas de producción vinícolas"){
              $rootScope.cuestionario.actividades.enoturismo = true;
          }

          if( value.nombre_actividad == "Recreativas y deportivas de montaña y nieve"){
              $rootScope.cuestionario.actividades.aventura = true;
          }

          if( value.nombre_actividad == "Rutas de interés temático"){
              $rootScope.cuestionario.actividades.especiales = true;
          }

          if( value.nombre_actividad == "Actividades de Playa"){
              $rootScope.cuestionario.actividades.playa = true;
          }

          if( value.nombre_actividad == "Visitar Áreas Protegidas, ecoturismo, observación de paisaje, flora y fauna"){
              $rootScope.cuestionario.actividades.naturaleza = true;
          }

          if( value.nombre_actividad == "Actividades Profesionales"){
              $rootScope.cuestionario.actividades.reuniones = true;
          }

          if( value.nombre_actividad == "Gastronomía"){
              $rootScope.cuestionario.actividades.gastronomia = true;
          }

          if( value.nombre_actividad == "Visitas a museos, centros históricos, monumentos"){
              $rootScope.cuestionario.actividades.museos = true;
          }

          if( value.nombre_actividad == "Visitas a observatorios astronómicos"){
              $rootScope.cuestionario.actividades.astronomicos = true;
          }

          if( value.nombre_actividad == "Ocio-Descanso"){
              $rootScope.cuestionario.actividades.ocio = true;
          }

          if( value.nombre_actividad == "City tour"){
              $rootScope.cuestionario.actividades.cityTour = true;
          }

          if( value.nombre_actividad == "Otro"){
              $rootScope.cuestionario.actividades.otro = true;
          }
        });
	    }
	  }, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet', 'short')
	  });

  $scope.cerrarModalPreferencias = function(){
    console.log("cerar modal");
    if(ionic.Platform.isWebView())
      //$rootScope.map.setClickable(true);
    
    $rootScope.modalPreferencias.hide()
  }

  $scope.addCuestionario = function(cuestionario){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addCuestionarioGeo(lat,long,cuestionario);
      },function(err) {
        $scope.addCuestionarioGeo(0,0,cuestionario);
      },posOptions
    );
  }

  $scope.addCuestionarioGeo = function(lat,long,cuestionario){
  	console.log(cuestionario);
  	
    if( cuestionario == [] ){
      console.log("asdfsdfasdfgasd");
      $scope.noRespondeActividades    = true;
      $scope.noRespondeComida         = true;
      $scope.noRespondeDuracion       = true;
      $scope.noRespondeGasto          = true;
      $scope.noRespondeNumeroPersonas = true;
      $scope.noRespondeSoloAcom       = true;
      $scope.noRespondeTipoServicio   = true;
      $scope.noRespondeTipoTours      = true;
      $scope.noRespondeTransporte     = true;
      $scope.noRespondeTipoViaje      = true;
      $scope.noRespondeMoneda         = true;
    }else{
      $scope.noRespondeActividades    = false;
      $scope.noRespondeComida         = false;
      $scope.noRespondeDuracion       = false;
      $scope.noRespondeGasto          = false;
      $scope.noRespondeNumeroPersonas = false;
      $scope.noRespondeSoloAcom       = false;
      $scope.noRespondeTipoServicio   = false;
      $scope.noRespondeTipoTours      = false;
      $scope.noRespondeTransporte     = false;
      $scope.noRespondeTipoViaje      = false;
      $scope.noRespondeMoneda         = false;
    }

    if( cuestionario.actividades == null    || cuestionario.actividades=="" ){
      $scope.noRespondeActividades = true;
    }
    if( cuestionario.comida == null         || cuestionario.comida == "" ){
      $scope.noRespondeComida = true;
    }
    if( cuestionario.duracion == null       || cuestionario.duracion == "" ){
      $scope.noRespondeDuracion = true;
    }
    if( cuestionario.gasto == null          || cuestionario.gasto == "" ){
      $scope.noRespondeGasto = true;
    }
    if( cuestionario.numeroPersonas == null || cuestionario.numeroPersonas == "" ){
      $scope.noRespondeNumeroPersonas = true;
    }
    if( cuestionario.soloAcom == null       || cuestionario.soloAcom == "" ){
      $scope.noRespondeSoloAcom = true;
    }
    if( cuestionario.tipoServicio == null   || cuestionario.tipoServicio == "" ){
      $scope.noRespondeTipoServicio = true;
    }
    if( cuestionario.tipoTours == null      || cuestionario.tipoTours == "" ){
      $scope.noRespondeTipoTours = true;
    }
    if( cuestionario.transporte == null     || cuestionario.transporte == ""){
      $scope.noRespondeTransporte = true;
    }
    if( cuestionario.tipoViaje == null      || cuestionario.tipoViaje == ""){
      $scope.noRespondeTipoViaje = true;
    }
    if( cuestionario.moneda == null         || cuestionario.moneda == ""){
      $scope.noRespondeMoneda = true;
    }

  	$http({
	    method: "POST",
	    url: "http://200.14.68.107/atacamaGo/addPreferencia.php",
	    data: $httpParamSerializer({
	  		"cuestionario" : JSON.stringify(cuestionario),
	  		"id_usuario"   : localStorage.getItem("id_usuario"),
        "lat"          : lat,
        "long"         : long
	  }),
	    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	  }).then(function(response){ //ok si guardó correctamente.
	  	console.log(response.data)
	    if(response.data.resultado == "ok"){
	      $rootScope.toast('preferencias actualizadas', 'short');
        $ionicLoading.hide();
	    }else{
	      $rootScope.toast('Debes responder todas las preguntas', 'short');
	    }
      $ionicLoading.hide();
	  }, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet', 'short')
	  });
  }

  	$scope.acompanado = function(){
  		$scope.mostrarAcompanado = true;
  	}

  	$scope.solo = function(){		
  		$scope.mostrarAcompanado = false;
  		$rootScope.cuestionario.numeroPersonas = 0;
  	}*/

    $scope.openModalPreferenciasDetalle = function(){

      //Modal Modificar Imagen...
      $ionicModal.fromTemplateUrl('templates/preferenciasDetalle.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalpreferenciasDetalle = modal;      
        $rootScope.modalpreferenciasDetalle.show();

      });

    }

    $scope.opcionesPreferencias = function(id){
      switch(id) {
          case 1:
              console.log("solo acompañado");
              $rootScope.flagPreferencias = 1;
              $scope.openModalPreferenciasDetalle();
              break;
          case 2:
              console.log("radio");
              $rootScope.flagPreferencias = 2;
              $scope.openModalPreferenciasDetalle();
              break;
          case 3:
              console.log("gasto prom.");
              $rootScope.flagPreferencias = 3;
              $scope.openModalPreferenciasDetalle();
              break;
          case 4:
              console.log("moneda");
              $rootScope.flagPreferencias = 4;
              $scope.openModalPreferenciasDetalle();
              break;
          case 5:
              console.log("duracion estadia");
              $rootScope.flagPreferencias = 5;
              $scope.openModalPreferenciasDetalle();
              break;
          case 6:
              console.log("tipo viaje");
              $rootScope.flagPreferencias = 6;
              $scope.openModalPreferenciasDetalle();
              break;
          case 7:
              console.log("medio transporte");
              $rootScope.flagPreferencias = 7;
              $scope.openModalPreferenciasDetalle();
              break;
          case 8:
              console.log("actividades prefieres realizar");
              $rootScope.flagPreferencias = 8;
              $scope.openModalPreferenciasDetalle();
              break;
          case 9:
              console.log("tipo servicio alojamiento");
              $rootScope.flagPreferencias = 9;
              $scope.openModalPreferenciasDetalle();
              break;
          case 10:
              console.log("tipo comida");
              $rootScope.flagPreferencias = 10;
              $scope.openModalPreferenciasDetalle();
              break;
          case 11:
              console.log("tipo tours");
              $rootScope.flagPreferencias = 11;
              $scope.openModalPreferenciasDetalle();
              break;
          default:
              return
      }
    }

    /*$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      alert("Asdsd");
      $scope.addCuestionario($rootScope.cuestionario);
      $rootScope.modalPreferencias.remove();
    });

    $ionicPlatform.registerBackButtonAction(function () {
      alert("Asdsd");
      if (condition) {
        navigator.app.exitApp();
      } else {
        $scope.addCuestionario($rootScope.cuestionario);
        $rootScope.modalPreferencias.remove();
      }
    }, 100);*/

    document.addEventListener('backbutton', function(){
      $scope.addCuestionario($rootScope.cuestionario);
      $rootScope.modalPreferencias.remove();
    });

}]);