angular.module('starter.controllers')


.controller('detalleMenuExperienciaCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce,$ionicNativeTransitions) {
    console.log("detalle MENU experiencia controller");
    console.log($rootScope.experienciaDetalleMostrar);
    $scope.item = $rootScope.experienciaSeleccionadaNuevoHome;
    console.log($scope.item);
    $scope.iconoMenu = "";
    $scope.menuSeleccionado = $rootScope.experienciaDetalleMostrar;

    $scope.nombreMenu = "PROGRAMA";
    $scope.cerrarModalDetalleMenuExperiencia = function(){
      $rootScope.modalDetalleMenuExperiencia.hide();
  	}

    $scope.goToNuevoHome = function(){
      $rootScope.modalDetalleExperiencia.remove();
      $rootScope.modalDetalleMenuExperiencia.hide(); 
    }
    
    $scope.menu = function(id){
      var identificador = id;
      $rootScope.experienciaDetalleMostrar = identificador;
      $scope.menuSeleccionado = id;

      if( $rootScope.experienciaDetalleMostrar == 1 ){
          $scope.nombreMenu = "PROGRAMA";
          $scope.cuerpo     = $scope.item.detalle_paquete_turistico ;
          $scope.img        = "img/experiencia/programa.png";
          $scope.iconoMenu  = "img/experiencia/imgPrograma.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 2 ){
          $scope.nombreMenu = "INCLUYE";
          $scope.cuerpo     = $rootScope.experienciaSeleccionada.incluye_paquete_turistico;
          $scope.img        = "img/experiencia/incluye.png";
          $scope.iconoMenu  = "img/experiencia/imgIncluye.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 3 ){
          $scope.nombreMenu = "VIGENCIA";
          $scope.cuerpo     = $rootScope.experienciaSeleccionada.vigencia_paquete_turistico;
          $scope.img        = "img/experiencia/vigencia.png";
          $scope.iconoMenu  = "img/experiencia/imgVigencia.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 4 ){
          $scope.nombreMenu = "TRAER";
          $scope.cuerpo     = $rootScope.experienciaSeleccionada.turista_debe_traer;
          $scope.img        = "img/experiencia/traer.png";
          $scope.iconoMenu  = "img/experiencia/imgTraer.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 5 ){
          $scope.nombreMenu = "RESERVAR";
          $scope.cuerpo     = "";
          $scope.img        = "";
          $scope.iconoMenu  = "img/experiencia/imgReservar.png";
          $scope.mostrarBtnReservar = true;

          if($scope.item.precio_pesos == null) {
            $scope.ocultarPeso = true;
          }

          if ($scope.item.precio_dolar == null) {
            $scope.ocultarDolar = true;
          }
      }

    }

    $scope.menu( $rootScope.experienciaDetalleMostrar );

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.modificarReserva = function(id_reserva,id_servicio_turistico){
      $rootScope.id_reserva            = id_reserva;
      $rootScope.id_servicio_turistico = id_servicio_turistico;
      $rootScope.rescatarDesdeBd = true;
      //$scope.cerrarModalReservasModificar();
      $scope.openModalReserva();
      
    }

    $scope.openModalReserva = function(){
      $rootScope.item_seleccionado = $scope.item;
      $rootScope.id_servicio_turistico = $scope.item.id_servicio_turistico;
      $rootScope.rescatarDesdeBd = false;
      $rootScope.fromExpreiencia = true;
      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/reserva.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReserva = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
        //  $rootScope.map.setClickable(false);

        $rootScope.modalReserva.show();
      })  
    }

}]);


