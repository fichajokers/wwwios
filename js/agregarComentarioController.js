angular.module('starter.controllers')


.controller('agregarComentarioCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicLoading','$ionicModal','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicLoading,$ionicModal,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.cerrarModalAgregarComentario = function(){   

    $rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();

    $scope.item = [];
    // $rootScope.modalAgregarComentario.remove()
    // console.log("cierrra agregar comentario");
    $rootScope.modalMenuAcciones.remove();
    $rootScope.modalAgregarComentario.remove();
    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
      "type": "false",
      "direction": "up",
      "duration": 400,
    });
  }

  $scope.atrasModalAgregarComentario = function(){
    $scope.item = [];
    $rootScope.modalAgregarComentario.remove();
  }

  $scope.comentarios         = [];
	$scope.item                = [];
	$scope.foto_perfil_usuario = ""


        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getComentarioItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico": $rootScope.id_item_turistico,
            "id_usuario"       : localStorage.getItem("id_usuario")
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
        	//console.log(response);
          if(response.data.resultado == "ok"){
            $scope.comentarios         = response.data.comentario;
            $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;

          }else if ( response.data.resultado == "no data" ){
            //$rootScope.toast('lugar aún no tiene comentarios', 'short');
            $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/" + response.data.foto_perfil_usuario;
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });

  $scope.cerrarModalModificarComentario = function(){   
    $rootScope.modalModificarComentario.remove();
  }

  $scope.addComentario = function(item){
    $ionicLoading.show({
        template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addComentarioGeo(lat,long,item);
      },function(err) {
        $scope.addComentarioGeo(0,0,item);
      },posOptions
    );
  }

  $scope.addComentarioGeo = function(lat,long,item){   

    if(item.comentario || item.comentario != "" ){
	        $http({
	          method: "POST",
	          url: "http://200.14.68.107/atacamaGo/addComentarioItemTuristico.php",
	          data: $httpParamSerializer({
	            "id_item_turistico" : $rootScope.id_item_turistico,
	            "id_usuario"        : localStorage.getItem("id_usuario"),
	            "comentario"        : item.comentario,
              "lat"               : lat,
              "long"              : long
	        }),
	          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	        }).then(function(response){ //ok si guardó correctamente.
	        	console.log(response);
	          if(response.data.resultado == "ok"){
	            $rootScope.toast('comentario agregado correctamente', 'short');
                $scope.comentarios.push({
                  "texto_comentario" : item.comentario,
                  "fecha_comentario" : "Ahora",
                  "nombre"           : "",
                  "apellido_paterno" : "",
                  "foto_perfil"      : $scope.foto_perfil_usuario
                });
                $scope.item.comentario = "";
	          }else{
	            $rootScope.toast('error, intenta nuevamente', 'short');
	          }
            $ionicLoading.hide();
	        }, function(){ //Error de conexión
	          $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
	        });


    }else{
    	$ionicLoading.hide();
      $rootScope.toast('Debes escribir un comentario', 'short');
    }
  }

  $scope.openModalSubirImagen = function(){

      //Modal subirImagen...
      $ionicModal.fromTemplateUrl('templates/subirImagen.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSubirImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        // $scope.cerrarModalAgregarComentario();
        $rootScope.modalAgregarComentario.remove();
        $rootScope.modalSubirImagen.show();
      });

    }

    $scope.openModalAgregarVideo = function(){

      //Modal Agregar Video...
      $ionicModal.fromTemplateUrl('templates/agregarVideo.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        // $scope.cerrarModalAgregarComentario();
        $rootScope.modalAgregarComentario.remove();
        $rootScope.modalAgregarVideo.show();
      });
        
    }

    $scope.openModalAgregarAudio = function(){

      //Modal Agregar Audio...
      $ionicModal.fromTemplateUrl('templates/agregarAudio.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        // $scope.cerrarModalAgregarComentario();
        $rootScope.modalAgregarComentario.remove();
        $rootScope.modalAgregarAudio.show();
      });
  
    }

    $scope.openModalAgregarDescripcion = function(){
      $rootScope.modalAgregarComentario.remove();
      //Modal Agregar descripcion...
      /*$ionicModal.fromTemplateUrl('templates/agregarDescripcion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarComentario();
        $rootScope.modalAgregarDescripcion.show();
      });*/

    }

    $scope.openModalAgregarComentario = function(){
      //Modal Agregar Comentario...
      console.log("no vuelve a abrir");
      // $ionicModal.fromTemplateUrl('templates/agregarComentario.html', {
      //   scope: $rootScope,
      //   animation: 'slide-in-up',
      //   backdropClickToClose: false,
      // }).then(function(modal) {
      //   $rootScope.modalAgregarComentario = modal;
      //   //hacer que el mapa no sea clickeable.
      //   //if( ionic.Platform.isAndroid() )
      //     $rootScope.map.setClickable(false);  
      //   $scope.cerrarModalAgregarComentario();
      //   $rootScope.modalAgregarComentario.show();
      // });
    }

    $scope.openModalAgregarCalificar = function(){
      //Modal Agregar Calificar...
      $ionicModal.fromTemplateUrl('templates/agregarCalificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarCalificar = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        // $scope.cerrarModalAgregarComentario();
        $rootScope.modalAgregarComentario.remove();
        $rootScope.modalAgregarCalificar.show();
      });
  
    }

}]);