angular.module('starter.controllers')


.controller('recorrerRutaTipicaOfflineCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$httpParamSerializer','$timeout','$sce','$cordovaCamera','$cordovaFileTransfer','$cordovaSocialSharing','$ionicPopup','$ionicLoading','$ionicNativeTransitions','$filter',function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$httpParamSerializer,$timeout,$sce,$cordovaCamera,$cordovaFileTransfer,$cordovaSocialSharing,$ionicPopup,$ionicLoading,$ionicNativeTransitions,$filter) {
    $scope.ruta = $rootScope.rutaRecorrer;
    console.log($scope.ruta.id_ruta);
    //console.log(localStorage.getItem('rutasOffline'));
    
    
    $scope.rutaOffline = JSON.parse(localStorage.getItem('rutasOffline'));
    console.log("RUTA OFFLINE: ",$scope.rutaOffline);
    console.log("RUTA OFFLINE MAPA",$scope.rutaOffline[0].mapa);

    $scope.opcionSeleciconada            = false;
    $scope.mostrarBotonesRegistrarhuella = false;
    $scope.entrarUnaVezRecomendado       = 0;
    $scope.verMishuellas                 = false;
    $scope.verTodashuellas               = false;
    $scope.marcadorDraggable             = null;
    $scope.pausa                         = false;
    $scope.titulo                        = "DEJA TU HUELLA";

    var greenIcon = L.icon({
          iconUrl: 'img/gps.png',
          iconSize:     [20, 30], // size of the icon
      });

    $scope.position = L.marker([-33,-71],{icon: greenIcon});
    
    $scope.map = "";

    


    function renderMap(){

    $scope.map = L.map('mapid', {preferCanvas: true, zoomControl:false});

    
    
    //https://b.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoib3NtY2wiLCJhIjoiY2l6OGcxc3g4MDBxbzJ3bzF5MzI0cWlpaSJ9.6WVJyBCf1VlfZlrNXLpJCw
      //mapa/{z}/{x}/{y}.png
      //cordova.file.dataDirectory+'mapa/{z}/{x}/{y}.png'

      L.tileLayer("img/defecto-mapa.png",{
              minZoom: 14,
              maxZoom: 17,
            }).addTo($scope.map);


      L.tileLayer(cordova.file.applicationDirectory+'www/mapa/{z}/{x}/{y}.png', {
        attribution: '',
        maxZoom: 13,

      }).addTo($scope.map);







       lc = L.control.locate({
        position: 'topright',
        icon: 'iconoGeoLocalizacion',

      }).addTo($scope.map);
       


       
      //lc.start();




      $scope.rutaSeleccionada = -1;

      console.log("rutas: ",$scope.rutaOffline);

      angular.forEach($scope.rutaOffline, function(value, key) {    
          if($scope.rutaOffline[key].ruta.id_ruta == $scope.ruta.id_ruta){
            console.log("encontramos la ruta de id : ", $scope.rutaOffline[key].ruta.id_ruta, " en el key:", key);
            $scope.rutaSeleccionada = key;
          }
      });

      // crear lineas de rutas
      if ($scope.rutaSeleccionada == -1) {
        console.log("no se encuentra guardada la ruta");
      }else{
        console.log("Su ruta es seleccionada fue :",$scope.rutaOffline[$scope.rutaSeleccionada] );
        
        //Añadir polilyne de la ruta
        var latlngs;
        var polyline;
        /*angular.forEach($scope.rutaOffline[$scope.rutaSeleccionada].mapa, function(value, key) {     
            latlngs = [
                [value.inicio_lat, value.inicio_lng],
                [value.fin_lat, value.fin_lng]
            ];
            polyline = L.polyline(latlngs, {color: '#3A2B86'}).addTo($scope.map);
            $scope.map.fitBounds(polyline.getBounds());
            
        });
*/
      angular.forEach($scope.rutaOffline[$scope.rutaSeleccionada].mapa, function(value, key) {     
                  latlngs = [
                      [value.inicio_lat, value.inicio_lng],
                      [value.fin_lat, value.fin_lng]
                  ];
                  console.log(value);

                  iconoServicio = L.icon({
                      iconUrl: 'img/offline/punto.png',
                      iconSize:     [15, 15], // size of the icon
                  });

                  markerServicio = L.marker([value.inicio_lat,value.inicio_lng],{icon: iconoServicio});
                  $scope.map.addLayer(markerServicio);
              });


        $scope.map.setView([$scope.rutaOffline[$scope.rutaSeleccionada].mapa[0].inicio_lat,$scope.rutaOffline[$scope.rutaSeleccionada].mapa[0].inicio_lng], 13);

        //Añadir servicios
        var iconoServicio;
        angular.forEach($scope.rutaOffline[$scope.rutaSeleccionada].servicios, function(value,key){

          iconoServicio = L.icon({
              iconUrl: 'img/servicios/'+value.icono+'.png',
              iconSize:     [25, 35], // size of the icon
          });

          markerServicio = L.marker([value.direccion_georeferenciada_latitud,value.direccion_georeferenciada_longitud],{icon: iconoServicio});
          markerServicio.tipo_seleccionado = "servicio";
          markerServicio.servicio = value;
          markerServicio.on('click', onClick);
          $scope.map.addLayer(markerServicio);
        });


        //Añadir servicios
        var iconoServicio;
        angular.forEach($scope.rutaOffline[$scope.rutaSeleccionada].patrimonios, function(value,key){

          iconoServicio = L.icon({
              iconUrl: 'img/patrimonios/'+value.icono+'.png',
              iconSize:     [25, 35], // size of the icon
          });

          markerServicio = L.marker([value.direccion_georeferenciada_latitud,value.direccion_georeferenciada_longitud],{icon: iconoServicio});
          markerServicio.tipo_seleccionado = "patrimonio";
          markerServicio.servicio = value;
          markerServicio.on('click', onClick);
          $scope.map.addLayer(markerServicio);
        });

      }
      


    

    /*$scope.map.on('locationfound', onLocationFound);
        $scope.map.on('locationerror', onLocationError);
        $scope.map.locate({ maxZoom: 15, watch: true});

    */



/*navigator.geolocation.watchPosition(function(position){
  var lat  = position.coords.latitude;
  var long = position.coords.longitude;

  var radius = 10 / 2;

  L.marker([lat,long]).addTo($scope.map);
  L.circle([lat,long], radius).addTo($scope.map);
});
   */   

    }

    $scope.cerrarRecorrerRutaTipicasOffline = function(){

        //$rootScope.volverExploraYDescubre = true;
        $rootScope.volverExploraYDescubreRutas = true;

        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
        $rootScope.modalRutasTipicas.show();
    }

    $scope.atrasRecorrerRutaTipicasOffline = function(){
      
        $rootScope.atrasRecorrerRutaTipicas = true;

        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
        $rootScope.modalRutasTipicas.show();
        $rootScope.modalSaberMas.show();

    }


function onLocationFound(e) {
    console.log(e.latlng);
    

    var radius = e.accuracy / 2;

    //$scope.position = L.marker(e.latlng,{icon: greenIcon});

    //L.circle(e.latlng, radius).addTo($scope.map);
    //
    
    //$scope.position.setLatLng(e.latlng)
    //$scope.map.removeLayer(marker);
    //$scope.map.addLayer($scope.position);
    
}


function onLocationError(e) {
   // alert(e.message);
   console.log("error",e);
}



function onClick(e) {

  $rootScope.item = e.target.servicio;
  $rootScope.tipo_seleccionado = e.target.tipo_seleccionado;
    $scope.openModalMenuAccionesModificar();
}


$scope.openModalMenuAccionesModificar = function(ruta){

      //Modal menu Acciones Modiciar...
        $ionicModal.fromTemplateUrl('templates/menuAccionesModificarOffline.html', {
            scope: $rootScope,
            animation: 'slide-in-up',
            backdropClickToClose: false,
        }).then(function(modal) {
            $rootScope.modalMenuAccionesModificar = modal;

            //hacer que el mapa no sea clickeable.
            //if( ionic.Platform.isAndroid() )
            //$rootScope.map.setClickable(false);

            $rootScope.modalMenuAccionesModificar.show();
      
        });
    }


    document.addEventListener("deviceready", renderMap, false);

}]);