angular.module('starter.controllers')


.controller('agregarVideoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$cordovaFileTransfer','$cordovaCamera','$httpParamSerializer','$ionicLoading','$ionicPopup','$ionicModal',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$cordovaFileTransfer,$cordovaCamera,$httpParamSerializer,$ionicLoading,$ionicPopup,$ionicModal) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.rutaVideo   = "";
  $scope.nombreVideo = "";
  $scope.videos      = [];
  $scope.noEdita     = false;
  $scope.id_usuario  = localStorage.getItem("id_usuario");

  $scope.cerrarModalAgregarVideo = function(){   
    $rootScope.modalAgregarVideo.remove()
  }

  $scope.chooseVideo = function () {
        var options = {
            quality         : 10,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType       : Camera.MediaType.VIDEO,
            correctOrientation : true
        };
   
        $cordovaCamera.getPicture(options).then(function (videoData) {
            $scope.rutaVideo     = videoData;
            $scope.nombreVideo   = Math.floor((Math.random() * 9999999) + 1)+"_"+videoData.substr(videoData.lastIndexOf('/') + 1);
            $scope.modalNombrevideo();
            //console.log($scope.rutaVideo, $scope.nombreVideo);
        }, function (err) {
          //alert(err);
            // An error occured. Show a message to the user
        });
    }
  

  $scope.capturarVideo = function() {
    var options = { 
      quality  : 10,
      limit    : 1, 
      duration : 15 
    };

    $cordovaCapture.captureVideo(options).then(function(videoData) {
      // Success! Video data is here
      $scope.rutaVideo   = videoData[0].fullPath;
      //console.log(videoData);
      $scope.nombreVideo = Math.floor((Math.random() * 9999999) + 1)+"_"+videoData[0].name;
      $scope.modalNombrevideo();
    }, function(err) {
      // An error occurred. Show a message to the user
    });
  }

  $scope.eliminarVideo = function(imagen,index){
    console.log(imagen);
        $ionicLoading.show({
            template: 'Cargando...'
        });

            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteVideoItemTuristico.php",
                data: $httpParamSerializer({
                    "id_archivo_item_turistico" : imagen.id_archivo_item_turistico,
                    "nombre"                    : imagen.nombre_delete
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.toast('Video eliminado correctamente',"short");
                    $scope.videos.splice(index, 1);    
                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });    
  }

  $scope.addVideoItem = function(){
    if( $scope.nombreVideo != "" ){
      $ionicLoading.show({
        template: 'Cargando...'
      });

      var options = {
        fileKey: "videoItem",
        fileName: $scope.nombreVideo,
        chunkedMode: false,
        mimeType: "video/mp4"
      };

        $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirVideoItem.php", $scope.rutaVideo, options).then(function(result) {
          //
        }, function(err) {
          $rootScope.toast('Error agregando video', 'short');
          $ionicLoading.hide();
          console.log(err);
        }, function (progress) {
          // constant progress updates
          console.log("subiendo video...");
        });

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addArchivoItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico"   : $rootScope.id_item_turistico,
            "id_usuario"          : localStorage.getItem("id_usuario"),
            "nombre_archivo_item" : $scope.nombreVideo,
            "tipo_archivo_item"   : 3,
            "nombre_video"        : $scope.nombre.nombre
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){

            $scope.videos.unshift({ 
              "video" : $scope.rutaVideo,
              "nombre" : $scope.nombre.nombre,
              "fecha" : 'Ahora',
              "id_usuario"              : localStorage.getItem("id_usuario"),
              "id_archivo_item_turistico" : response.data.id_archivo_item_turistico,
              "nombre_delete"   : $scope.nombreVideo
            });
            $rootScope.toast("Video agregado.","short");

            $scope.noEdita = false;
            $scope.nombreVideo = "";
            $scope.nombre.nombre = "";

          }else
            $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
          $ionicLoading.hide();
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
          $ionicLoading.hide();
        });
    }else{
      $rootScope.toast('Debes seleccionar un video',"short");
      $ionicLoading.hide();
    }
    
  }

  $scope.modalNombrevideo = function(){
    $scope.nombre = {};

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<input type="text" ng-model="nombre.nombre">',
      title: 'Nombre video',
      subTitle: 'Dale un nombre al video',
      scope: $scope,
      buttons: [
        { 
          text: '<i class="icon ion-close-round"></i>',
          type:'popclose'
        },{
          text: '<b>Aceptar</b>',
          type: 'button-positive asd',
          onTap: function(e) {
            if (!$scope.nombre.nombre) {
              //don't allow the user to close unless he enters nombre password
              e.preventDefault();
            } else {
              console.log($scope.nombre.nombre);
              return $scope.nombre.nombre;
            }
          }
        }
      ]
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  }

  $scope.openModalVisorVideo = function(video) {

    //modal visor de video...
    $ionicModal.fromTemplateUrl('templates/visorVideo.html', {
      scope: $scope
    }).then(function(modal) {
      
      $scope.modalVisorvideo = modal;
      
      console.log("video que llega: " + video);
      $scope.videoUrl = null;
      $scope.videoUrl = video;
      $scope.modalVisorvideo.show();

    });
  }

  $scope.cerrarModalVisorvideo = function(){
    //document.getElementById('video123').src='';
    $scope.modalVisorvideo.remove();
  }

  $scope.cerrarModalModificarVideo = function(){   
    $rootScope.modalModificarVideo.remove();
  }

  $scope.openModalSubirImagen = function(){

      //Modal subirImagen...
      $ionicModal.fromTemplateUrl('templates/subirImagen.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSubirImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarVideo();
        $rootScope.modalSubirImagen.show();
      });

    }

    $scope.openModalAgregarVideo = function(){

      //Modal Agregar Video...
      $ionicModal.fromTemplateUrl('templates/agregarVideo.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarVideo();
        $rootScope.modalAgregarVideo.show();
      });
        
    }

    $scope.openModalAgregarAudio = function(){

      //Modal Agregar Audio...
      $ionicModal.fromTemplateUrl('templates/agregarAudio.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarVideo();
        $rootScope.modalAgregarAudio.show();
      });
  
    }

    $scope.openModalAgregarDescripcion = function(){
      $rootScope.modalAgregarVideo.remove();
      //Modal Agregar descripcion...
      /*$ionicModal.fromTemplateUrl('templates/agregarDescripcion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarVideo();
        $rootScope.modalAgregarDescripcion.show();
      });*/

    }

    $scope.openModalAgregarComentario = function(){
      //Modal Agregar Comentario...
      $ionicModal.fromTemplateUrl('templates/agregarComentario.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarComentario = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarVideo();
        $rootScope.modalAgregarComentario.show();
      });
    }

    $scope.openModalAgregarCalificar = function(){
      //Modal Agregar Calificar...
      $ionicModal.fromTemplateUrl('templates/agregarCalificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarCalificar = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarVideo();
        $rootScope.modalAgregarCalificar.show();
      });
  
    }

}]);