angular.module('starter.controllers')


.controller('compartirCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicLoading','$cordovaSocialSharing','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicLoading,$cordovaSocialSharing,$ionicPopup) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.item = $rootScope.item_seleccionado;

  if( $rootScope.fromEventoSeleccionado == true ){
    $scope.item = [];
    $scope.item.nombre = $rootScope.evento_seleccionado.titulo_evento;
    $rootScope.fromEventoSeleccionado = false;
  }
  console.log($rootScope.linkTwitter);
  console.log($rootScope.linkFacebook);

  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.iconoCompartir = "img/menuAcciones/invCompartirAtractivo.png"
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.iconoCompartir = "img/menuAcciones/compartir.png"
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#3B3C97"
    }
  }

  if($rootScope.enEvento == true){
    console.log("viene de evento");
    $scope.estiloBarraModificar = {
      "background-color" : "#AF105E"
    }
    $scope.iconoCompartir = "img/evento/compartir.png"
    $scope.imagenTipo = "img/evento/compartir.png"
  }
  
  $scope.cerrarModalCompartir = function(){
    $rootScope.modalModificarCompartir.hide();      
  }

  $scope.cerrarModalCompartirMapa = function(){
    console.log("cerrar compartir");
    $rootScope.modalModificarCompartir.hide();     
    console.log("cerrar menu modificar");
    $rootScope.map.setClickable(true);
    $rootScope.modalMenuAccionesModificar.remove();
  }

  $scope.compartirFacebook = function(){
        $ionicLoading.show({
            template: 'Cargando...'
        });

        link = encodeURI($rootScope.linkFacebook);  

        console.log("compartirFacebook",link);
        $ionicLoading.hide();
        $cordovaSocialSharing.shareViaFacebook(link,null,link)
        .then(function(result) {
            console.log("ok",result);
        }, function(err) {
            $ionicLoading.hide();
            console.log("error",err);
            /*$ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de facebook para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
            });*/
        });
    }

    $scope.compartirTwitter  = function(){
        link = encodeURI($rootScope.linkTwitter);

        console.log("compartirTwitter", link);
        $ionicLoading.hide();
        $cordovaSocialSharing.shareViaTwitter("www.atacama-go.cl", null, link)
        .then(function(result) {
            $ionicLoading.hide();
            console.log("ok",result);
        }, function(err) {
          $ionicLoading.hide();
            /*$ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de twitter para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
            });*/
        });
    }

}]);