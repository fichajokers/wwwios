angular.module('starter.controllers')


.controller('serviciosTuristicosCtrl', ['$scope','$http','$state','$rootScope',function($scope,$http,$state,$rootScope) {
  // With the new view caching in Ionic, Controllers are only called
  
  width = window.screen.height;
  var alturaMapa = width - 166;
  $scope.styleMapa = {
      "height" : alturaMapa+"px",
      "width"  : "100%",
      "overflow-y" : "hidden"
  };
  
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
    data: {},
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      if(response.data.resultado == "ok"){
        $scope.tipo_servicio = response.data.tipo_servicio;
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });



  $scope.cerrarModalServiciosTuristicos = function(){
  	$scope.modalServiciosTuristicos.hide();
  }

  $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
  	$scope.cerrarModalServiciosTuristicos();
  	$rootScope.flagMostrarServiciosTuristicos = true;
  	$rootScope.mostrarServiciosTuristicos = {
  		"id_tipo_servicio" : id_tipo_servicio,
  		"index"            : index};
  	$state.go('serviciosAtractivos');
  }
}]);