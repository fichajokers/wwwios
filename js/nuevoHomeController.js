angular.module('starter.controllers')
.controller('nuevoHomeCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$ionicModal','$window','$httpParamSerializer','$ionicSideMenuDelegate','$ionicPlatform','$ionicNativeTransitions','$ionicPopup','$cordovaClipboard',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$ionicModal,$window,$httpParamSerializer,$ionicSideMenuDelegate,$ionicPlatform,$ionicNativeTransitions,$ionicPopup,$cordovaClipboard) {
  // With the new view caching in Ionic, Controllers are only called
  console.log("nuevoHomecontroller");
  /*PRIMERA VES SE SELECCIONA EL MENU "EXPLORA Y DESCUBRE"*/
  $scope.soloUnaVez = false;
  $scope.menuAbierto = false;
  $scope.experienciaSeleccionada = false;
  $scope.serviciosAtractivosImgSeleccionado = false;
  $scope.exploraDescubreImgSeleccionado = false;
  $scope.imgMenuExperiencia = "img/nuevoHome/experienciaAtacama.png";
  $scope.filtroActual = "todas las comunas";
  $scope.$on('$ionicView.afterEnter', function(){
     //$scope.exploraDescubre();
     //$scope.experienciaAtacama();
  });

  width = window.screen.width;

     primerMargin = 0;
     img2 = 0;
     img3 = 0;

     txt2 = 0;
     txt3 = 0;

     /*si es iphone 4*/
     if( width < 321  ){
      primerMargin = 85;
      img2 = 80 + primerMargin;
      img3 = 80 + img2;

      txt2 = 80 + primerMargin;
      txt3 = 80 + txt2;
      $scope.styleExperienciaAtacama  = {
        "margin-left" : primerMargin+"px",
        "z-index"     : "99999 !important"
      };
      $scope.styleExploraDescubre     = {
        "margin-left" : img2+"px",
        "z-index"     : "99999 !important"
      };
      $scope.styleServiciosAtractivos = {
        "margin-left" : img3+"px",
        "z-index"     : "99999 !important"
      };

      $scope.styleTxtExperienciaAtacama  = {
        "margin-left" : primerMargin+"px",
      };
      $scope.styleTxtExploraDescubre     = {
        "margin-left" : img2+"px"
      };
      $scope.styleTxtServiciosAtractivos = {
        "margin-left" : img3+"px"
      };
     }

     /*si es */
     if( width > 321 && width <= 360 ){
      primerMargin = 105;
      img2 = 80 + primerMargin;
      img3 = 80 + img2;

      txt2 = 86 + primerMargin;
      txt3 = 80 + txt2;
      $scope.styleExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleExploraDescubre     = {
        "margin-left" : img2+"px"
      };
      $scope.styleServiciosAtractivos = {
        "margin-left" : img3+"px"
      };

      $scope.styleTxtExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleTxtExploraDescubre     = {
        "margin-left" : img2+"px"
      };
      $scope.styleTxtServiciosAtractivos = {
        "margin-left" : img3+"px"
      };
     }

     /*si es iphone 6*/
     if( width > 360 && width <= 384 ){
      primerMargin = 110;
      img2 = 80 + primerMargin;
      img3 = 80 + img2;

      txt2 = 80 + primerMargin;
      txt3 = 80 + txt2;
      $scope.styleExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleExploraDescubre     = {
        "margin-left" : img2+"px"
      };
      $scope.styleServiciosAtractivos = {
        "margin-left" : img3+"px"
      };

      $scope.styleTxtExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleTxtExploraDescubre     = {
        "margin-left" : txt2+"px"
      };
      $scope.styleTxtServiciosAtractivos = {
        "margin-left" : txt3+"px"
      };
     }

     

     /*384-767*/
     if( width > 384 && width <= 599 ){
      primerMargin = 130;
      img2 = 80 + primerMargin;
      img3 = 80 + img2;

      txt2 = 80 + primerMargin;
      txt3 = 80 + txt2;
      $scope.styleExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleExploraDescubre     = {
        "margin-left" : img2+"px"
      };
      $scope.styleServiciosAtractivos = {
        "margin-left" : img3+"px"
      };

      $scope.styleTxtExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleTxtExploraDescubre     = {
        "margin-left" : txt2+"px"
      };
      $scope.styleTxtServiciosAtractivos = {
        "margin-left" : txt3+"px"
      };
     }

     /*lenovo tablet*/
     if( width > 599 && width <= 767 ){
       primerMargin = 180;
       img2 = 120 + primerMargin;
       img3 = 120 + img2;

       txt2 = 120 + primerMargin;
       txt3 = 120 + txt2;
       $scope.styleExperienciaAtacama  = {
         "margin-left" : primerMargin+"px"
       };
       $scope.styleExploraDescubre     = {
         "margin-left" : img2+"px"
       };
       $scope.styleServiciosAtractivos = {
         "margin-left" : img3+"px"
       };

       $scope.styleTxtExperienciaAtacama  = {
         "margin-left" : primerMargin+"px"
       };
       $scope.styleTxtExploraDescubre     = {
         "margin-left" : txt2+"px"
       };
       $scope.styleTxtServiciosAtractivos = {
         "margin-left" : txt3+"px"
       };
     }


     /*si es ipad*/
     if( width > 767 && width <= 768 ){
      console.log("¨width 768");

      primerMargin = 200;
      img2 = 200 + primerMargin;
      img3 = 200 + img2;

      txt2 = 200 + primerMargin;
      txt3 = 200 + txt2;
      $scope.styleExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleExploraDescubre     = {
        "margin-left" : img2+"px"
      };
      $scope.styleServiciosAtractivos = {
        "margin-left" : img3+"px"
      };

      $scope.styleTxtExperienciaAtacama  = {
        "margin-left" : primerMargin+"px"
      };
      $scope.styleTxtExploraDescubre     = {
        "margin-left" : txt2+"px"
      };
      $scope.styleTxtServiciosAtractivos = {
        "margin-left" : txt3+"px"
      };
     }

  /*document.addEventListener("deviceready", function() {
    if( $rootScope.desdeOtraVista ){
     
      $state.go($state.current, {}, {reload: true});
      $rootScope.desdeOtraVista = false;
    }
  });*/

cordova.getAppVersion(function(version) {
// version = "3.0.6";
    console.log(version);
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getVersion.php",
      data: $httpParamSerializer({
        "so"    : "ios",
        version : version
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
      if(response.data.resultado == "ok"){
        console.log("version ok");
      }else{
        var alertPopup = $ionicPopup.alert({
          title: 'AVISO',
          template: 'Actualice la versión de Atacama Go para seguir utilizando la aplicación',
          buttons: [{
            text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo" style="margin-bottom: -30px;"></img><b>ACEPTAR</b><div>&nbsp;</div>',
            type: 'button-positive asd botonNaranja',
            onTap: function(e) {
              // $ionicLoading.show({
              //   template: 'Cargando...'
              // });
              alertPopup.then(function(res) {
                window.open('https://itunes.apple.com/cl/app/atacamago/id1178051933?mt=8','_system');
              });
            }
          }]
        });
      }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });
  });



  $scope.reload = function(){
    $state.go($state.current, {}, {reload: true});
  }  

  
  $scope.serviciosAtractivosSeleccionado = false;
  $scope.exploraDescubreSeleccionado = true;

  $scope.getExperiencia = function(){
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getExperiencia.php",
      data: $httpParamSerializer({}),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        console.log(response);
        if(response.data.resultado == "ok"){
          $rootScope.experiencia = response.data.experiencia;
          $rootScope.experienciaFiltro = response.data.experiencia;
          console.log($rootScope.experiencia);
        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });  
  }


  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getComunas.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
    console.log(response);
    $scope.comunas = response.data.comuna;
    $scope.comunas.unshift({
      id_comuna : -1,
       nombre : "Todas las comunas"
    });
    console.log($scope.comunas);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  })
  
  $scope.getExperiencia();

  $scope.experienciaAtacama = function(){
    $scope.experienciaSeleccionada = true;
    $scope.serviciosAtractivosImgSeleccionado = false;
    $scope.exploraDescubreImgSeleccionado = false;

    $scope.getExperiencia();
  	$scope.styleTxtExperienciaAtacama  = {
  		"color" : "#AB1765",
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"color" : "white",
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"color" : "white",
  		"margin-left" : txt3+"px"
  	};

    $scope.experienciaAtacamaSeleccionado  = true;
    $scope.serviciosAtractivosSeleccionado = false;
    $scope.exploraDescubreSeleccionado     = false;
  }

  $scope.exploraDescubre = function(){
    $scope.experienciaSeleccionada = false;
    $scope.serviciosAtractivosImgSeleccionado = false;
    $scope.exploraDescubreImgSeleccionado = true;
  	$scope.styleTxtExperienciaAtacama  = {
  		"color" : "white",
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"color" : "#7162AB",
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"color" : "white",
  		"margin-left" : txt3+"px"
  	};

    $scope.serviciosAtractivosSeleccionado = false;
    $scope.experienciaAtacamaSeleccionado  = false;
    $scope.exploraDescubreSeleccionado     = true;
  }

  $scope.serviciosAtractivos = function(){
    $scope.experienciaSeleccionada = false;
    $scope.serviciosAtractivosImgSeleccionado = true;
    $scope.exploraDescubreImgSeleccionado = false;
  	$scope.styleTxtExperienciaAtacama  = {
  		"color" : "white",
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"color" : "white",
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"color" : "#764B18",
  		"margin-left" : txt3+"px"
  	};

    $scope.serviciosAtractivosSeleccionado = true;
    $scope.exploraDescubreSeleccionado     = false;
    $scope.experienciaAtacamaSeleccionado  = false;
  }

  $scope.menuOpcion = function(opcion){
  	if(opcion == 1){
  		//alert("rutas tipicas atama");

  		//Modal rutas Tipicas...
  		$ionicModal.fromTemplateUrl('templates/rutasTipicas.html', {
  		  scope: $rootScope,
  		  animation: 'slide-in-up',
  		  backdropClickToClose: false,
  		}).then(function(modal) {
  		  $rootScope.modalRutasTipicas = modal;
  		  $rootScope.modalRutasTipicas.show();
  		});
  		
  	}

  	if(opcion == 2){
  		//alert("deja tu huella");
      //$state.go("app.dejaTuHuella");
      $ionicNativeTransitions.stateGo("dejaTuHuella", {inherit:false},
        {"type": "slide",
          "direction":"up",
          "duration":10});
  	}

  	if(opcion == 3){
  		$ionicNativeTransitions.stateGo("iniciarMiRecorrido", {inherit:false},
        {"type": "slide",
          "direction":"up",
          "duration":10});
      //Modal eventos...
      // $ionicModal.fromTemplateUrl('templates/iniciarMiRecorrido.html', {
      //   scope: $rootScope,
      //   animation: 'slide-in-up',
      //   backdropClickToClose: false,
      // }).then(function(modal) {
      //   $rootScope.modalEventos = modal;
      //   $rootScope.modalEventos.show();
      // });
  	}

  	if(opcion == 4){
  		//alert("mis recorridos");
      $state.go("misRecorridos");
  	}

  	if(opcion == 5){
  		//Modal eventos...
      $ionicModal.fromTemplateUrl('templates/eventos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalEventos = modal;
        $rootScope.modalEventos.show();
      });
  	}

    if(opcion == 6){
      $ionicModal.fromTemplateUrl('templates/serviciosTuristicos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalServiciosTuristicos = modal;
        $rootScope.modalServiciosTuristicos.show();
      });
    }

    if(opcion == 7){
      $ionicModal.fromTemplateUrl('templates/atractivosTuristicos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAtractivosTuristicos = modal;
        $rootScope.modalAtractivosTuristicos.show();
      });
    }

     if(opcion == 22){
    //   $ionicModal.fromTemplateUrl('templates/menuAcciones.html', {
    //     scope: $rootScope,
    //     animation: 'slide-in-up',
    //     backdropClickToClose: false,
    //   }).then(function(modal) {
    //     $rootScope.correos ='';
    //     $rootScope.modalMenuAccionesModificar = modal;

    //     //hacer que el mapa no sea clickeable.
    //     //if( ionic.Platform.isAndroid() )
    //       //$rootScope.map.setClickable(false);

    //     $rootScope.modalMenuAccionesModificar.show();
      
    //   });
        $ionicModal.fromTemplateUrl('templates/pruebamodifica.html', {
            scope: $rootScope,
            animation: 'slide-in-up',
            backdropClickToClose: false,
          }).then(function(modal) {
            $rootScope.modalMenuAccionesModificar = modal;

            //hacer que el mapa no sea clickeable.
            //if( ionic.Platform.isAndroid() )
              //$rootScope.map.setClickable(false);

            $rootScope.modalMenuAccionesModificar.show();
          
          });
     }

    if(opcion == 8){
      //Modal Reservas...
      $rootScope.volverReservas = true;
      $ionicModal.fromTemplateUrl('templates/reservas.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReservas = modal;
        $rootScope.modalReservas.show();
      });
    }
  }

  $scope.detalleExperiencia = function(item){
    $rootScope.experienciaSeleccionadaNuevoHome = item;
    $rootScope.id_producto_servicio_turistico = item.id_producto_servicio_turistico;
    $ionicModal.fromTemplateUrl('templates/detalleExperiencia.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalDetalleExperiencia = modal;
      $rootScope.modalDetalleExperiencia.show();
    });
  }

  /*if( $rootScope.flagDesdeNuevoHomeAtractivos == true ){
    $rootScope.flagDesdeNuevoHomeAtractivos = false;
    $ionicModal.fromTemplateUrl('templates/atractivosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalAtractivosTuristicos = modal;
      $rootScope.modalAtractivosTuristicos.show();
    });
  }

  if( $rootScope.flagDesdeNuevoHomeServicios == true ){
    $rootScope.flagDesdeNuevoHomeServicios = false;
    $ionicModal.fromTemplateUrl('templates/serviciosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalServiciosTuristicos = modal;
      $rootScope.modalServiciosTuristicos.show();
    });
  }*/

  $scope.tutorial = function(){
    $state.go("app.tutorial");
  }

  $scope.$watch(function () {
      return $ionicSideMenuDelegate.getOpenRatio();
    },
    function (ratio) {
      if (ratio == 1){
        $scope.menuAbierto = true;
      }else{
        $scope.menuAbierto = false;
      }
    }
  );

  $scope.mostrarFiltro = function(){
    if( $scope.mostrarMenuFiltro ){
      $scope.mostrarMenuFiltro = false;
    }else{
      $scope.mostrarMenuFiltro = true;
    }
  }

  $scope.filtrar = function(filtro){
    console.log(filtro);
    var contador = 0;
    $rootScope.experienciaFiltro = [];

    if(filtro == -1){
      $rootScope.experienciaFiltro = $rootScope.experiencia;
      $scope.filtroActual  = "todas las comunas";
    }else{
      angular.forEach($rootScope.experiencia, function(value, key) {
        if( value.id_comuna == filtro){
          $rootScope.experienciaFiltro[contador] = value;
          contador +=1;
        }
      });

      angular.forEach($scope.comunas, function(value, key) {
        if(value.id_comuna == filtro){
          $scope.filtroActual  = value.nombre;
        }
      });
    }

    $scope.mostrarMenuFiltro = false;

    /*$scope.rutas_tipicas = {};
    if(filtro == 'todas'){
      $scope.rutas_tipicas = $scope.rutas_tipicas_aux;
      $scope.filtroActual  = "Todas las rutas";
      $scope.mostrarMenuFiltro = false;
    }

    if(filtro == 'enRecorrido'){
      var contador = 0;
      angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
        if(value.estado_ruta == 'en curso'){
          $scope.rutas_tipicas[contador] = value;
          contador +=1;
        }      
      });
      $scope.filtroActual  = "En recorrido";
      $scope.mostrarMenuFiltro = false;
    }

    if(filtro == 'recorridas'){
      var contador = 0;
      angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
        if(value.estado_ruta == 'completa'){

          $scope.rutas_tipicas[contador] = value;
          contador +=1;
        }      
      });
      $scope.filtroActual  = "Recorridas";
      $scope.mostrarMenuFiltro = false;
    }

    if(filtro == 'sinRecorrer'){
      var contador = 0;
      angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
        if(value.estado_ruta == null){
          $scope.rutas_tipicas[contador] = value;
          contador +=1;
        }      
      });
      $scope.filtroActual  = "Sin recorrer";
      $scope.mostrarMenuFiltro = false;
    }*/
    //console.log("Rutas_tipicas:",$scope.rutas_tipicas);
  }
  $scope.experienciaAtacama();

  if( $rootScope.volverDeServiciosAtractivos == 'atractivos' ){
    $rootScope.volverDeServiciosAtractivos = null;
    $ionicModal.fromTemplateUrl('templates/atractivosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalAtractivosTuristicos = modal;
      $rootScope.modalAtractivosTuristicos.show();
    });
  }else if ( $rootScope.volverDeServiciosAtractivos == 'servicios' ){
    $rootScope.volverDeServiciosAtractivos = null;
    $ionicModal.fromTemplateUrl('templates/serviciosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalServiciosTuristicos = modal;
      $rootScope.modalServiciosTuristicos.show();
    });
  }

  

  if( $rootScope.volverDeDejaTuHuella == true ){
    $rootScope.volverDeDejaTuHuella = false;
    $scope.exploraDescubre();
  }

  if( $rootScope.volverReservas == true ){
    $rootScope.volverReservas = false;
    // $scope.exploraDescubre();
    $scope.menuOpcion(8);
  }
  if( $rootScope.backToMenuEventos == true ){
    $rootScope.backToMenuEventos = false;
    // $scope.exploraDescubre();
    $scope.menuOpcion(5);
  }

  $ionicPlatform.registerBackButtonAction(function () {
    if (condition) {
      navigator.app.exitApp();
    } else {
      //alert("HELOU");
    }
  }, 100);

}]);