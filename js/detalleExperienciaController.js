angular.module('starter.controllers')


.controller('detalleExperienciaCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce,$ionicPopup) {
    console.log("detalle experiencia controller: "+ $rootScope.id_producto_servicio_turistico);
    $rootScope.experienciaDetalleMostrar = null;
    $scope.item = $rootScope.experienciaSeleccionadaNuevoHome;
    $scope.calificacionOriginal = $scope.item.calificacion;

    $scope.cerrarModalDetalleExperiencia = function(){
    	 $rootScope.modalDetalleExperiencia.hide();
  	}

    angular.forEach($rootScope.experiencia, function(value, key) {
      if(value.id_producto_servicio_turistico == $rootScope.id_producto_servicio_turistico){
        $rootScope.experienciaSeleccionada = value;
      }

    });

    $scope.menu = function(id){
      var identificador = id;

      $rootScope.experienciaDetalleMostrar = identificador;
      if(id == 1){
        $ionicModal.fromTemplateUrl('templates/detalleMenuExperiencia.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalDetalleMenuExperiencia = modal;
          $rootScope.modalDetalleMenuExperiencia.show();
        });
      }

      if(id == 2){
        $ionicModal.fromTemplateUrl('templates/detalleMenuExperiencia.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalDetalleMenuExperiencia = modal;
          $rootScope.modalDetalleMenuExperiencia.show();
        });
      }

      if(id == 3){
        $ionicModal.fromTemplateUrl('templates/detalleMenuExperiencia.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalDetalleMenuExperiencia = modal;
          $rootScope.modalDetalleMenuExperiencia.show();
        });
      }

      if(id == 4){
        $ionicModal.fromTemplateUrl('templates/detalleMenuExperiencia.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalDetalleMenuExperiencia = modal;
          $rootScope.modalDetalleMenuExperiencia.show();
        });
      }

      if(id == 5){
        $ionicModal.fromTemplateUrl('templates/detalleMenuExperiencia.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalDetalleMenuExperiencia = modal;
          $rootScope.modalDetalleMenuExperiencia.show();
        });
      }
    }

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.openCompartir = function(){

      $ionicModal.fromTemplateUrl('templates/experienciaAtacamaCompartir.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalExperienciaAtacamaCompartir = modal;
        $rootScope.modalExperienciaAtacamaCompartir.show();
      });

    }

    $scope.openComentar = function(){
      
      $ionicModal.fromTemplateUrl('templates/experienciaAtacamaComentar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalExperienciaAtacamaComentar = modal;
        $rootScope.modalExperienciaAtacamaComentar.show();
      });

    }

    $scope.openContacto = function(){
      
      $ionicModal.fromTemplateUrl('templates/experienciaAtacamaContacto.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalExperienciaAtacamaContacto = modal;
        $rootScope.modalExperienciaAtacamaContacto.show();
      });

    }

    $scope.addCalificacion = function(ruta,calificacion){
      if( $scope.item.calificacion == 0 ||  $scope.item.calificacion == null){
        $rootScope.toast('Calificar con mas de 0 estrellas.', 'short');
      }else{
        var myPopup = $ionicPopup.show({
          template: '<p CLASS="fuenteRoboto fuenteModal">¿Calificar experiencia con '+ $scope.item.calificacion+' estrellas?</p>',
          title: '<h4 class="izquierda"> <img src="img/preferencias/logo.png" class="corazonCalificar" >Calificación</h4>',
          cssClass: 'tituloRosado',
          scope: $scope,
          buttons: [
            { 
                text: '<i class="icon ion-close-round"></i>',
                type:'popclose',
                  onTap: function(e) {
                    // $scope.item.calificacion = 0;
                    $scope.item.calificacion = $scope.calificacionOriginal;

                  }
            },
            {
              text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo" style="margin-bottom: -30px;"></img><b>ACEPTAR</b><div>&nbsp;</div>',
              type: 'button-positive asd botonNaranja',
              onTap: function(e) {
                  $ionicLoading.show({
                    template: 'Cargando...'
                  });

                  var posOptions = {timeout: 10000, enableHighAccuracy: false};
                  navigator.geolocation.getCurrentPosition(
                    function(position){
                      var lat  = position.coords.latitude;
                      var long = position.coords.longitude;
                      $scope.addCalificacionGeo(lat,long,ruta,calificacion);
                    },function(err) {
                      $scope.addCalificacionGeo(0,0,ruta,calificacion);
                    },posOptions
                  );
              }
            }
          ]
        });

        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });     
      }
    
    }

    $scope.addCalificacionGeo = function(lat,long,ruta,calificacion){
      console.log("Calificacion: " + calificacion);
      $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionExperiencia.php",
          data: $httpParamSerializer({
            id_usuario                     : localStorage.getItem("id_usuario"),
            id_producto_servicio_turistico : $scope.item.id_producto_servicio_turistico,
            calificacion                   : calificacion
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ 
          console.log(response.data);
          $ionicLoading.hide();

          if(response.data.resultado == "ok"){
            //console.log(response.data.item_turistico);
            //$scope.marcadores = response.data.item_turistico;
            $rootScope.toast('Paquete turístico calificado', 'short');
            $scope.calificacionOriginal = response.data.promedio;
            $scope.item.calificacion    = response.data.promedio;
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
    }

}]);