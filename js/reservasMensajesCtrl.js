angular.module('starter.controllers')


.controller('reservasMensajesCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$ionicModal','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$ionicModal,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  console.log("reservasMensajesCtrl");
  

  $scope.cerrarModalMensajesReserva = function(){   
    //$rootScope.modalMensajes.remove();

    console.log("atras mensaje reserva");
    $rootScope.modalMensajes.remove();
    $rootScope.modalReservas.show();

    // $ionicModal.fromTemplateUrl('templates/reservas.html', {
    //     scope: $rootScope,
    //     animation: 'slide-in-up',
    //     backdropClickToClose: false,
    //   }).then(function(modal) {
    //     $rootScope.modalReservas = modal;
    //     $rootScope.modalReservas.show();
    // });
  }

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/mensajesReserva.php",
    data: $httpParamSerializer({
      "id"         : $rootScope.id_reserva_servicio_turistico,
      "id_usuario" : localStorage.getItem("id_usuario")
    }),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      console.log(response.data);
      $scope.mensajes = response.data.mensajes;
      $scope.foto     = response.data.foto_perfil;

      if (response.data.foto_perfil == null){
        $scope.foto = "img/perfil.jpg";
      }else{
        if( response.data.foto_perfil.includes("https") ){
          $scope.foto = response.data.foto_perfil;
        }else if( response.data.foto_perfil == "http://200.14.68.107/atacamaGoMultimedia/files/perfil/" ){
          $scope.foto = "img/perfil.jpg";
        }else if( response.data.foto_perfil != null ){
          // $scope.foto = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
        }
      }

  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });


  $scope.addMensaje = function(){
    $ionicLoading.show({
      template: 'Cargando...'
    });
    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addMensajeGeo(lat,long);
      },function(err) {
        $scope.addMensajeGeo(0,0);
      },posOptions
    );
  }

  $scope.addMensajeGeo = function(lat,long){

    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/addMensajeReserva.php",
      data: $httpParamSerializer({
        "id_reserva_servicio_turistico" : $rootScope.id_reserva_servicio_turistico,
        "estado_reserva"                : $rootScope.estado_reserva,
        "id_usuario"                    : localStorage.getItem("id_usuario"),
        "mensaje"                       : $scope.mensaje,
        "lat"                           : lat,
        "long"                          : long
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        console.log(response.data);
        
        

        if($scope.mensajes ==  undefined){

          $rootScope.toast('Mensaje agregado correctamente', 'short');
          $ionicLoading.hide();
          $scope.cerrarModalMensajesReserva();
        }
        if( $scope.mensajes === undefined || $scope.mensajes === null ){
          console.log($scope.mensajes);
          $scope.mensajes.unshift({
            "mensaje"     : $scope.mensaje,
            "tipo"        : "turista",
            "fecha"       : "ahora",
            "foto_perfil" : $scope.foto
          });
          console.log($scope.mensajes);
          // alert("1");
        }else{
          $scope.mensajes.unshift({
            "mensaje" : $scope.mensaje,
            "tipo"    : "turista",
            "fecha"   : "ahora",
            "foto_perfil" : $scope.foto
          });
          // alert("2");
        }
        

        $scope.mensaje = "";
        $ionicLoading.hide();

    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
      $ionicLoading.hide();
    });
  }

}]);