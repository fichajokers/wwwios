angular.module('starter.controllers')


.controller('experienciaAtacamaContactoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce','$cordovaClipboard',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce,$cordovaClipboard) {
    console.log("detalle experiencia controller: "+ $rootScope.id_producto_servicio_turistico);
    $rootScope.experienciaDetalleMostrar = null;
    $scope.item = $rootScope.experienciaSeleccionadaNuevoHome;

    $scope.copiarCorreo = function(correo){
      //console.log(correo)
       $cordovaClipboard
        .copy(correo)
        .then(function () {
          // success
          //alert(1)
          $rootScope.toast('Email copiado al portapapeles', 'short');
        }, function () {
          // error
        });
    }

    $scope.cerrarModalExperienciaAtacamaContacto = function(){
    	 $rootScope.modalExperienciaAtacamaContacto.hide();
  	}

  	$scope.goToNuevoHome = function(){
      $rootScope.modalDetalleExperiencia.remove();
      $rootScope.modalExperienciaAtacamaContacto.hide();
    }

    $scope.trustAsHtml = function(string) {
      //console.log("entro", string);
      return $sce.trustAsHtml(string);
    };

    $scope.link = function(url){
      //console.log("entro", url);
      window.open(url,"_system",'location=yes');
    }

}]);