angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$ionicSideMenuDelegate,$rootScope,$ionicNativeTransitions,$ionicHistory,$ionicViewService,$state) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};
  $rootScope.disableBtnRutasTipicas = false;

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.modal = modal;
  });


  $scope.openModalMisDatos = function(){

    //Modal mis datos...
    $ionicModal.fromTemplateUrl('templates/misDatos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalMisDatos = modal;

      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView()){
        //$rootScope.map.setClickable(false);  
      }
      
      $rootScope.modalMisDatos.show();
    });

  }


  $scope.openModalContacto = function(){

    //Modal mis datos...
    $ionicModal.fromTemplateUrl('templates/contacto.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalContacto = modal;

      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView()){
        //$rootScope.map.setClickable(false);  
      }
      
      $rootScope.modalContacto.show();
    });

  }

  //Modal configuracion...
  $ionicModal.fromTemplateUrl('templates/configuracion.html', {
    scope: $rootScope,
    animation: 'slide-in-up',
    backdropClickToClose: false,
  }).then(function(modal) {
    $rootScope.modalConfiguracion = modal;
  });

  $scope.openModalConfiguracion = function(){
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView())
      //$rootScope.map.setClickable(false);  
    
    $rootScope.modalConfiguracion.show();
  }

  

  $scope.openModalPreferencias = function(){

    //Modal preferencias...
    $ionicModal.fromTemplateUrl('templates/preferencias.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalPreferencias = modal;
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView()){
        //$rootScope.map.setClickable(false);  
      }  
      
      $rootScope.modalPreferencias.show();
    });
  }


  //Modal preferencias2...
  $scope.openModalPreferencias2 = function(){
    $ionicModal.fromTemplateUrl('templates/preferencias2.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalPreferencias2 = modal;
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);
      $rootScope.modalMenuAccionesModificar.show();
      
    });
  };
  
  /*$scope.openModalPreferencias2 = function(){
    //hacer que el mapa no sea clickeable.
    if( ionic.Platform.isAndroid() )
      $rootScope.map.setClickable(false);  
    
    $rootScope.modalPreferencias2.show();
  }*/

  //Modal Mis rutas...
  $ionicModal.fromTemplateUrl('templates/misRutas.html', {
    scope: $rootScope,
    animation: 'slide-in-up',
    backdropClickToClose: false,
  }).then(function(modal) {
    $rootScope.modalMisrutas = modal;
  });

  $scope.openModalMisRutas = function(){
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView())
      $rootScope.map.setClickable(false);  
    
    $rootScope.modalMisrutas.show();
  }

  $scope.openModalSlider = function(){
    $ionicModal.fromTemplateUrl('templates/modalSlider.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalSlider = modal;
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView())
        $rootScope.map.setClickable(false);
      $rootScope.modalSlider.show();
      
    });
  }

  $scope.openModalRutasTipicas = function(){

    if( $rootScope.disableBtnRutasTipicas == false){
        $rootScope.disableBtnRutasTipicas = true;
    }

    //esconder popove y abris el menu...
    $rootScope.popover.hide();
    
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView())
      $rootScope.map.setClickable(false);  
    
    //Modal rutas Tipicas...
    $ionicModal.fromTemplateUrl('templates/rutasTipicas.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalRutasTipicas = modal;
      $rootScope.modalRutasTipicas.show();
    });

  }

  $scope.openModalRutasTipicasOffline = function(){
    
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView()){
      $rootScope.map.setClickable(false);  
    }
      
    
    //Modal rutas Tipicas...
    $ionicModal.fromTemplateUrl('templates/rutasTipicasOffline.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalRutasTipicasOffline = modal;
      $rootScope.modalRutasTipicasOffline.show();
    });

  }

  $scope.openModalReservas = function(){
    
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView()){
      //$rootScope.map.setClickable(false);  
    }
      
    
    //Modal rutas Tipicas...
    $ionicModal.fromTemplateUrl('templates/reservas.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalReservas = modal;
      $rootScope.modalReservas.show();
    });

  }

  //Modal rutas Compartidas...
  $ionicModal.fromTemplateUrl('templates/rutasCompartidas.html', {
    scope: $rootScope,
    animation: 'slide-in-up',
    backdropClickToClose: false,
  }).then(function(modal) {
    $rootScope.modalRutasCompartidas = modal;
  });

  $scope.openModalRutasCompartidas = function(){
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView())
      $rootScope.map.setClickable(false);  

    $rootScope.modalRutasCompartidas.show();
  }

  //Modal login...
  $ionicModal.fromTemplateUrl('templates/modalLogin.html', {
    scope: $rootScope,
    animation: 'slide-in-up',
    backdropClickToClose: false,
  }).then(function(modal) {
    $rootScope.modalLogin = modal;
  });

  $scope.openModalEventos = function(){
    
    //hacer que el mapa no sea clickeable.
    if(ionic.Platform.isWebView()){
      $rootScope.map.setClickable(false);  
    }  
    
    //Modal rutas Tipicas...
    $ionicModal.fromTemplateUrl('templates/eventos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalEventos = modal;
      $rootScope.modalEventos.show();
    });

  }

  $scope.openModalLogin = function(){
    //eliminar Local Storage...
    localStorage.removeItem('id_usuario');
    localStorage.removeItem('id_agenda');
    localStorage.removeItem('correo');
    
    $state.go("login");
  }

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.pdf = function() {
    if( ionic.Platform.isIOS() )
          cordova.InAppBrowser.open('http://200.14.68.107/atacamaGo/manual/AtacamaGoIOS6.pdf', '_blank', 'location=yes');
      else
        window.open("http://200.14.68.107/atacamaGo/manual/AtacamaGo6.pdf","_system",'location=yes');
  }
  
  // Perform the login action when the user submits the login form
  /*
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  }; */

  $scope.$watch(function(){
    return $ionicSideMenuDelegate.getOpenRatio();
  }, function(newValue, oldValue) {
    if (newValue == 0){
      $scope.hideLeft = true;
    } else{
      $scope.hideLeft = false;
    }
  });

  /**
  * [cerrarSesion]
  * Cierra la sesion elimina el localsotrage, cierra el mapa y se va a la pantalla de login.
  */
  $scope.cerrarSesion = function(){
    
  }

})

