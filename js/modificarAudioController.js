angular.module('starter.controllers')


.controller('modificarAudioCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$ionicModal','$httpParamSerializer',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$ionicModal,$httpParamSerializer) {
  // With the new view caching in Ionic, Controllers are only called
    
    $scope.item = $rootScope.item_seleccionado;
    
    $scope.audios = [];
    if( $rootScope.tipo_seleccionado == "servicio" ){
        $scope.url="http://200.14.68.107/atacamaGoMultimedia";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getAudioServicio.php",
          data: $httpParamSerializer({
            "id_servicio": $rootScope.item_seleccionado.id,
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.audio = response.data.servicio;

            angular.forEach($scope.audio, function(value, key) {
              $scope.audios[key] = { 
                "audio" :$scope.url + value.audio
              };
            });
            
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay audios', 'short');
            //$scope.cerrarModalModificarAudio();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
        $scope.url="http://200.14.68.107/atacamaGoMultimedia";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getAudioPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio": $rootScope.item_seleccionado.id,
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.audio = response.data.patrimonio;

            angular.forEach($scope.audio, function(value, key) {
              $scope.audios[key] = { 
                "audio" :$scope.url + value.audio
              };
            });
            
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay audios', 'short');
            //$scope.cerrarModalModificarAudio();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }


  $scope.cerrarModalModificarAudio = function(){   
    $rootScope.modalModificarAudio.remove();
  }

  //modal visor de Audio...
  $ionicModal.fromTemplateUrl('templates/visorAudio.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalVisorAudio = modal;
  });
      
  $scope.openModalVisorAudio = function(audio) {
    console.log("audio que llega: " + audio);
    $scope.audioUrl = null;
    $scope.audioUrl = audio;

    $scope.modalVisorAudio.show();
  }

}]);