angular.module('starter.controllers')


.controller('agregarAudioCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.cerrarModalAgregarAudio = function(){   
    $rootScope.modalAgregarAudio.remove()
  }

  $scope.chooseAudio = function () {
        var options = {
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType       : Camera.MediaType.VIDEO,
            correctOrientation : true
        };
   
        $cordovaCamera.getPicture(options).then(function (videoData) {
            $scope.rutaVideo     = videoData;
            $scope.nombreVideo   = Math.floor((Math.random() * 9999999) + 1)+"_"+videoData.substr(videoData.lastIndexOf('/') + 1);
            console.log($scope.rutaVideo, $scope.nombreVideo);
        }, function (err) {
          //alert(err);
            // An error occured. Show a message to the user
        });
    }

 	$scope.capturarAudio = function() {
    console.log("hola");
    var options = { limit: 1, duration: 30 };

    $cordovaCapture.captureAudio(options).then(function(audioData) {
      // Success! Audio data is here
      console.log(audioData);
    }, function(err) {
      console.log(err);
      // An error occurred. Show a message to the user
    });
  }

}]);