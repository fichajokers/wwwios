angular.module('starter.controllers')


.controller('eventoVerCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$ionicPopup','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$ionicPopup,$ionicNativeTransitions) {
  $scope.nombre = "lalal 123";
  // With the new view caching in Ionic, Controllers are only called
    document.addEventListener("deviceready", function() {
        var div = document.getElementById("map_canvas222");
        var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);

        // Initialize the map view
        $rootScope.map = plugin.google.maps.Map.getMap(div,{
          'backgroundColor' : 'white',
          'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
          'controls' : {
            'compass'          : true,
            'myLocationButton' : true,
            'indoorPicker'     : true,
            'zoom'             : false
          },
          'gestures': {
            'scroll' : true,
            'tilt'   : true,
            'rotate' : true,
            'zoom'   : true
          },
          'camera': {
            'latLng' : COPIAPO,
            'tilt'   : 0,
            'zoom'   : 10,
            'bearing': 0
          }
        });

        // Wait until the map is ready status.
        $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);
    }, false);

      function onMapReady() {
        $rootScope.map.setPadding( 0, 0 , 200 , 0);
        $scope.markerArrayPatrimonioReserva = [];
        console.log($rootScope.marcadorEvento);
        var value = $rootScope.marcadorEvento;
        //angular.forEach($rootScope.marcadorEvento, function(value, key) {
          console.log(value);

          var ubicacion = new plugin.google.maps.LatLng(
          parseFloat(value.georeferenciacion_evento_latitud),
          parseFloat(value.georeferenciacion_evento_longitud) );

          var url = "www/img/gps.png";


          $rootScope.map.animateCamera({
            target: {
              lat: parseFloat(value.georeferenciacion_evento_latitud),
              lng: parseFloat(value.georeferenciacion_evento_longitud)},
            zoom: 17,
            duration: 500
          }, function() {

            $rootScope.map.addMarker({
              'position': ubicacion,
              'title': value.nombre_item_turistico,
              icon: {
                url: url,
                size: { width: 30, height: 45 },
                anchor:  [10, 10],
              },
                zIndex: 1
            },function(marker) {
              //accion al hacer clic en el marcador
              $scope.markerArrayPatrimonioReserva     = marker;
                marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                $rootScope.tipo_seleccionado = "evento";
                $rootScope.item_seleccionado = value;
                $rootScope.evento_seleccionado = $rootScope.marcadorEvento;
                $rootScope.fromMapa = true;
                $scope.openModalEventoSeleccionado();
              });
            });

          });
        //});

        document.addEventListener('backbutton', function (event) {
          event.preventDefault();
          event.stopPropagation();
          $rootScope.map.remove();
          $rootScope.volverDeServiciosAtractivos = true;
        }, false);
      }

      function onBtnClicked() {
        map.showDialog();
      }

      $scope.cerrarEventoVer = function(){
        $rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();
        //$state.go("app.nuevoHome");
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
      }

      $scope.volverMenuEventos = function(){
        $rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();
        $rootScope.backToMenuEventos = true;
        //$state.go("app.nuevoHome");
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
      }
      
      

      /*$scope.openModalEventoSeleccionado = function(){
        $rootScope.map.setClickable(false);
        
        $ionicModal.fromTemplateUrl('templates/eventoSeleccionado.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalEventoSeleccionado = modal;
          $rootScope.modalEventoSeleccionado.show();
      });
      }*/
}]);