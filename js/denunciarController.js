angular.module('starter.controllers')


.controller('denunciarCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.denuncia = "";
  $scope.item = $rootScope.item_seleccionado;

  $scope.cerrarModalDenunciar = function(){   
    $rootScope.modalDenunciar.hide()
  }

  $scope.cerrarModalDenunciarMapa = function(){   
    console.log("cerrar denunciar");
    $rootScope.modalDenunciar.hide()
    console.log("cerrar menu modificar");
    $rootScope.map.setClickable(true);
    $rootScope.modalMenuAccionesModificar.remove();
  }

  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#AA1664"
    }
  }
  
  $scope.denunciar = function(denuncia){
    $ionicLoading.show({
      template: 'Enviando denuncia...'
    });
    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.realizarDenuncia(lat,long,denuncia);
      },function(err) {
        $scope.realizarDenuncia(0,0,denuncia);
      },posOptions
    );
  }

  $scope.realizarDenuncia = function(lat,long,denuncia){
  
    console.log(denuncia);
    console.log($scope.denuncia);
    if( $scope.denuncia == undefined || $scope.denuncia == "" ){
      $rootScope.toast('Debes llenar el campo de denuncia', 'short');
    }else{
      $ionicLoading.show({
        template: 'Enviando denuncia...'
      });
      
      if( $rootScope.tipo_seleccionado == "item" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addDenunciaItemTuristico.php",
          data: $httpParamSerializer({
            "id_item"    : $rootScope.item_seleccionado.id,
            "id_usuario" : localStorage.getItem("id_usuario"),
            "denuncia"   : $scope.denuncia,
            "lat"        : lat,
            "long"       : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacama/public/usuario/denunciaMovil",
              data: $httpParamSerializer({
                "nombre"     : $rootScope.item_seleccionado.nombre,
                "tipo"       : "ítem turístico",
                "denuncia"   : $scope.denuncia,
                "id_usuario" : localStorage.getItem("id_usuario")
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
              console.log(response.data);
              if(response.data.resultado == "ok"){
                $scope.denuncia = "";
                $ionicLoading.hide();
                $rootScope.toast('Denuncia emitida', 'short');
              }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
                $ionicLoading.hide();
              }
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
                $ionicLoading.hide();
            });

          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
            $ionicLoading.hide();
          }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
        });  
      }

      if( $rootScope.tipo_seleccionado == "servicio" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addDenunciaServicio.php",
          data: $httpParamSerializer({
            "id_servicio_turistico" : $rootScope.item_seleccionado.id,
            "id_usuario"            : localStorage.getItem("id_usuario"),
            "denuncia"              : $scope.denuncia,
            "lat"                   : lat,
            "long"                  : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacama/public/usuario/denunciaMovil",
              data: $httpParamSerializer({
                "nombre"     : $rootScope.item_seleccionado.nombre,
                "tipo"       : "servicio",
                "denuncia"   : $scope.denuncia,
                "id_usuario" : localStorage.getItem("id_usuario")
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
              console.log(response.data);
              if(response.data.resultado == "ok"){
                $scope.denuncia = "";
                $ionicLoading.hide();
                $rootScope.toast('Denuncia emitida', 'short');
              }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
                $ionicLoading.hide();
              }
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
                $ionicLoading.hide();
            });

          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      }

      if( $rootScope.tipo_seleccionado == "patrimonio" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addDenunciaPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio"  : $rootScope.item_seleccionado.id,
            "id_usuario"     : localStorage.getItem("id_usuario"),
            "denuncia"       : $scope.denuncia,
            "lat"            : lat,
            "long"           : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacama/public/usuario/denunciaMovil",
              data: $httpParamSerializer({
                "nombre"   : $rootScope.item_seleccionado.nombre,
                "tipo"     : "atractivo",
                "denuncia" : $scope.denuncia,
                "id_usuario" : localStorage.getItem("id_usuario")
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
              console.log(response.data);
              if(response.data.resultado == "ok"){
                $scope.denuncia = "";
                $ionicLoading.hide();
                $rootScope.toast('Denuncia emitida', 'short');
              }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
                $ionicLoading.hide();
              }
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
                $ionicLoading.hide();
            });
            
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      }
    }
  }

}]);