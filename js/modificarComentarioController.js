angular.module('starter.controllers')


.controller('modificarComentarioCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicScrollDelegate','$timeout','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicScrollDelegate,$timeout,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.esAndroid = false;
  if( ionic.Platform.isAndroid() ){
    $scope.esAndroid = true;
  }else{
    $scope.esAndroid = false;
  }

  $timeout(function() {
    $ionicScrollDelegate.scrollBottom();
  });

  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.imgComentario = "img/menuAcciones/invComentariosAtractivo.png";
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.imgComentario = "img/menuAcciones/comentarios.png";
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.imgComentario = "img/deja_tu_huella/invComentariosServicio.png";
    $scope.estiloBarraModificar = {
      "background-color" : "#3B3C97"
    }
  }

	$scope.comentarios         = [];
  $scope.comentariosTotal    = [];
	$scope.item                = [];
  $scope.item.comentario     = "";
	$scope.foto_perfil_usuario = "";
  $scope.cantComentarios     = 2; //inicializa en 2 para cuando se llamela fx se rescaten 20...
  $scope.id_usuario          = localStorage.getItem("id_usuario");

  $scope.itemSelect = $rootScope.item_seleccionado;

  if( $rootScope.tipo_seleccionado == "item" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getComentarioItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico": $rootScope.item_seleccionado.id,
            "id_usuario"       : localStorage.getItem("id_usuario")
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
        	console.log(response);
          if(response.data.resultado == "ok"){
            $scope.comentariosTotal    = response.data.comentario;
            //obtengo los 10 primeros comentarios
            angular.forEach($scope.comentariosTotal, function(value, key) {
              if( key < 4){
                $scope.comentarios.push($scope.comentariosTotal[key]);    
              }
            });
            
            if( response.data.foto_perfil_usuario.includes("https") ){
              $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            }else{
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
            }

            //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay comentarios', 'short');
            //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            
            if( response.data.foto_perfil_usuario.includes("https") ){
              $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            }else{
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
            }
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getComentarioServicio.php",
          data: $httpParamSerializer({
            "id_servicio": $rootScope.item_seleccionado.id,
            "id_usuario" : localStorage.getItem("id_usuario")
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          if(response.data.resultado == "ok"){
            console.log(response.data.comentario);
          	$scope.comentariosTotal    = response.data.comentario;
            //obtengo los 10 primeros comentarios
            angular.forEach($scope.comentariosTotal, function(value, key) {
              if( key < 4){
                $scope.comentarios.push($scope.comentariosTotal[key]);    
              }
            });
            //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            console.log(response.data);

            if( response.data.foto_perfil_usuario.includes("https") ){
              $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            }else{
              //$scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.datos.foto_perfil;
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
            }
          
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay comentarios', 'short');
            //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            if( response.data.foto_perfil_usuario.includes("https") ){
              $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            }else{
              //$scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.datos.foto_perfil;
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
            }
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getComentarioPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio": $rootScope.item_seleccionado.id,
            "id_usuario"   : localStorage.getItem("id_usuario")
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response);
          if(response.data.resultado == "ok"){
          	$scope.comentariosTotal    = response.data.comentario;
            //obtengo los 10 primeros comentarios
            angular.forEach($scope.comentariosTotal, function(value, key) {
              if( key < 4){
                $scope.comentarios.push($scope.comentariosTotal[key]);    
              }
            });
            //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            if( response.data.foto_perfil_usuario.includes("https") ){
              $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            }else{
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
            }
         
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay comentarios', 'short');
            //$scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            if( response.data.foto_perfil_usuario.includes("https") ){
              $scope.foto_perfil_usuario = response.data.foto_perfil_usuario;
            }else{
              $scope.foto_perfil_usuario = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.foto_perfil_usuario;
            }
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

  $scope.cerrarModalModificarComentario = function(){   
    $rootScope.modalModificarComentario.remove();
  }


  $scope.cerrarModalModificarComentarioMapa = function(){   
    console.log("cerrar modificar comentario");
    $rootScope.modalModificarComentario.remove();
    console.log("cerrar menu modificar");
    $rootScope.map.setClickable(true);
    $rootScope.modalMenuAccionesModificar.remove();
  }

  $scope.addComentario = function(item){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addComentarioGeo(lat,long,item);
      },function(err) {
        $scope.addComentarioGeo(0,0,item);
      },posOptions
    );
  }

  $scope.addComentarioGeo = function(lat,long,item){   
    if( item.comentario != "" || item.length > 0){
 
    	if( $rootScope.tipo_seleccionado == "item" ){
	        $http({
	          method: "POST",
	          url: "http://200.14.68.107/atacamaGo/addComentarioItemTuristico.php",
	          data: $httpParamSerializer({
	            "id_item_turistico" : $rootScope.item_seleccionado.id,
	            "id_usuario"        : localStorage.getItem("id_usuario"),
	            "comentario"        : item.comentario,
              "lat"               : lat,
              "long"              : long
	        }),
	          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	        }).then(function(response){ //ok si guardó correctamente.
	        	console.log(response);
	          if(response.data.resultado == "ok"){
              $scope.comentarios.unshift({
                "foto_perfil"      : $scope.foto_perfil_usuario,
                "texto_comentario" : item.comentario,
                "id_usuario"       : $scope.id_usuario,
                "id"               : response.data.id
              });

              $scope.comentariosTotal.unshift({
                "foto_perfil"      : $scope.foto_perfil_usuario,
                "texto_comentario" : item.comentario,
                "id_usuario"       : $scope.id_usuario,
                "id"               : response.data.id
              });
              $scope.item.comentario = "";
	            $rootScope.toast('comentario agregado correctamente', 'short');
	          }else{
	            $rootScope.toast('error, intenta nuevamente', 'short');
	          }
            $ionicLoading.hide();
	        }, function(){ //Error de conexión
	          $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
	        });
	    }

	    if( $rootScope.tipo_seleccionado == "servicio" ){
	      	$http({
	          method: "POST",
	          url: "http://200.14.68.107/atacamaGo/addComentarioServicio.php",
	          data: $httpParamSerializer({
	            "id_servicio" : $rootScope.item_seleccionado.id,
	            "id_usuario"  : localStorage.getItem("id_usuario"),
	            "comentario"  : item.comentario,
              "lat"         : lat,
              "long"        : long
	        }),
	          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	        }).then(function(response){ //ok si guardó correctamente.

	          if(response.data.resultado == "ok"){
              $scope.comentarios.unshift({
                "foto_perfil"      : $scope.foto_perfil_usuario,
                "texto_comentario" : item.comentario,
                "id_usuario"       : $scope.id_usuario,
                "id"               : response.data.id
              });

              $scope.comentariosTotal.unshift({
                "foto_perfil"      : $scope.foto_perfil_usuario,
                "texto_comentario" : item.comentario,
                "id_usuario"       : $scope.id_usuario,
                "id"               : response.data.id
              });

              $scope.item.comentario = "";
	            $rootScope.toast('comentario agregado correctamente', 'short');
	          }else{
	            $rootScope.toast('error, intenta nuevamente', 'short');
	          }
            $ionicLoading.hide();
	        }, function(){ //Error de conexión
	          $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
	        });
	    }

	    if( $rootScope.tipo_seleccionado == "patrimonio" ){
	    	$http({
	          method: "POST",
	          url: "http://200.14.68.107/atacamaGo/addComentarioPatrimonio.php",
	          data: $httpParamSerializer({
	            "id_patrimonio": $rootScope.item_seleccionado.id,
	            "id_usuario"   : localStorage.getItem("id_usuario"),
	            "comentario"   : item.comentario,
              "lat"          : lat,
              "long"         : long
	        }),
	          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	        }).then(function(response){ //ok si guardó correctamente.
            console.log(response.data);
	          if(response.data.resultado == "ok"){
              $scope.comentarios.unshift({
                "foto_perfil"      : $scope.foto_perfil_usuario,
                "texto_comentario" : item.comentario,
                "id_usuario"       : $scope.id_usuario,
                "id"               : response.data.id
              });

              $scope.comentariosTotal.unshift({
                "foto_perfil"      : $scope.foto_perfil_usuario,
                "texto_comentario" : item.comentario,
                "id_usuario"       : $scope.id_usuario,
                "id"               : response.data.id
              });
              $scope.item.comentario = "";
	            $rootScope.toast('comentario agregado correctamente', 'short');
	          }else{
	            $rootScope.toast('error, intenta nuevamente', 'short');
	          }
            $ionicLoading.hide();
	        }, function(){ //Error de conexión
	          $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
	        });
	    }

    }else{
      $ionicLoading.hide();
      //comentario en blanco, no ha sido guardado
      $rootScope.toast('Comentario en blanco, no ha sido guardado', 'short');

    	console.log("pa que po");

    }
  }

  $scope.deleteComentario = function(comentario,index){
    console.log(comentario);

    if( $rootScope.tipo_seleccionado == "item" ){
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/deleteComentarioItem.php",
        data: $httpParamSerializer({
          "id": comentario.id
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        if(response.data.resultado == "ok"){
          $scope.comentarios.splice(index, 1);
          $scope.item.comentario = "";

          angular.forEach($scope.comentariosTotal, function(value, key) {
            if( value.id == comentario.id ){
              $scope.comentariosTotal.splice(key, 1);
            }
          });

          $rootScope.toast('comentario eliminado correctamente', 'short');
        }else{
          $rootScope.toast('error, intenta nuevamente', 'short');
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
      }); 
    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/deleteComentarioServicio.php",
        data: $httpParamSerializer({
          "id": comentario.id
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        if(response.data.resultado == "ok"){
          $scope.comentarios.splice(index, 1);
          $scope.item.comentario = "";

          console.log(index);
          console.log($scope.comentariosTotal);
          console.log(comentario);

          angular.forEach($scope.comentariosTotal, function(value, key) {
            if( value.id == comentario.id ){
              $scope.comentariosTotal.splice(key, 1);
            }
          });
          
          $rootScope.toast('comentario eliminado correctamente', 'short');
        }else{
          $rootScope.toast('error, intenta nuevamente', 'short');
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
      });   
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/deleteComentarioPatrimonio.php",
        data: $httpParamSerializer({
          "id": comentario.id
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        if(response.data.resultado == "ok"){
          $scope.comentarios.splice(index, 1);
          $scope.item.comentario = "";

          angular.forEach($scope.comentariosTotal, function(value, key) {
            if( value.id == comentario.id ){
              $scope.comentariosTotal.splice(key, 1);
            }
          });
          
          $rootScope.toast('comentario eliminado correctamente', 'short');
        }else{
          $rootScope.toast('error, intenta nuevamente', 'short');
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
      });
    }

  }

  $scope.masComentarios = function(cantComentarios){
    $scope.comentarios = [];
    //obtengo los comentarios 20, 30, 40, etc...
    console.log(cantComentarios);
    console.log($scope.comentariosTotal);
    angular.forEach($scope.comentariosTotal, function(value, key) {
      if( key < (cantComentarios*4) ){
        $scope.comentarios.push($scope.comentariosTotal[key]);    
      }
    });
    $scope.cantComentarios ++;
  }

}]);