angular.module('starter.controllers')


.controller('eventoSeleccionadoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaCamera','$cordovaFileTransfer','$timeout','$ionicLoading','$ionicModal','$sce',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaCamera,$cordovaFileTransfer,$timeout,$ionicLoading,$ionicModal,$sce) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.mostrarVerMapa = true;
  console.log( $rootScope.evento_seleccionado );

  $scope.cerrarModalEventoSeleccionado = function(){
    console.log("cerar modal");
    if( $rootScope.desdeEventosController = true ){

    }else{
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(true);
    }

    if( $rootScope.fromMapa == true ){
      $rootScope.map.setClickable(true);
    }else{

    }
    $rootScope.fromMapa = false;
    $rootScope.desdeEventosController = false;
    $rootScope.modalEventoSeleccionado.remove();
  }

  $scope.volverServiciosAtractivos = function(){
    $rootScope.fromMapa = false;
    $rootScope.map.setClickable(true);
    
    $rootScope.modalEventoSeleccionado.remove();

    $rootScope.modalEventos.remove();
  }

  //para transformar el texto a html, ya que no pasaba la etiqueta style=".."
  $scope.trustAsHtml = function(string) {
    return $sce.trustAsHtml(string);
  };

  $scope.hrefWeb = function(link){
    window.open(link,'_blank','location=yes');
  }

  $scope.mailToCorreo = function(link){
    window.open('mailto:${link}','_system');
  }

  

  $scope.verEvento = function(){
    $rootScope.marcadorEvento = $rootScope.evento_seleccionado;
    //$scope.ocultarModalReservas();
    $rootScope.modalEventoSeleccionado.remove();
    $rootScope.modalEventos.remove();
    $state.go("eventoVer");
  }

  if( $rootScope.fromMapa == true ){
    $scope.mostrarVerMapa = false;
  }else{

  }

  $scope.openModalCompartir = function(){

    $rootScope.enEvento = true;
    $rootScope.fromEventoSeleccionado = true;
    $rootScope.linkTwitter  = encodeURI("http://www.atacama-go.cl/eventos/"+$rootScope.evento_seleccionado.es_evento+"/" + $rootScope.evento_seleccionado.id_evento);
    $rootScope.linkFacebook = encodeURI("http://www.atacama-go.cl/eventos/"+$rootScope.evento_seleccionado.es_evento+"/" + $rootScope.evento_seleccionado.id_evento);
    //Modal Modificar Descripcion...
    $ionicModal.fromTemplateUrl('templates/compartir.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarCompartir = modal;
      
      $rootScope.modalModificarCompartir.show();

    });
  }

}]);