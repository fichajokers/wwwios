angular.module('starter.controllers')


.controller('preferencias2Ctrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  
	//$scope.preferencia = [];
	$scope.mostrarAcompanado        = false;
	$scope.cuestionario             = {};
	$scope.comida                   = "";
	$scope.tipoTours                = "";
  	
  	$scope.noRespondeActividades    = false;
	$scope.noRespondeComida         = false;
	$scope.noRespondeDuracion       = false;
	$scope.noRespondeGasto          = false;
	$scope.noRespondeNumeroPersonas = false;
	$scope.noRespondeSoloAcom       = false;
	$scope.noRespondeTipoServicio   = false;
	$scope.noRespondeTipoTours      = false;
  	$scope.noRespondeTransporte     = false;
  	$scope.noRespondeTipoViaje      = false;
  	$scope.noRespondeMoneda         = false;
  	
  $http({
	    method: "POST",
	    url: "http://200.14.68.107/atacamaGo/getPreferencia.php",
	    data: $httpParamSerializer({
	  		"id_usuario"    : localStorage.getItem("id_usuario")
	  }),
	    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	  }).then(function(response){ //ok si guardó correctamente.
	  	//console.log(response.data)
	    if(response.data.resultado == "ok"){    	
	    }
	  }, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet', 'short')
	  });

  $scope.cerrarModalPreferencias2 = function(){
    console.log("cerar modal");
    $rootScope.map.setClickable(true); 
    $rootScope.modalPreferencias2.hide()
  }

  $scope.addCuestionario = function(cuestionario){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addCuestionarioGeo(lat,long,cuestionario);
      },function(err) {
        $scope.addCuestionarioGeo(0,0,cuestionario);
      },posOptions
    );
  }

  $scope.addCuestionarioGeo = function(lat,long,cuestionario){
  	console.log(cuestionario);

  	if( cuestionario == [] ){
  		console.log("asdfsdfasdfgasd");
  		$scope.noRespondeActividades    = true;
		$scope.noRespondeComida         = true;
		$scope.noRespondeDuracion       = true;
		$scope.noRespondeGasto          = true;
		$scope.noRespondeNumeroPersonas = true;
		$scope.noRespondeSoloAcom       = true;
		$scope.noRespondeTipoServicio   = true;
		$scope.noRespondeTipoTours      = true;
	  	$scope.noRespondeTransporte     = true;
	  	$scope.noRespondeTipoViaje      = true;
	  	$scope.noRespondeMoneda         = true;
  	}else{
  		$scope.noRespondeActividades    = false;
		$scope.noRespondeComida         = false;
		$scope.noRespondeDuracion       = false;
		$scope.noRespondeGasto          = false;
		$scope.noRespondeNumeroPersonas = false;
		$scope.noRespondeSoloAcom       = false;
		$scope.noRespondeTipoServicio   = false;
		$scope.noRespondeTipoTours      = false;
	  	$scope.noRespondeTransporte     = false;
	  	$scope.noRespondeTipoViaje      = false;
	  	$scope.noRespondeMoneda         = false;
  	}

  	if( cuestionario.actividades == null    || cuestionario.actividades=="" ){
  		$scope.noRespondeActividades = true;
  	}
	if( cuestionario.comida == null         || cuestionario.comida == "" ){
		$scope.noRespondeComida = true;
	}
	if( cuestionario.duracion == null       || cuestionario.duracion == "" ){
		$scope.noRespondeDuracion = true;
	}
	if( cuestionario.gasto == null          || cuestionario.gasto == "" ){
		$scope.noRespondeGasto = true;
	}
	if( cuestionario.numeroPersonas == null || cuestionario.numeroPersonas == "" ){
		$scope.noRespondeNumeroPersonas = true;
	}
	if( cuestionario.soloAcom == null       || cuestionario.soloAcom == "" ){
		$scope.noRespondeSoloAcom = true;
	}
	if( cuestionario.tipoServicio == null   || cuestionario.tipoServicio == "" ){
		$scope.noRespondeTipoServicio = true;
	}
	if( cuestionario.tipoTours == null      || cuestionario.tipoTours == "" ){
		$scope.noRespondeTipoTours = true;
	}
	if( cuestionario.transporte == null     || cuestionario.transporte == ""){
  		$scope.noRespondeTransporte = true;
  	}
  	if( cuestionario.tipoViaje == null     || cuestionario.tipoViaje == ""){
  		$scope.noRespondeTipoViaje = true;
  	}
  	if( cuestionario.moneda == null     || cuestionario.moneda == ""){
  		$scope.noRespondeMoneda = true;
  	}

	
  	$http({
	    method: "POST",
	    url: "http://200.14.68.107/atacamaGo/addPreferencia.php",
	    data: $httpParamSerializer({
	  		"cuestionario" : JSON.stringify(cuestionario),
	  		"id_usuario"   : localStorage.getItem("id_usuario"),
	  		"lat"          : lat,
        	"long"         : long
	  }),
	    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	  }).then(function(response){ //ok si guardó correctamente.
	  	console.log(response.data)
	    if(response.data.resultado == "ok"){
	      $rootScope.toast('preferencias actualizadas', 'short');
	      $ionicLoading.hide();
	      $scope.cerrarModalPreferencias();
	    }else{
	      $rootScope.toast('Debes responder todas las preguntas', 'short');
	    }
	    $ionicLoading.hide();
	  }, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet', 'short')
	  });
	 
  }

  	$scope.acompanado = function(){
  		$scope.mostrarAcompanado = true;
  	}

  	$scope.solo = function(){		
  		$scope.mostrarAcompanado = false;
  		$scope.cuestionario.numeroPersonas = 0;
  	}

}]);