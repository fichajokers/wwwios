angular.module('starter.controllers')


.controller('saberMasCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$ionicPopup','$sce',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$ionicPopup,$sce) {
    console.log("saber mas controller")
    console.log($rootScope.rutaSeleccionada);
    $scope.ruta = $rootScope.rutaSeleccionada;
    $scope.estado_ruta = "";
    $scope.tieneRutaContinuar = false;
    console.log($scope.ruta);


    if($scope.ruta.estado_ruta == "en curso"){
      $scope.estado_ruta = "EN PAUSA";
      $scope.estado_ruta_margen = {
        "margin-left" : "25px"
      };
      $scope.tieneRutaContinuar = true;
    }

    if($scope.ruta.estado_ruta == "completa"){
      $scope.estado_ruta = "CONTINUAR RUTA";
      $scope.estado_ruta_margen = {
        "margin-left" : "10px"
      };
      $scope.tieneRutaContinuar = true;
    }

    $scope.cerrarModalSaberMas = function(){
    	 $rootScope.modalSaberMas.hide();
  	}

    $scope.volverrutasTipicas = function(){
      $rootScope.modalSaberMas.hide();
      $rootScope.modalRutasTipicas.hide();
    }

  	$scope.slideChanged3 = function(index) {
      $scope.slideIndex = index;
    };

    $scope.addCalificacion = function(ruta,calificacion){
      if( $scope.ruta.calificacion_usuario == 0 ||  $scope.ruta.calificacion_usuario == null ){
        $rootScope.toast('Calificar con mas de 0 estrellas.', 'short');
      }else{
        var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">¿Calificar ruta con '+ $scope.ruta.calificacion_usuario+' estrellas?</p>',
        title: '<h4 class="izquierda"> <img src="img/preferencias/logo.png" class="corazonCalificar" >Calificación</h4>',
        cssClass: 'tituloRosado',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {
                  $scope.ruta.calificacion_usuario = 0;
                  $scope.ruta.calificacion_promedio = "0.00";
                }
          },
          {
            text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo" style="margin-bottom: -30px;"></img><b>ACEPTAR</b><div>&nbsp;</div>',
            type: 'button-positive asd botonNaranja',
            onTap: function(e) {
                $ionicLoading.show({
                  template: 'Cargando...'
                });
                var posOptions = {timeout: 10000, enableHighAccuracy: false};

                $scope.ruta.calificacion_promedio = $scope.ruta.calificacion_usuario + ".00";
                navigator.geolocation.getCurrentPosition(
                  function(position){
                    var lat  = position.coords.latitude;
                    var long = position.coords.longitude;
                    $scope.addCalificacionGeo(lat,long,ruta,$scope.ruta.calificacion_usuario);
                  },function(err) {
                    $scope.addCalificacionGeo(0,0,ruta,$scope.ruta.calificacion_usuario);
                  },posOptions
                );
            }
          }
        ]
        });

        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });
      }

    }

    $scope.addCalificacionGeo = function(lat,long,ruta,calificacion){
      console.log("Calificacion: " + calificacion);
      $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionRutasTipicas.php",
          data: $httpParamSerializer({
            id_usuario   : localStorage.getItem("id_usuario"),
            id_ruta      : $scope.ruta.id_ruta,
            calificacion : calificacion,
            "lat"        : lat,
            "long"       : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ 
          console.log(response.data);
          $ionicLoading.hide();

          if(response.data.resultado == "ok"){
            //console.log(response.data.item_turistico);
            //$scope.marcadores = response.data.item_turistico;
            $rootScope.toast('Ruta calificada', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
    }
    
    $scope.recorrerRuta = function(){
      $rootScope.modalSaberMas.hide();
      $rootScope.modalRutasTipicas.hide();
      $rootScope.rutaRecorrer = $rootScope.rutaSeleccionada;
      $state.go("recorrerRutaTipica");
    }

    /*$scope.openModalCompartir = function(){
      $ionicModal.fromTemplateUrl('templates/modalCompartir.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $scope.tituloCompartir = $scope.ruta.nombre_ruta;
        $scope.link = 
        $rootScope.modalCompartir = modal;
        $rootScope.modalCompartir.show();
      });
    }*/

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };
}]);