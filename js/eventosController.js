angular.module('starter.controllers')


.controller('eventosCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaCamera','$cordovaFileTransfer','$timeout','$ionicLoading','$ionicModal','$sce',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaCamera,$cordovaFileTransfer,$timeout,$ionicLoading,$ionicModal,$sce) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.evento = [];
  //$scope.evento.foto = "https://mcdonalds.com.gt/wp-content/uploads/2013/01/Ronald-McDonald.png";

  /**
   * [Rescatar todos los eventos de la base de datos.]
   * 
   * @return {[Array]}  $scope.eventos [Array con eventos]
   */
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getEventos.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    $scope.eventos = response.data.eventos;
    console.log($scope.eventos);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });


  $scope.mostrarMarcadorEvento = function(evento){
    $rootScope.mostrarEventoSeleccionado(evento.georeferenciacion_evento_latitud,evento.georeferenciacion_evento_longitud,evento.titulo_evento,evento);
    $scope.cerrarModalEventos();
  }

  $scope.cerrarModalEventos = function(){
    console.log("cerrar modal");    
    $rootScope.modalEventos.remove();
  }

  //para transformar el texto a html, ya que no pasaba la etiqueta style=".."
  $scope.trustAsHtml = function(string) {
    return $sce.trustAsHtml(string);
  };

  $scope.hrefWeb = function(link){
    window.open(link,'_blank','location=yes');
  }

  $scope.mailToCorreo = function(link){
    window.open('mailto:${link}','_system');
  }

  $scope.openModalEventoSeleccionado = function(evento){
    $rootScope.evento_seleccionado = evento;
    $rootScope.desdeEventosController = true;
    $ionicModal.fromTemplateUrl('templates/eventoSeleccionado.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalEventoSeleccionado = modal;
      $rootScope.modalEventoSeleccionado.show();
    });
  }

}]);