angular.module('starter.controllers')


.controller('recorrerRutaTipicaCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$httpParamSerializer','$timeout','$sce','$cordovaCamera','$cordovaFileTransfer','$cordovaSocialSharing','$ionicPopup','$ionicLoading','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$httpParamSerializer,$timeout,$sce,$cordovaCamera,$cordovaFileTransfer,$cordovaSocialSharing,$ionicPopup,$ionicLoading,$ionicNativeTransitions) {
    $scope.ruta = $rootScope.rutaRecorrer;
    console.log($scope.ruta);
    
    $scope.opcionSeleciconada            = false;
    $scope.mostrarBotonesRegistrarhuella = false;
    $scope.entrarUnaVezRecomendado       = 0;
    $scope.verMishuellas                 = false;
    $scope.verTodashuellas               = false;
    $scope.marcadorDraggable             = null;
    $scope.pausa                         = false;
    $scope.titulo                        = "DEJA TU HUELLA";
    var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);
    width = window.screen.height;
    var alturaMapa = width;
    var alturaMapa = width - 110;
    $scope.styleMapa = {
        "height" : alturaMapa+"px",
        "width"  : "100%",
        "overflow-y" : "hidden"
    };

    document.addEventListener("deviceready", function() {
        var div = document.getElementById("map_canvas");
        
        // Initialize the map view
        $rootScope.map = plugin.google.maps.Map.getMap(div,{
            'backgroundColor' : 'white',
            'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
            'controls' : {
                'compass'          : true,
                'myLocationButton' : true,
                'indoorPicker'     : true,
                'zoom'             : false
            },
            'gestures': {
                'scroll' : true,
                'tilt'   : true,
                'rotate' : true,
                'zoom'   : true
            },
            'camera': {
                'latLng' : COPIAPO,
                'tilt'   : 0,
                'zoom'   : 10,
                'bearing': 0
            }
        });

        // Wait until the map is ready status.
        $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);

        document.addEventListener('backbutton', function (event) {
          event.preventDefault();
          event.stopPropagation();
          $rootScope.map.remove();
        }, false);

    }, false);

    function onMapReady() {
            $rootScope.map.setPadding( 0, 0 , 0 , 0);
            $ionicLoading.show({
              template : 'cargando...'
            });
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/getCoordenadasRutaTipica.php",
              data: $httpParamSerializer({
                "id_ruta": $scope.ruta.id_ruta
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si recato bien los datos                    
                //se dibuja la ruta en el homeController, en mostrarRecomendados()
                $rootScope.coorRutaTipica = response.data.ruta;

                var flightPlanCoordinates ="";
                var inicio ="";
                var fin="";
                //dibujar ruta en el mapa....
                $scope.rutaDibujo = [];
                angular.forEach($rootScope.coorRutaTipica, function(value, key) {
                  

                 inicio = new plugin.google.maps.LatLng(value.inicio_lat,value.inicio_lng);
                 fin    = new plugin.google.maps.LatLng(value.fin_lat,value.fin_lng);


                 var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.inicio_lat),
                                        parseFloat(value.inicio_lng) );


              var url = "www/img/huella/pie1.png";
                  flightPlanCoordinates  = [
                    {lat: parseFloat(value.inicio_lat), lng: parseFloat(value.inicio_lng)},
                    {lat: parseFloat(value.fin_lat), lng: parseFloat(value.fin_lng)},
                  ];

                  $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': "",
                                        'draggable': false,
                                        'anchor':  [1, 3],
                                        icon: {
                                          url: url,
                                          size: { width: 10, height: 10 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                              //console.log($rootScope.coorRutaTipica.length + '  ' + key);
                                                if( $rootScope.coorRutaTipica.length == (key+1) ){
                                                  $ionicLoading.hide().then(function(){});
                                                }
                                          });
                                      });

            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
            });












            $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addEstadoRutaTuristica.php",
        data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario"),
            "id_ruta"   : $scope.ruta.id_ruta,
            "estado"    : 1
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        $rootScope.toast('Comenzando ruta...', 'short');
        $scope.entrarUnaVezRecomendado = 0;
        $scope.fechaAnterior = new Date();
        $scope.rutaTipica();
        //$ionicLoading.hide().then(function(){});
    }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
    });





    
    }

    function onBtnClicked() {
        map.showDialog();
    }

    

    $scope.rutaTipica = function(){
      $scope.watchRutaTipica = navigator.geolocation.watchPosition(function(position){
        console.log(position);
        if( $scope.pausa == false ){
          if($scope.entrarUnaVezRecomendado == 0){
            //actualizar el estado de la ruta
            //console.log("AYUDA2");
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
              data: $httpParamSerializer({
                "id_usuario"     : localStorage.getItem("id_usuario"),
                "id_ruta"        : $scope.ruta.id_ruta,
                "punto_longitud" : long,
                "punto_latitud"  : lat
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si recato bien los datos
                console.log(response.data);
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
            });

            $scope.mostrarRecomendados(position.coords.latitude,position.coords.longitude); 
            $scope.entrarUnaVezRecomendado = 1;
          }
            $scope.now = new Date();
            console.log("ANTES IF");
            console.log($scope.now);
            console.log($scope.fechaAnterior);
          if ($scope.now - $scope.fechaAnterior > 30000) {
              console.log("AYUDA2");
              var lat  = position.coords.latitude;
              var long = position.coords.longitude;
              //console.log(lat, long, $scope.now-$scope.fechaAnterior,$scope.now, $scope.fechaAnterior);

              //actualizar el estado de la ruta
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
                data: $httpParamSerializer({
                  "id_usuario"     : localStorage.getItem("id_usuario"),
                  "id_ruta"        : $scope.ruta.id_ruta,
                  "punto_longitud" : long,
                  "punto_latitud"  : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  console.log(response.data);
              }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
              });

              $scope.mostrarRecomendados(lat,long);

              $scope.fechaAnterior = $scope.now;
          }
        }

      },function(error){
        console.log("Error rutatipica: ",error);
        //console.log("llamar de nuevo a ruta tipica");
        //$scope.rutaTipica();
        
        /*$ionicLoading.hide();
        
        $rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();
        
        $rootScope.coorRutaTipica = null;
        //$state.go("app.nuevoHome");
        $rootScope.volverExploraYDescubre = false;
        $rootScope.volverExploraYDescubreRutas = false;
        
        alert('nuvo homeeeee!');
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });*/
      },{
        timeout : 30000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      });
    }

    $scope.mostrarRecomendados = function(lat,long){
            //Obtener patrimonios Recomendacion
              $http({
                method: "POST",
                //url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
                url: "http://200.14.68.107/atacamaGo/getPatrimonioRecomendacionNuevo.php",
                data: $httpParamSerializer({
                  "id_usuario" : localStorage.getItem("id_usuario"),
                  "id_ruta"    : $scope.ruta.id_ruta,
                  "longitud"   : long,
                  "latitud"    : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  console.log(response.data);
                  $scope.patrimoniosRecomendados = response.data.patrimonio;
                  console.log("patrimoinos: ",response.data.patrimonio);

                  //Obtener Servicios Recomendacion
                  $http({
                    method: "POST",
                    //url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
                    url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosRutaDinamicaNuevo.php",
                    data: $httpParamSerializer({
                      "id_usuario" : localStorage.getItem("id_usuario"),
                      "id_ruta"    : $scope.ruta.id_ruta,
                      "longitud"   : long,
                      "latitud"    : lat
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.serviciosRecomendados = response.data.servicios;
                      console.log("Servicios: ",response.data.servicios);

                      $http({
                        method: "POST",
                        //url: "http://200.14.68.107/atacamaGo/getItemsTuristicosRecomendacion2.php",
                        url: "http://200.14.68.107/atacamaGo/getItemTuristicoDinamico.php",
                        data: $httpParamSerializer({
                          "id_usuario" : localStorage.getItem("id_usuario"),
                          "longitud"   : long,
                          "latitud"    : lat
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                      }).then(function(response){ //ok si recato bien los datos
                          $scope.itemsRecomendados = response.data.item;
                          console.log("itemsRecomendados: ",response.data.item);

                          $http({
                            method: "POST",
                            url: "http://200.14.68.107/atacamaGo/getEventosDinamicos.php",
                            data: $httpParamSerializer({
                              "id_usuario" : localStorage.getItem("id_usuario"),
                              "longitud"   : long,
                              "latitud"    : lat
                            }),
                            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                          }).then(function(response){ 
                              if(response.data.resultado == "ok" || response.data.resultado == "no data"){
                                $scope.eventosDinamicosRuta = response.data.eventos;
                                console.log("eventosDinamicos:",response.data.eventos);

                                //llenar mapa con patrimoinos y servicios recmendados...
                                //$rootScope.map.clear();
                                //$rootScope.map.off();

                                //dibujar ruta en el mapa....
                                $scope.rutaDibujo = [];
                                angular.forEach($rootScope.coorRutaTipica, function(value, key) {
                                    var inicio = new plugin.google.maps.LatLng(value.inicio_lat,value.inicio_lng);
                                    var fin    = new plugin.google.maps.LatLng(value.fin_lat,value.fin_lng);
                                    $scope.rutaDibujo.push(inicio);
                                    $scope.rutaDibujo.push(fin);            
                                });
                                  
                                var idx = 0;
                                /*$rootScope.map.addPolyline({
                                   'points': $scope.rutaDibujo,
                                   'color' : "red",
                                   'width': 3,
                                   'geodesic': true
                                }, function(polyline) {
                                   polyline.on(plugin.google.maps.event.OVERLAY_CLICK, function() {
                                   });
                                });
*/
                                //ELiminar marcadores de Servicios markerArrayServiciosTuristicosRecomendados
                                angular.forEach($scope.markerArrayServiciosTuristicosRecomendados, function(value, key) { 
                                  $scope.markerArrayServiciosTuristicosRecomendados[key].remove();
                                });
                                $scope.markerArrayServiciosTuristicosRecomendados = [];
                                
                                angular.forEach($scope.serviciosRecomendados, function(value, key) {  
                                    
                                      var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.direccion_georeferenciada_latitud),
                                        parseFloat(value.direccion_georeferenciada_longitud) );

                                      //var url = "www/img/servicios/"+value.icono+".png";
                                      var url = "www/img/servicios/"+value.icono+"Oculto.png";
                                      $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': value.nombre_item_turistico,
                                        'draggable': false,
                                        'anchor':  [30, 45],
                                        icon: {
                                          url: url,
                                          size: { width: 30, height: 45 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                          //accion al hacer clic en el marcador
                                          $scope.markerArrayServiciosTuristicosRecomendados[key] = marker;
                                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                            $rootScope.tipo_seleccionado = "servicio";
                                            $rootScope.item_seleccionado = value;
                                            $scope.openModalMenuAccionesModificar();
                                          });
                                      });
                                      
                                });

                                //ELiminar marcadores de Servicios markerArrayPatrimonioRecomendados
                                angular.forEach($scope.markerArrayPatrimonioRecomendados, function(value, key) { 
                                  $scope.markerArrayPatrimonioRecomendados[key].remove();
                                });
                                $scope.markerArrayPatrimonioRecomendados = [];

                                angular.forEach($scope.patrimoniosRecomendados, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayPatrimonioRecomendados[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "patrimonio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                //ELiminar marcadores de Servicios markerArrayItemRecomendados
                                angular.forEach($scope.markerArrayItemRecomendados, function(value, key) { 
                                  $scope.markerArrayItemRecomendados[key].remove();
                                });
                                $scope.markerArrayItemRecomendados = [];

                                angular.forEach($scope.itemsRecomendados, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/posicionGpsTuristaOculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayItemRecomendados[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "item";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                //llenar mapa con eventos dinamicos...
                                //ELiminar marcadores de Servicios markerArrayEventosDinamicosRuta
                                angular.forEach($scope.markerArrayEventosDinamicosRuta, function(value, key) { 
                                  $scope.markerArrayEventosDinamicosRuta[key].remove();
                                });
                                $scope.markerArrayEventosDinamicosRuta = [];
                                          
                                angular.forEach($scope.eventosDinamicosRuta, function(value, key) {
                                   
                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.georeferenciacion_evento_latitud),
                                    parseFloat(value.georeferenciacion_evento_longitud) );

                                    var url = "www/img/gps.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.titulo_evento,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                        //accion al hacer clic en el marcador
                                        $scope.markerArrayEventosDinamicosRuta[key] = marker;
                                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                          $rootScope.tipo_seleccionado = "evento";
                                          $rootScope.evento_seleccionado = value;
                                          $scope.openModalEvento();
                                        });
                                    });

                                  });

                            
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                        });
                      }, function(){ //Error de conexión
                        $rootScope.toast('Verifica tu conexión a internet', 'short');
                      });

                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });

                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });           
    };


    $scope.salirRutaTipica = function(opcion){

      $rootScope.map.setClickable(false);
      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">¿Desea pausar o parar la ruta en curso?</p>',
        title: 'Aviso',
        scope: $scope,
        //cssClass: 'tituloRosado',
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {
                  $rootScope.map.setClickable(true);
                }
          },
          {
            text: '<b>Pausar</b>',
            type: 'button-positive asd-pausa',
            onTap: function(e) {
              $scope.pauseRutaTipica();
              $rootScope.map.setClickable(false);
              if (opcion == "cerrar") {
                $scope.cerrarRecorrerRutaTipicas();
              }else if (opcion == "atras"){
                $scope.atrasRecorrerRutaTipicas();
              }
            }
          },
          {
            text: '<b>Parar</b>',
            type: 'button-positive asd-parar',
            onTap: function(e) {
              $scope.stopRutaTipica();
              $rootScope.map.setClickable(false);
              if (opcion == "cerrar") {
                $scope.cerrarRecorrerRutaTipicas();
              }else if (opcion == "atras"){
                $scope.atrasRecorrerRutaTipicas();
              }
            }
          }
        ]
      });
    }


    $scope.cerrarRecorrerRutaTipicas = function(){

        navigator.geolocation.clearWatch($scope.watchRutaTipica);
        $scope.watchRutaTipica = null;

        $rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();
        // $scope.stopRutaTipica();
        
        $rootScope.coorRutaTipica = null;
        //$state.go("app.nuevoHome");
        $rootScope.volverExploraYDescubre = false;
        $rootScope.volverExploraYDescubreRutas = true;

        
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
        $rootScope.modalRutasTipicas.show();
    }

    //atras del mapa
    $scope.atrasRecorrerRutaTipicas = function(){
        //Modal menu Acciones Modiciar...

        //$rootScope.atrasRecorrerRutaTipicas = true;
        
        // $scope.stopRutaTipica();

        $rootScope.map.clear();
        //$rootScope.map.off();
        $rootScope.map.remove();
        $rootScope.coorRutaTipica = null;

        $rootScope.atrasRecorrerRutaTipicas = true;
        
        navigator.geolocation.clearWatch($scope.watchRutaTipica);
        $scope.watchRutaTipica = null;

        //$rootScope.map.setClickable(false);

        /*$ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 0, // in milliseconds (ms), default 400
        });*/

        //$rootScope.volverExploraYDescubre = false;
        //$rootScope.volverExploraYDescubreRutas = false;

        //$rootScope.modalRutasTipicas.show();
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
        $rootScope.modalRutasTipicas.show();
        $rootScope.modalSaberMas.show();

        //$rootScope.map.setVisible();

        
        
    }


    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
        $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
            scope: $rootScope,
            animation: 'slide-in-up',
            backdropClickToClose: false,
        }).then(function(modal) {
            $rootScope.modalMenuAccionesModificar = modal;

            //hacer que el mapa no sea clickeable.
            //if( ionic.Platform.isAndroid() )
            $rootScope.map.setClickable(false);

            $rootScope.modalMenuAccionesModificar.show();
      
        });
    }
    
   
   $scope.test = function(id){
    alert("id: "+id);
   }

  $scope.addHuella = function(){
    $scope.mostrarBotonesRegistrarhuella = true;

    //var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);
    //
    $rootScope.map.setClickable(false);
        var myPopup = $ionicPopup.show({
          template: '<p CLASS="fuenteRoboto fuenteModal">Presionar prolongadamente para mover marcador.</p>',
          title: '<h4 class="izquierda"> ! AVISO</h4>',
          cssClass: 'headRosado',
          scope: $scope,
          buttons: [
            { 
                text: '<i class="icon ion-close-round"></i>',
                type:'popclose',
                  onTap: function(e) {
                    $rootScope.map.setClickable(true);
                  }
            },
            {
              text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo"></img><b style="color: white !important;">ACEPTAR</b><div>&nbsp;</div>',
              type: 'botonMorado',
              onTap: function(e) {
                $rootScope.map.setClickable(true);
              }
            }
          ]
        });
        if ($scope.marcadorDraggable != null ) {
          $scope.marcadorDraggable.remove();
        }else{
          console.log("no existe marcador previo");
        }
    // $rootScope.map.getCameraPosition(function(camera) {
        // Get the current camera position.
        var camera = $rootScope.map.getCameraPosition();
        var lat = camera.target.lat;
        var lng = camera.target.lng;
      
        var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);

        $rootScope.map.addMarker({
            'position': POS_MARCADOR,
            'draggable': true,
            'anchor':  [30, 35],
            icon: {
                url: "www/img/deja_tu_huella/deja_huella_azul.png",
                size: { width: 40, height: 35 },
            },        
        }, function(marker) {
            $scope.marcadorDraggable = marker;
            marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_END, function(marker) {
              // marker.getPosition(function(latLng) {
              //     //marker.setTitle(latLng.toUrlValue());
              //     //marker.showInfoWindow();
              //     //var coor = split(",",)
              //     console.log(latLng);
              //     $rootScope.lat = latLng.lat;
              //     $rootScope.lon = latLng.lng;
              //   });
                // $rootScope.lat = latLng.lat;
                $rootScope.lat = marker.lat;
                // $rootScope.lon = latLng.lng;
                $rootScope.lon = marker.lng;
                console.log("$rootScope.lat: " + $rootScope.lat);
                console.log("$rootScope.lon: " + $rootScope.lon);
            }); 
        });
        $rootScope.lat = $rootScope.map.getCameraPosition().target.lat;
        $rootScope.lon = $rootScope.map.getCameraPosition().target.lng;
        console.log("$rootScope.lat: " + $rootScope.lat);
        console.log("$rootScope.lon: " + $rootScope.lon);
    // });

    $scope.addHuellaConfirmar = function(){
        // $rootScope.map.clear();
        // $rootScope.map.off();
        // $scope.mostrarBotonesRegistrarhuella = false;
        $scope.marcadorDraggable.remove();
        $scope.agregarhuellaPhp($rootScope.lat,$rootScope.lon);
    }

    $scope.descartarHuella = function(){
        $scope.marcadorDraggable.remove();
        // $rootScope.map.clear();
        // $rootScope.map.off();
        $scope.mostrarBotonesRegistrarhuella = false;
    }

    $scope.agregarhuellaPhp = function(lat, lon){          
          $rootScope.POS_MARCADOR = new plugin.google.maps.LatLng(lat, lon);
          
          //obtenemos la direccion del lugar....
          var request = {
            'position': $rootScope.POS_MARCADOR
          };

          plugin.google.maps.Geocoder.geocode(request, function(results) {
            // if (results.length) {
              var result = results[0];
              var position = result.position; 
              var address = {
                numero: result.subThoroughfare || "",
                calle: result.thoroughfare || "",
                ciudad: result.locality || "",
                region: result.adminArea || "",
                postal: result.postalCode || "",
                pais:   result.country || ""
              };

              
              //GUARDAR LA DIRECCION ...
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addItemTuristico.php",
                data: $httpParamSerializer({
                  "id_usuario"                         : localStorage.getItem("id_usuario"),
                  "direccion_georeferenciada_longitud" : lon,
                  "direccion_georeferenciada_latitud"  : lat,
                  "direccion_item"                     : address.callle,
                  "numero_direccion_item"              : address.numero,
                  "ciudad"                             : address.ciudad,
                  "id_ruta_recorrida_turista"          : $rootScope.id_ruta_recorrida_turista
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                console.log(response);
                if(response.data.resultado == "ok"){
                  console.log("resultadoAddHuella",response.data.resultado);
                  //guardar el id_item_turistico para guardar la descripcion titulos videos etc.
                  $rootScope.id_item_turistico = response.data.id_item_turistico;
                  var id_item_turistico_creado_usuario = response.data.id_item_turistico;

                  $ionicModal.fromTemplateUrl('templates/menuAcciones.html', {
                      scope: $rootScope,
                      animation: 'slide-in-up',
                      backdropClickToClose: false,
                  }).then(function(modal) {
                      $rootScope.modalMenuAcciones = modal;
                      $rootScope.map.setClickable(false);
                      $rootScope.modalMenuAcciones.show();
                  });

                  /*creamos el marcador...*/
                  var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lon);
                  var url = 'www/img/deja_tu_huella/mis_huellas.png';
                  $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    'draggable': false,
                    'anchor':  [30, 35],
                    icon: {
                      url: url,
                      size: { width: 30, height: 35 },
                    }
                    
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        var marcadorNuevo = {};
                        $http({
                          method: "POST",
                          url: "http://200.14.68.107/atacamaGo/getItemTuristicoNuevo.php",
                          data: $httpParamSerializer({
                            "id_item_turistico": id_item_turistico_creado_usuario,
                            "id_usuario": localStorage.getItem("id_usuario")
                        }),
                          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                        }).then(function(response){ //ok si guardó correctamente.
                          //console.log(response);
                          if(response.data.resultado == "ok"){
                            
                            marcadorNuevo = {
                              "descripcion"                        : response.data.item_turistico.descripcion,
                              "nombre"                             : response.data.item_turistico.nombre,
                              "direccion_georeferenciada_latitud"  : "",
                              "direccion_georeferenciada_longitud" : "",
                              "foto"                               : response.data.item_turistico.foto,
                              "id"                                 : id_item_turistico_creado_usuario,
                              "id_usuario"                         : response.data.item_turistico.id_usuario
                            };

                            $rootScope.tipo_seleccionado = "item";
                            $rootScope.item_seleccionado = marcadorNuevo;
                            $ionicLoading.hide();
                            $scope.openModalMenuAccionesModificar();
                          }else if ( response.data.resultado == "no data" ){
                            //$rootScope.toast('lugar aún no tiene audios', 'short');
                            $ionicLoading.hide();
                            $scope.mostrarBotonesRegistrarhuella = false;
                          }else{
                            $rootScope.toast('error, intenta nuevamente', 'short');
                            $ionicLoading.hide();
                            $scope.mostrarBotonesRegistrarhuella = false;
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                          $ionicLoading.hide();
                          $scope.mostrarBotonesRegistrarhuella = false;
                        });

                      });   
                  });
                  /*fin crear marcador...*/
                  $rootScope.toast('Item agregado', 'short');
                  $ionicLoading.hide();
                  $scope.mostrarBotonesRegistrarhuella = false;
                }
                else{
                  //$scope.cerrarModalFiguraGps();
                  $rootScope.toast('Sólo se pueden crear ítems en Atacama', 'short');
                  $ionicLoading.hide();
                  $scope.mostrarBotonesRegistrarhuella = false;
                }

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short');
                  $ionicLoading.hide();
                  $scope.mostrarBotonesRegistrarhuella = false;
              });
              //GUARDAR LA DIRECCION ...  
        //     } else {
        //       $rootScope.toast('No se encuentra el terreno', 'short');
        //       $ionicLoading.hide();
        // $scope.mostrarBotonesRegistrarhuella = false;
        //     }
          });
    }
  }

  $scope.startRutaTipica = function(){
    $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addEstadoRutaTuristica.php",
        data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario"),
            "id_ruta"   : $scope.ruta.id_ruta,
            "estado"    : 1
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        $rootScope.toast('Comenzando ruta...', 'short');
        $scope.entrarUnaVezRecomendado = 0;
        $scope.fechaAnterior = new Date();
        $scope.rutaTipica();
    }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
    });
  }

  $scope.stopRutaTipica = function(){
    if( $scope.watchRutaTipica == null ){
      //esta finalizada la ruta, por lo tanto iniciarla
      $scope.startRutaTipica();
    }else{
      $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addEstadoRutaTuristica.php",
          data: $httpParamSerializer({
              "id_usuario": localStorage.getItem("id_usuario"),
              "id_ruta"   : $scope.ruta.id_ruta,
              "estado"    : 2
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si recato bien los datos
          /*$rootScope.toast('Comenzando ruta...', 'short');
          $scope.entrarUnaVezRecomendado = 0;
          $scope.fechaAnterior = new Date();
          $scope.rutaTipica();*/

          console.log($scope.watchRutaTipica);
          navigator.geolocation.clearWatch($scope.watchRutaTipica);
          $scope.watchRutaTipica = null;

          $rootScope.toast('Ruta típica finalizada', 'short');
          $rootScope.btnRecorrerRuta    = false;
          $rootScope.btnRecorriendoRuta = false;

          $scope.atrasRecorrerRutaTipicas();
      }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
    }
  }

  $scope.pauseRutaTipica = function(){
    if( $scope.pausa == false ){ //si aprieta pausar.....
      $rootScope.toast('Ruta en pausa', 'short');
      $scope.pausa = true;
    }else{
      $scope.pausa = false;
      $rootScope.toast('Continuando ruta', 'short');
    }
  }

  $scope.openModalEvento = function(){
    $ionicModal.fromTemplateUrl('templates/eventoSeleccionado.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
    }).then(function(modal) {
        $rootScope.fromMapa = true;
        $rootScope.modalEventoSeleccionado = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);

        $rootScope.modalEventoSeleccionado.show();
    
    });
  }

}]);