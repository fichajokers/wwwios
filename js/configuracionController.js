angular.module('starter.controllers')


.controller('configuracionCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called

  $scope.distanciaSeleccionada = "Distancia";
  $scope.selectables           = ['50 metros','100 metros','500 metros','1 kilómetro'];
  $scope.material              = [];
  
  $scope.cerrarModalConfiguracion = function(){
    console.log("cerar modal");
    //if( ionic.Platform.isAndroid() )
      $rootScope.map.setClickable(true);
    
    $rootScope.modalConfiguracion.hide()
  }
  //rescatar la configuracion del usuario
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getConfiguracionUsuario.php",
    data: $httpParamSerializer({
      'id_usuario' : localStorage.getItem("id_usuario")
    }),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    //console.log(response.data);
    if(response.data.resultado == "ok"){
    
      switch(response.data.radio) {
        case '10.000':
            $scope.distanciaSeleccionada = '10 metros';
            break;
        case '20.000':
            $scope.distanciaSeleccionada = '20 metros';
            break;
        case '50.000':
            $scope.distanciaSeleccionada = '50 metros';
            break;
        case '100.000':
            $scope.distanciaSeleccionada = '100 metros';
            break;
        case '500.000':
            $scope.distanciaSeleccionada = '500 metros';
            break;
        case '999.000':
            $scope.distanciaSeleccionada = '1 kilómetro';
            break;
      }

      //seleccionar los checkbox
      $scope.material.audio        = (localStorage.getItem("audio") === "true");
      $scope.material.video        = (localStorage.getItem("video") === "true");
      $scope.material.imagen       = (localStorage.getItem("imagen") === "true");
    }
      
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet',"short");
  });
 
  $scope.selectDistancia = function(nuevo,viejo){
    $scope.distanciaSeleccionada = nuevo;
  }

  $scope.guardarConfiguracion = function(datos){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.guardarConfiguracionGeo(lat,long,datos);
      },function(err) {
        $scope.guardarConfiguracionGeo(0,0,datos);
      },posOptions
    );
  }

  $scope.guardarConfiguracionGeo = function(lat,long,datos){
    //se guarda el material audiovisual que desea ver el usuario.
    localStorage.setItem("video", datos.video);
    localStorage.setItem("audio", datos.audio);
    localStorage.setItem("imagen", datos.imagen);  

    var radio = 0;
    switch($scope.distanciaSeleccionada) {
    case '10 metros':
        radio = 10;
        break;
    case '20 metros':
        radio = 20;
        break;
    case '50 metros':
        radio = 50;
        break;
    case '100 metros':
        radio = 100;
        break;
    case '500 metros':
        radio = 500;
        break;
    case '1 kilómetro':
        radio = 999;
        break;
    }

    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/guardarConfiguracionUsuario.php",
      data: $httpParamSerializer({
        'radio'      : radio,
        'id_usuario' : localStorage.getItem("id_usuario"),
        'lat'        : lat,
        'long'       : long
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si guardó correctamente.
      if(response.data.resultado == "ok"){
        $rootScope.radio = radio;
        $rootScope.toast("Configuración actualizada","short");
      }
      else
        $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
      
      $ionicLoading.hide();
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet',"short");
    });
  }

}]);