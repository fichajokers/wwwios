angular.module('starter.controllers')


.controller('modificarDescripcionCtrl', ['$scope','$http','$state','$rootScope','$cordovaInAppBrowser','$ionicModal','$sce','$httpParamSerializer','$window','$ionicLoading',function($scope,$http,$state,$rootScope,$cordovaInAppBrowser,$ionicModal,$sce,$httpParamSerializer,$window,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.tieneImagen         = true;
  $scope.tieneImagenFoto     = true;
  $scope.tieneWeb            = false;
  $scope.noEdita             = true;
  $scope.anchoPantalla       = $window.innerHeight;
  $scope.pantallaGrande      = false;
  $scope.pantallaChica       = true;
  $scope.marginLeft          = 0;
  $scope.mostrarCalificacion = false;

  if( $scope.anchoPantalla >= 768){
    $scope.pantallaGrande  = true;
    $scope.pantallaChica   = false;
  }


  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#AF115E"
    }
  }
  
  $scope.cerrarModalModificarDescripcion = function(){   
    $rootScope.modalModificarDescripcion.remove();
  }

   //asignar a $scope.item el item seleccionado...
   $scope.item = $rootScope.item_seleccionado;

  angular.forEach($scope.item.correos, function(value, key) {
    if( value.tipo_contacto == 'web' )
      $scope.tieneWeb = true;
  });


  $scope.style_Sello_S = {
    "width"       : "30px",
    "margin-left" : $scope.marginLeft + "px"
  }

   	if( $rootScope.tipo_seleccionado == "item" ){
      console.log($scope.item.id_usuario, localStorage.getItem("id_usuario"))
      if($scope.item.id_usuario == localStorage.getItem("id_usuario")){
        $scope.noEdita = false;
      }

      $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
      $scope.foto = $scope.url+$scope.item.foto;

      if( $scope.item.foto == "logoFic.png" || $scope.item.foto == "" || $scope.item.foto == null ){
        $scope.tieneImagenFoto = false;
      }
    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
   		
      if( $scope.item.calificacion_sello_q.length > 1 && $scope.item.sello_q == true){
        switch ($scope.item.calificacion_sello_q.length) {
          case 2:
            $scope.marginLeft = -46;
            break;
          case 3:
            $scope.marginLeft = 0;
            break;
          case 4:
            $scope.marginLeft = 46;
            break;
          case 5:
            $scope.marginLeft = 91;
            break;
          case 6:
            $scope.marginLeft = 136;
            break;
          default:
            break;
        }
      }else{
        $scope.item.calificacion_sello_q = false;
      }
      
      console.log( $scope.item.calificacion_sello_q.length );
      console.log("calificacion_sello_q: " + $scope.item.calificacion_sello_q);

     	$scope.url="http://200.14.68.107/atacamaGoMultimedia";
      	$scope.logo = $scope.url+$scope.item.logo;
        $scope.foto = $scope.url+$scope.item.foto;
      	if( $scope.item.logo == "logoFic.png" || $scope.item.logo == "" || $scope.item.logo == null ){
        	$scope.tieneImagen = false;
      	}

        if( $scope.item.foto == "logoFic.png" || $scope.item.foto == "" || $scope.item.foto == null ){
          $scope.tieneImagenFoto = false;
        }
   	}

   	if( $rootScope.tipo_seleccionado == "patrimonio" ){
   		console.log( $scope.item );
   		$scope.url="http://200.14.68.107/atacamaGoMultimedia";
   		$scope.logo = $scope.url+$scope.item.logo;
      $scope.foto = $scope.url+$scope.item.foto;
   		if( $scope.item.logo == "logoFic.png" || $scope.item.logo == "" || $scope.item.logo == null ){
        	$scope.tieneImagen = false;
      	}

      if( $scope.item.foto == "logoFic.png" || $scope.item.foto == "" || $scope.item.foto == null ){
        $scope.tieneImagenFoto = false;
      }
   	}

    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };

    $scope.slideChanged2 = function(index) {
      $scope.slideIndex = index;
    };

    $scope.slideChanged3 = function(index) {
      $scope.slideIndex = index;
    };

    var optionsLink = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'yes'
    };
    
    $scope.abrirNavegador = function(link) {
      $cordovaInAppBrowser.open(link, '_blank', optionsLink)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
    }

    $scope.reservarNavegador = function(id_servicio){
      $cordovaInAppBrowser.open("http://www.sadepi.cl/atacama/public/servicio/reserva/"+id_servicio, '_blank', optionsLink)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      }); 
    }

    $scope.openModalReserva = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/reserva.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReserva = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
        //  $rootScope.map.setClickable(false);

        $rootScope.modalReserva.show();
      
      });
    }

    //para transformar el texto a html, ya que no pasaba la etiqueta style=".."
    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.editarComentario = function(item){
      $ionicLoading.show({
      template: 'Cargando...'
    });     

      if( (item.nombre || item.nombre != "") && (item.descripcion || item.descripcion != "")){
        console.log(item);

            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/editarDescripcionItemTuristico.php",
              data: $httpParamSerializer({
                "id_item_turistico" : $scope.item.id,
                "id_usuario"        : localStorage.getItem("id_usuario"),
                "comentario"        : item.descripcion,
                "nombre"            : item.nombre
            }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
              console.log(response);
              if(response.data.resultado == "ok"){
                $rootScope.toast('Descripción actualizada', 'short');
              }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
              }
              $ionicLoading.hide();
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
              $ionicLoading.hide();
            });


      }else{
        console.log("pa que po");

      }
    }

    $scope.mailtoCorreo = function(link) {
      window.open(`mailto:${link}`, '_system');
    }

    $scope.hrefweb = function(link){
      window.open(link,'_system');
    }
}]);