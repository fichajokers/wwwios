angular.module('starter.controllers')


.controller('crearMisRecorridosCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$cordovaFileTransfer','$cordovaCamera','$httpParamSerializer','$ionicLoading','$ionicPopup','$ionicModal','$ionicNativeTransitions','$cordovaSocialSharing',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$cordovaFileTransfer,$cordovaCamera,$httpParamSerializer,$ionicLoading,$ionicPopup,$ionicModal,$ionicNativeTransitions,$cordovaSocialSharing) {
  // With the new view caching in Ionic, Controllers are only called

  console.log("crearMisRecorridosCtrl");
  $scope.modalCompartir = false;
  $scope.item = [];
  $scope.item.descripcion = "";
  $scope.item.titulo      = "";
  

  $scope.cerrarModalCrearMisRecorridos = function(){
    $rootScope.imgRecorrido = "";
    if( $rootScope.btnCerrarMiRecorrido== true ){
      $rootScope.btnCerrarMiRecorrido = false;
      $rootScope.map.clear();  //Lo comente por las transiciones no se si afecta
      $rootScope.map.off();    //Lo comente por las transiciones no se si afecta
      $rootScope.map.remove(); //Lo comente por las transiciones no se si afecta
      
      $rootScope.modalCrearMisRecorridos.remove();
      
      $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 0, // in milliseconds (ms), default 400
      });
    }

    $rootScope.map.setClickable(true);
    $rootScope.modalCrearMisRecorridos.remove();
  }

  $scope.atrasModalCrearMisRecorridos = function(){
    $rootScope.imgRecorrido = "";
    $rootScope.map.setClickable(true);
    $rootScope.modalCrearMisRecorridos.remove();
  }

  $scope.compartirFacebook = function(){
      $ionicLoading.show({
          template: 'Cargando...'
      });

      console.log($scope.rutaImgPng);
      console.log("compartirFacebook");
      $cordovaSocialSharing.shareViaFacebook(null, $scope.rutaImgPng, null)
      .then(function(result) {
          $ionicLoading.hide();
          console.log("ok",result);
      }, function(err) {
          $ionicLoading.hide();
          /*console.log("error",err);
          $ionicPopup.alert({
            title: 'Aviso!',
            template: 'Debes tener la aplicaci√≥n de Facebook para compartir la imagen',
            okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
            okType: 'btn-morado', // String (default: 'button-positive'). The type of the OK button.
          });*/
      });
  }

  $scope.compartirTwitter  = function(){
      $ionicLoading.show({
          template: 'Cargando...'
      });
      console.log("compartirTwitter");
      var link = encodeURI( "http://200.14.68.107/atacamaGo/compartirTwitter.php?title="+$scope.item.titulo+"&image="+$scope.rutaImgPng );
      console.log( link );
      console.log( $rootScope.imgRecorrido );
      $cordovaSocialSharing.shareViaTwitter(null,$scope.rutaImgPng, null)
      .then(function(result) {
          $ionicLoading.hide();
          console.log("ok",result);
      }, function(err) {
          $ionicLoading.hide();
          /*console.log("error",err);
          $ionicPopup.alert({
            title: 'Aviso!',
            template: 'Debes tener la aplicaci√≥n de twitter para compartir la imagen',
            okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
            okType: 'btn-morado', // String (default: 'button-positive'). The type of the OK button.
          });*/
      });

      /*$cordovaSocialSharing.canShareVia("twitter", "This is your subject", "www/imagefile.png", "https://www.thepolyglotdeveloper.com").then(function(result) {
          $cordovaSocialSharing.shareViaTwitter("This is your message", "This is your subject", "www/imagefile.png", "https://www.thepolyglotdeveloper.com");
      }, function(error) {
          alert("Cannot share on Twitter");
      });*/
  }

  $scope.addCrearHuella = function(item){



    if( item.titulo == "" || item.descripcion == ""){
      $rootScope.toast('Título y descripcion obligatrio.', 'short');
    } else if( item.descripcion.length  >= 200){
      $rootScope.toast('Descripción debe ser menor a 200 caracteres.', 'short');
    }else{
      $ionicLoading.show({
        template: 'Cargando...'
      });

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addIniciarMiRecorrido.php",
        data: $httpParamSerializer({
          "titulo"                    : item.titulo,
          "descripcion"               : item.descripcion,
          "id_ruta_recorrida_turista" : $rootScope.id_ruta_recorrida_turista,
          "estado"                    : $rootScope.estadoRutaUsuario,
          "img"                       : $rootScope.imgRecorrido,
          "visible"                   : true
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si recato bien los datos
          console.log(response.data);
          $ionicLoading.hide();
          $scope.rutaImgPng = response.data.ruta_img_png;
          $rootScope.toast('Recorrido creado correctamente.', 'short');
          
          $scope.modalCompartir = true;

          //$scope.cerrarModalCrearMisRecorridos();

      }, function(){ //Error de conexi√≥n
        $rootScope.toast('Verifica tu conexi√≥n a internet', 'short');
      });
    }

    

  }
  
  $scope.descartarHuella = function(item){
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/addIniciarMiRecorrido.php",
      data: $httpParamSerializer({
        "titulo"                    : null,
        "descripcion"               : null,
        "id_ruta_recorrida_turista" : $rootScope.id_ruta_recorrida_turista,
        "estado"                    : $rootScope.estadoRutaUsuario,
        "visible"                   : false
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        console.log(response.data);
        $rootScope.toast('Recorrido creado correctamente.', 'short');
        $scope.cerrarModalCrearMisRecorridos();

    }, function(){ //Error de conexi√≥n
      $rootScope.toast('Verifica tu conexi√≥n a internet', 'short');
    });
  }

}]);