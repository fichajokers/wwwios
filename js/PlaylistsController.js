angular.module('starter.controllers')


.controller('PlaylistsCtrl', ['$scope','$http',function($scope,$http) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
}]);