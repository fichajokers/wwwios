angular.module('starter.controllers')

.controller('modificarImagenCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCamera','$httpParamSerializer','$ionicModal','$cordovaFileTransfer','$cordovaSocialSharing','$ionicLoading','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCamera,$httpParamSerializer,$ionicModal,$cordovaFileTransfer,$cordovaSocialSharing,$ionicLoading,$ionicPopup) {
    console.log("modificarImagenCtrl");

    $rootScope.eliminarImagenVisor = false;

    $scope.esAndroid = false;
    if( ionic.Platform.isAndroid() ){
        $scope.esAndroid = true;
    }else{
        $scope.esAndroid = false;
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
      $scope.estiloBarraModificar = {
        "background-color" : "#825012"
      };
      $scope.imgCamara = "img/menuAcciones/invGaleriasAtractivo.png";
      $scope.imgDesdeArchivo = "img/desdeArchivoPatrimonio.png";
      $scope.imgTomarFoto    = "img/tomarFotoPatrimonio.png";
      $scope.colorTexto      = {
        "color" : "#825012"
      };
    }
    if( $rootScope.tipo_seleccionado == "servicio" ){
      $scope.estiloBarraModificar = {
        "background-color" : "#775DA5"
      };
      $scope.imgCamara = "img/menuAcciones/imagenes.png";
      $scope.imgDesdeArchivo = "img/desdeArchivoAtractivo.png";
      $scope.imgTomarFoto    = "img/tomarFotoAtractivo.png";
      $scope.colorTexto      = {
        "color" : "#775DA5"
      };
    }
    if( $rootScope.tipo_seleccionado == "item" ){
      $scope.estiloBarraModificar = {
        "background-color" : "#3B3C97"
      };
      $scope.imgCamara = "img/deja_tu_huella/invGaleriaServicio.png";
      $scope.imgDesdeArchivo = "img/deja_tu_huella/desde_archivo.png";
      $scope.imgTomarFoto    = "img/deja_tu_huella/tomar_foto.png";
      $scope.colorTexto      = {
        "color" : "#3B3C97"
      };
    }

    // With the new view caching in Ionic, Controllers are only called
    $scope.imagenes          = [];
    $scope.hola              = [];
    $scope.holaTotal         = [];
    $scope.tieneSrc          = false;
    $scope.cantImagenes      = 3;
    $scope.noEdita           = false;
    $scope.id_usuario        = localStorage.getItem("id_usuario");
    $scope.nombre            = [];
    $scope.nombre.comentario = [];
    $scope.item = $rootScope.item_seleccionado;

    console.log( $rootScope.item_seleccionado );
    
    if( $rootScope.tipo_seleccionado == "item" ){
        $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
        $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getImagenItemTuristico.php",
            data: $httpParamSerializer({
                "id_item_turistico": $rootScope.item_seleccionado.id,
        }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
            if(response.data.resultado == "ok"){
                console.log(response.data);
                $scope.imagenes      = response.data.item_turistico;
                angular.forEach($scope.imagenes, function(value, key) {
                    $scope.holaTotal[key] = { 
                        "foto"                      : $scope.url + value.foto,
                        "comentario"                : value.comentario,
                        "id_usuario"                : value.id_usuario,
                        "id_archivo_item_turistico" : value.id_archivo_item_turistico,
                        "nombre"                    : value.nombre_archivo_item
                    };
                    
                    if( key < 4){
                        $scope.hola[key] = { 
                            "foto"                      : $scope.url + value.foto,
                            "comentario"                : value.comentario,
                            "id_usuario"                : value.id_usuario,
                            "id_archivo_item_turistico" : value.id_archivo_item_turistico,
                            "nombre"                    : value.nombre_archivo_item
                        };
                    }
                });

                //console.log( response.data.id_usuario, localStorage.getItem("id_usuario") )
                if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
                    //console.log("EDITAR");
                    $scope.noEdita = false;
                }

            }else if ( response.data.resultado == "no data" ){
                $rootScope.toast('No hay imágenes', 'short');
                //console.log( response.data.id_usuario, localStorage.getItem("id_usuario") )
                if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
                    //console.log("EDITAR");
                    $scope.noEdita = false;
                }
                //$scope.cerrarModalModificarImagen();
            }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
            }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
        $scope.noEdita = false;
        $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
        $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getImagenServicio.php",
            data: $httpParamSerializer({
                "id_servicio": $rootScope.item_seleccionado.id,
        }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
            console.log(response);
            if(response.data.resultado == "ok"){
                $scope.imagenes      = response.data.servicio;
                angular.forEach($scope.imagenes, function(value, key) {
                    $scope.holaTotal[key] = { 
                        "foto"                          : $scope.url + value.foto,
                        "comentario"                    : value.comentario,
                        "id_usuario"                    : value.id_usuario,
                        "id_archivo_servicio_turistico" : value.id_archivo_servicio_turistico,
                        "nombre"                        : value.ruta_archivo_servicio
                    };
                    
                    if( key < 4){
                        $scope.hola[key] = { 
                            "foto"                          : $scope.url + value.foto,
                            "comentario"                    : value.comentario,
                            "id_usuario"                    : value.id_usuario,
                            "id_archivo_servicio_turistico" : value.id_archivo_servicio_turistico,
                            "nombre"                        : value.ruta_archivo_servicio
                        };
                    }
                });

            }else if ( response.data.resultado == "no data" ){
                $rootScope.toast('No hay imágenes', 'short');
                //$scope.cerrarModalModificarImagen();
            }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
            }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
        $scope.noEdita = false;
        $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
        $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getImagenPatrimonio.php",
            data: $httpParamSerializer({
                "id_patrimonio": $rootScope.item_seleccionado.id,
        }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
            console.log(response);
            if(response.data.resultado == "ok"){
                $scope.imagenes      = response.data.patrimonio;
                angular.forEach($scope.imagenes, function(value, key) {
                    $scope.holaTotal[key] = { 
                        "foto"                  : $scope.url + value.foto,
                        "comentario"            : value.comentario,
                        "id_usuario"            : value.id_usuario,
                        "id_archivo_patrimonio" : value.id_archivo_patrimonio,
                        "nombre"                : value.ruta_archivo_patrimonio
                    };
                    
                    if( key < 4){
                        $scope.hola[key] = { 
                            "foto"                  : $scope.url + value.foto,
                            "comentario"            : value.comentario,
                            "id_usuario"            : value.id_usuario,
                            "id_archivo_patrimonio" : value.id_archivo_patrimonio,
                            "nombre"                : value.ruta_archivo_patrimonio
                        };
                    }
                });

            }else if ( response.data.resultado == "no data" ){
                $rootScope.toast('No hay imágenes', 'short');
                //$scope.cerrarModalModificarImagen();
            }else{
                $rootScope.toast('error, intenta nuevamente', 'short');
            }
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

    console.log($rootScope.item_seleccionado.id);
    

    $scope.cerrarModalModificarImagen = function(){   
        $rootScope.modalModificarImagen.remove();
    }

    $scope.cerrarModalModificarImagenMapa = function(){   
        console.log("cerrar modificar imagen");
        $rootScope.modalModificarImagen.remove();
        console.log("cerrar menu modificar");
        $rootScope.map.setClickable(true);
        $rootScope.modalMenuAccionesModificar.remove();
    }

    //modal visor de imágenes...
    $ionicModal.fromTemplateUrl('templates/visorImagen.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modalVisorImagen = modal;
    });
      
    $scope.openModalVisorImagen = function(imagen,index) {

        if( imagen.id_usuario == localStorage.getItem("id_usuario") ){
            $rootScope.eliminarImagenVisor = true;
            $rootScope.imgEliminar = imagen;
            $rootScope.indexEliminar = index;
        }

        console.log(imagen);
        $scope.modalVisorImagen.show();
        $scope.imgUrl     = imagen.foto;
        $scope.comentario = imagen.comentario;
    }

    $scope.compartirFacebook = function(){
        $ionicLoading.show({
            template: 'Cargando...'
        });

        console.log("compartirFacebook");
        $cordovaSocialSharing.shareViaFacebook($scope.comentario, $scope.imgUrl, null)
        .then(function(result) {
            $ionicLoading.hide();
            console.log("ok",result);
        }, function(err) {
            $ionicLoading.hide();
            /*console.log("error",err);
            $ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de Facebook para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
            });*/
        });
    }

    $scope.compartirTwitter  = function(){
        $ionicLoading.show({
            template: 'Cargando...'
        });
        console.log("compartirTwitter");
        $cordovaSocialSharing.shareViaTwitter(null, $scope.imgUrl,null)
        .then(function(result) {
            $ionicLoading.hide();
            console.log("ok",result);
        }, function(err) {
            $ionicLoading.hide();
            /*console.log("error",err);
            $ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de twitter para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
            });*/
        });

        /*$cordovaSocialSharing.canShareVia("twitter", "This is your subject", "www/imagefile.png", "https://www.thepolyglotdeveloper.com").then(function(result) {
            $cordovaSocialSharing.shareViaTwitter("This is your message", "This is your subject", "www/imagefile.png", "https://www.thepolyglotdeveloper.com");
        }, function(error) {
            alert("Cannot share on Twitter");
        });*/
    }

    $scope.eliminarImagen = function(imagen,index){
        $ionicLoading.show({
            template: 'Cargando...'
        });

        if( $rootScope.tipo_seleccionado == "item" ){
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteImagenItemTuristico.php",
                data: $httpParamSerializer({
                    "id_archivo_item_turistico" : imagen.id_archivo_item_turistico,
                    "nombre"                    : imagen.nombre
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.eliminarImagenVisor = false;
                    $rootScope.toast('Imagen eliminada correctamente',"short");
                    $rootScope.cerrarModalVisorImagen();
                    $scope.hola.splice(index, 1);
                    
                    angular.forEach($scope.holaTotal, function(value, key) {
                      if(key == index){
                        $scope.holaTotal.splice(index, 1);                        
                      }
                    });
                    
                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });    
        }

        if( $rootScope.tipo_seleccionado == "servicio" ){
            console.log(imagen);
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteImagenServicio.php",
                data: $httpParamSerializer({
                    "id_archivo_servicio_turistico"   : imagen.id_archivo_servicio_turistico,
                    "nombre"                          : imagen.nombre
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.eliminarImagenVisor = false;
                    $rootScope.toast('Imagen eliminada correctamente',"short");
                    $rootScope.cerrarModalVisorImagen();
                    $scope.hola.splice(index, 1);

                    angular.forEach($scope.holaTotal, function(value, key) {
                      if(key == index){
                        $scope.holaTotal.splice(index, 1);                        
                      }
                    });  
                    
                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });
        }
        
        if( $rootScope.tipo_seleccionado == "patrimonio" ){
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteImagenPatrimonio.php",
                data: $httpParamSerializer({
                    "id_archivo_patrimonio"   : imagen.id_archivo_patrimonio,
                    "nombre"                  : imagen.nombre
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.eliminarImagenVisor = false;
                    $rootScope.toast('Imagen eliminada correctamente',"short");
                    $rootScope.cerrarModalVisorImagen();
                    $scope.hola.splice(index, 1);

                    angular.forEach($scope.holaTotal, function(value, key) {
                      if(key == index){
                        $scope.holaTotal.splice(index, 1);                        
                      }
                    });

                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });  
        }
    }

    //----------------Subir imagenes-------------------//
    $scope.takePhoto = function () {
        var options = {
            quality: 100,
            targetWidth: 900,
            targetHeight: 600,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            mediaType       : Camera.MediaType.PICTURE,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
   
        $ionicLoading.show({
            template: 'Cargando...'
        });

        $cordovaCamera.getPicture().then(function (imageData) {
            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            console.log(imageData);
            /*var image           = document.getElementById('imagenSacadaFoto2');
            image.style.display = 'block';
            image.src           = imageData;*/
            $scope.rutaImagen   = imageData;
            $scope.tieneSrc     = true;
            $scope.nombreImagen = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);

            $ionicLoading.hide();
            $scope.addImagenItem();
        }, function (err) {
            console.log(err);
            $ionicLoading.hide();
            // An error occured. Show a message to the user
        });
    }
                
    $scope.choosePhoto = function () {
        var options = {
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType       : Camera.MediaType.PICTURE,
            correctOrientation : true
        };

        $ionicLoading.show({
            template: 'Cargando...'
        });


        $cordovaCamera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            //console.log(imageData);
            

            /*var image2 = document.getElementById('imagenSacadaFoto2');
            image2.style.display   = 'block';
            image2.src             = imageData;*/

            $scope.rutaImagen     = imageData;
            $scope.tieneSrc = true;
            $scope.nombreImagen   = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);

            $ionicLoading.hide();
            $scope.addImagenItem();
        }, function (err) {
            $ionicLoading.hide();
            //alert(err);
            // An error occured. Show a message to the user
        });
    }

    $scope.addImagenItem = function(){
        $ionicLoading.show({
          template: 'Cargando...'
        });

        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        navigator.geolocation.getCurrentPosition(
          function(position){
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            $scope.addImagenItemGeo(lat,long);
          },function(err) {
            $scope.addImagenItemGeo(0,0);
          },posOptions
        );
    }

    $scope.addImagenItemGeo = function(lat,long){
        
        if( $scope.nombreImagen != undefined ){

            var options = {
              fileKey: "fotoItem",
              fileName: $scope.nombreImagen,
              chunkedMode: false
            };

            $scope.noEdita = true;
            if( $rootScope.tipo_seleccionado == "item" ){
                // $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenItem.php", $scope.rutaImagen, options).then(function(result) {
                //   $rootScope.toast('Imagen agregada', 'short');
                //   $ionicLoading.hide();
                // }, function(err) {
                //   $rootScope.toast('Error agregando imagen', 'short');
                //   console.log(err);
                //   $ionicLoading.hide();
                // }, function (progress) {
                //     // constant progress updates
                // });

                //modal que sirve para porner un comentario en la imagen
                $scope.nombre = {};
                // An elaborate, custom popup
                $ionicLoading.hide();
                var myPopup = $ionicPopup.show({
                  template: '<input type="text" ng-model="nombre.comentario" placeholder="Escriba aquí">',
                  title: 'Agrega un comentario',
                  // subTitle: 'Agregar un comentario',
                  scope: $scope,
                  buttons: [
                    { 
                      text: '<i class="icon ion-close-round"></i>',
                      type:'popclose',
                      onTap: function(e) {
                        // alert("dsaasddasdasdasdasasd");
                        }
                    },{
                      text: '<b>Aceptar</b>',
                      type: 'button-positive asd',
                      onTap: function(e) {
                        if (!$scope.nombre.comentario) {
                          //don't allow the user to close unless he enters nombre password
                          e.preventDefault();
                          $ionicLoading.hide();
                        } else {
                            $ionicLoading.show({
                                template: 'Cargando...'
                            });

                $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenItem.php", $scope.rutaImagen, options).then(function(result) {
                  $rootScope.toast('Imagen agregada', 'short');
                  $ionicLoading.hide();
                }, function(err) {
                  $rootScope.toast('Error agregando imagen', 'short');
                  console.log(err);
                  $ionicLoading.hide();
                }, function (progress) {
                    // constant progress updates
                });
                            console.log($scope.nombre.comentario);
                            $http({
                              method: "POST",
                              url: "http://200.14.68.107/atacamaGo/addArchivoItemTuristico.php",
                              data: $httpParamSerializer({
                                "id_item_turistico"   : $rootScope.item_seleccionado.id,
                                "id_usuario"          : localStorage.getItem("id_usuario"),
                                "nombre_archivo_item" : $scope.nombreImagen,
                                "tipo_archivo_item"   : 2,
                                "comentario"          : $scope.nombre.comentario,
                                "lat"                 : lat,
                                "long"                : long
                              }),
                              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                            }).then(function(response){ //ok si guardó correctamente.
                              console.log(response.data);
                              if(response.data.resultado == "ok"){
                                $scope.hola.unshift({ 
                                    "foto"                      : $scope.rutaImagen,
                                    "comentario"                : $scope.nombre.comentario,
                                    "id_usuario"                : localStorage.getItem("id_usuario"),
                                    "id_archivo_item_turistico" : response.data.id_archivo_item_turistico,
                                    "nombre"                    : $scope.nombreImagen

                                });

                                $scope.holaTotal.unshift({ 
                                    "foto"                          : $scope.rutaImagen,
                                    "comentario"                    : $scope.nombre.comentario,
                                    "id_usuario"                    : localStorage.getItem("id_usuario"),
                                    "id_archivo_servicio_turistico" : response.data.id_archivo_servicio_turistico,
                                    "nombre"                        : $scope.nombreImagen,
                                });

                                $rootScope.toast("imagen agregada.","short");
                                
                                /*var image2 = document.getElementById('imagenSacadaFoto2');
                                image2.style.display   = 'block';
                                image2.src             = "";*/
                                $scope.tieneSrc        = false;
                                $ionicLoading.hide();
                                $ionicLoading.hide();
                                $scope.noEdita = false;
                                $scope.nombreImagen = undefined;
                              }else
                                $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            }, function(){ //Error de conexión
                              $rootScope.toast('Verifica tu conexión a internet',"short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            });
                            
                            $ionicLoading.hide();
                            return $scope.nombre.comentario;
                        }
                      }
                    }
                  ]
                });
$scope.$watch("nombre.comentario", function(newValue, oldValue){
    console.log("nuevo: " + newValue);
    console.log("viejo: " + oldValue);
    if (newValue.trim().length > 15){
        $scope.nombre.comentario = newValue.trim().substring(0,15);
    }
});

            }

            if( $rootScope.tipo_seleccionado == "servicio" ){
               // $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenServicio.php", $scope.rutaImagen, options).then(function(result) {
               //    console.log(result);
               //    //$rootScope.toast('Imagen agregada', 'short');
               //  }, function(err) {
               //    $rootScope.toast('Error agregando imagen', 'short');
               //    $ionicLoading.hide();
               //  }, function (progress) {
               //      // constant progress updates
               //  });

                //modal que sirve para porner un comentario en la imagen
                $scope.nombre = {};
                // An elaborate, custom popup
                $ionicLoading.hide();
                var myPopup = $ionicPopup.show({
                  template: '<input type="text" ng-model="nombre.comentario" placeholder="Escriba aquí">',
                  title: 'Agrega un comentario',
                  // subTitle: 'comentario',
                  scope: $scope,
                  buttons: [
                    { 
                      text: '<i class="icon ion-close-round"></i>',
                      type:'popclose',
                      onTap: function(e) {
                        // alert("dsaasddasdasdasdasasd");
                        }
                    },{
                      text: '<b>Aceptar</b>',
                      type: 'button-positive asd',
                      onTap: function(e) {
                        if (!$scope.nombre.comentario) {
                            //don't allow the user to close unless he enters nombre password
                            e.preventDefault();
                        } else {
                            console.log($scope.nombre.comentario);

                            $ionicLoading.show({
                                template: 'Cargando...'
                            });

               $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenServicio.php", $scope.rutaImagen, options).then(function(result) {
                  console.log(result);
                  //$rootScope.toast('Imagen agregada', 'short');
                }, function(err) {
                  $rootScope.toast('Error agregando imagen', 'short');
                  $ionicLoading.hide();
                }, function (progress) {
                    // constant progress updates
                });

                            $http({
                              method: "POST",
                              url: "http://200.14.68.107/atacamaGo/addArchivoServicio.php",
                              data: $httpParamSerializer({
                                "id_servicio"           : $rootScope.item_seleccionado.id,
                                "id_usuario"            : localStorage.getItem("id_usuario"),
                                "ruta_archivo_servicio" : $scope.nombreImagen,
                                "tipo_archivo_servicio" : 2,
                                "comentario"            : $scope.nombre.comentario,
                                "lat"                   : lat,
                                "long"                  : long
                              }),
                              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                            }).then(function(response){ //ok si guardó correctamente.
                              console.log(response.data);
                              if(response.data.resultado == "ok"){
                                $scope.hola.unshift({ 
                                    //"foto"                          : $scope.rutaImagen,
                                    "foto"                          : "http://200.14.68.107/atacamaGoMultimedia/files/servicios/"+ $scope.nombreImagen,
                                    "comentario"                    : $scope.nombre.comentario,
                                    "id_usuario"                    : localStorage.getItem("id_usuario"),
                                    "id_archivo_servicio_turistico" : response.data.id_archivo_servicio_turistico,
                                    "nombre"                        : $scope.nombreImagen,
                                });

                                $scope.holaTotal.unshift({ 
                                    //"foto"                          : $scope.rutaImagen,
                                    "comentario"                    : $scope.nombre.comentario,
                                    "foto"                          : "http://200.14.68.107/atacamaGoMultimedia/files/servicios/"+ $scope.nombreImagen,
                                    "id_usuario"                    : localStorage.getItem("id_usuario"),
                                    "id_archivo_servicio_turistico" : response.data.id_archivo_servicio_turistico,
                                    "nombre"                        : $scope.nombreImagen,
                                });

                                $rootScope.toast("imagen agregada.","short");

                                //var image2 = document.getElementById('imagenSacadaFoto2');
                                //image2.style.display   = 'block';
                                //image2.src             = "";
                                $scope.tieneSrc        = false;

                                $ionicLoading.hide();
                                $scope.noEdita = false;
                                $scope.nombreImagen = undefined;
                              }else
                                $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            }, function(){ //Error de conexión
                              $rootScope.toast('Verifica tu conexión a internet',"short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            });

                          return $scope.nombre.comentario;
                        }
                      }
                    }
                  ]
                });
$scope.$watch("nombre.comentario", function(newValue, oldValue){
    console.log("nuevo: " + newValue);
    console.log("viejo: " + oldValue);
    
    if( newValue === undefined ){
        $rootScope.toast('comentario obligatorio', 'short');
    }

    if (newValue.trim().length > 15){
        $scope.nombre.comentario = newValue.trim().substring(0,15);
    }
});
                
            }

            if( $rootScope.tipo_seleccionado == "patrimonio" ){
               // $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenPatrimonio.php", $scope.rutaImagen, options).then(function(result) {
               //    $rootScope.toast('Imagen agregada', 'short');
               //  }, function(err) {
               //    $rootScope.toast('Error agregando imagen', 'short');
               //    $ionicLoading.hide();
               //    console.log(err);
               //  }, function (progress) {
               //      // constant progress updates
               //  });
                
                //modal que sirve para porner un comentario en la imagen
                $scope.nombre = {};
                $ionicLoading.hide();
                // An elaborate, custom popup
                var myPopup = $ionicPopup.show({
                  template: '<input type="text" ng-model="nombre.comentario" placeholder="Escriba aquí">',
                  title: 'Agregar comentario',
                  // subTitle: 'comentario',
                  scope: $scope,
                  buttons: [
                    { 
                      text: '<i class="icon ion-close-round"></i>',
                      type:'popclose',
                      onTap: function(e) {
                        // alert("dsaasddasdasdasdasasd");
                        }
                    },{
                      text: '<b>Aceptar</b>',
                      type: 'button-positive asd',
                      onTap: function(e) {
                        if (!$scope.nombre.comentario) {
                          //don't allow the user to close unless he enters nombre password
                          
                          e.preventDefault();
                        } else {
                            $ionicLoading.show({
                                template: 'Cargando...'
                            });

               $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenPatrimonio.php", $scope.rutaImagen, options).then(function(result) {
                  $rootScope.toast('Imagen agregada', 'short');
                }, function(err) {
                  $rootScope.toast('Error agregando imagen', 'short');
                  $ionicLoading.hide();
                  console.log(err);
                }, function (progress) {
                    // constant progress updates
                });

                          console.log($scope.nombre.comentario);
                          $http({
                              method: "POST",
                              url: "http://200.14.68.107/atacamaGo/addArchivoPatrimonio.php",
                              data: $httpParamSerializer({
                                "id_patrimonio"             : $rootScope.item_seleccionado.id,
                                "id_usuario"                : localStorage.getItem("id_usuario"),
                                "ruta_archivo_patrimonio"   : $scope.nombreImagen,
                                "tipo_archivo_patrimonio"   : 2,
                                "comentario"                : $scope.nombre.comentario,
                                "lat"                       : lat,
                                "long"                      : long
                              }),
                              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                            }).then(function(response){ //ok si guardó correctamente.
                              console.log(response.data);
                              if(response.data.resultado == "ok"){
                                $scope.hola.unshift({ 
                                    //"foto"                    : $scope.rutaImagen,
                                    "foto"                    : "http://200.14.68.107/atacamaGoMultimedia/files/patrimonios/"+ $scope.nombreImagen,
                                    "comentario"              : $scope.nombre.comentario,
                                    "id_usuario"              : localStorage.getItem("id_usuario"),
                                    "id_archivo_patrimonio"   : response.data.id_archivo_patrimonio,
                                    "nombre"                  : $scope.nombreImagen,
                                });

                                $scope.holaTotal.unshift({ 
                                    //"foto"                          : $scope.rutaImagen,
                                    "foto"                          : "http://200.14.68.107/atacamaGoMultimedia/files/patrimonios/"+ $scope.nombreImagen,
                                    "comentario"                    : $scope.nombre.comentario,
                                    "id_usuario"                    : localStorage.getItem("id_usuario"),
                                    "id_archivo_servicio_turistico" : response.data.id_archivo_servicio_turistico,
                                    "nombre"                        : $scope.nombreImagen,
                                });
                                
                                $rootScope.toast("imagen agregada.","short");

                                $ionicLoading.hide();
                                $scope.noEdita = false;
                                $scope.nombreImagen = undefined;
                                
                                var image2 = document.getElementById('imagenSacadaFoto2');
                                image2.style.display   = 'block';
                                image2.src             = "";
                                $scope.tieneSrc        = false;

                                
                              }else
                                $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            }, function(){ //Error de conexión
                              $rootScope.toast('Verifica tu conexión a internet',"short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            });

                          return $scope.nombre.comentario;
                        }
                      }
                    }
                  ]
                });
                $scope.$watch("nombre.comentario", function(newValue, oldValue){
    console.log("nuevo: " + newValue);
    console.log("viejo: " + oldValue);
    if (newValue.trim().length > 15){
        $scope.nombre.comentario = newValue.trim().substring(0,15);
    }
});
                
            }
        }else{
            $rootScope.toast('Debes seleccionar una imagen',"short");
            $ionicLoading.hide();
        }
            
    }

    $scope.masComentarios = function(cantImagenes){
        console.log("total imagenes, ", $scope.holaTotal);
        console.log("antes de mas imagenes, ", $scope.hola);
        $scope.hola = [];
        //obtengo los comentarios 20, 30, 40, etc...
        angular.forEach($scope.holaTotal, function(value, key) {
          if( key < (cantImagenes*4) ){
            $scope.hola.push($scope.holaTotal[key]);    
          }
        });
        console.log("Despues de mas imagenes, ", $scope.hola);
        $scope.cantImagenes ++;
    }

    $rootScope.cerrarModalVisorImagen = function(){
        $rootScope.eliminarImagenVisor = false;
         $scope.modalVisorImagen.hide();
    }
    
  
}]);